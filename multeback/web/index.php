<?php

include_once("env.php");

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../multebox/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../multebox/config/main.php'),
    require(__DIR__ . '/../../multebox/config/main-local.php'),
    require(__DIR__ . '/../../multebox/config/datecontrol-module.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);

loadMulteDBConfigItems();
//loadMulteAppLic();
setMulteSessionParams();

if(!Yii::$app->user->isGuest)
{
	multebox\models\SessionVerification::checkSessionDetails();
}

function loadMulteDBConfigItems()
{
		$items = multebox\models\ConfigItem::find()->asArray()->all();
		foreach ($items as $item)
        {
            if ($item['config_item_name'])
			{
				Yii::$app->params[$item['config_item_name']] = $item['config_item_value'];
				Yii::$app->params[$item['config_item_name']."_description"] = $item['config_item_description'];
			}
        }
		
		$company = multebox\models\Company::find()->asArray()->one();
		Yii::$app->params['company'] = $company;
		Yii::$app->params['address']= multebox\models\search\Address::companyAddress($company['id']);

		$role = '';

		if(isset(Yii::$app->user->identity))
		{
			$role = multebox\models\AuthAssignment::find()->where("item_name='Admin' and user_id='".Yii::$app->user->identity->id."'")->asArray()->one();
		}	

		if(count($role) > 0){
			Yii::$app->params['user_role']= 'admin';
		}else{
			Yii::$app->params['user_role']= 'guest';	
		}

		Yii::$app->params['invalid_ext'] = array("PH", "JS", "EXE", "VB", "CMD", "BAT", "CGI", "PERL", "PY"); //All extentions with mentioned keywords in them will be blocked
		Yii::$app->params['zero_decimal_currencies'] = ['MGA','BIF','CLP','PYG','DJF','RWF','GNF','JPY','VND','VUV','XAF','KMF','KRW','XOF','XPF'];
}

function loadMulteAppLic()
{
		//Application Licence
		$licences = multebox\models\PrdLic::find()->where("prd_lic_status=1")->asArray()->all();
		if(count($licences) > 0){
			$lid= array();
			foreach($licences as $licence){
				$lid[]=$licence['id'];
			}
			$ids =implode(',',$lid);
			$sql ="select * from tbl_prd_mdl_lic,tbl_prd_mdl,tbl_prd_lic where tbl_prd_mdl_lic.prd_lic_id=tbl_prd_lic.id and  tbl_prd_mdl_lic.prd_mdl_id=tbl_prd_mdl.id and tbl_prd_mdl_lic.prd_lic_id IN($ids)";
			$connection = \Yii::$app->db;
			$command=$connection->createCommand($sql);
			$modules=$command->queryAll();
			$moduleArray = array();
			foreach($modules as $module){
				$moduleArray[]=$module['mdl_name'];
			}
			Yii::$app->params['modules'] =$moduleArray;
		}
}

function setMulteSessionParams()
{
		$_SESSION['SHOW_DEBUG_TOOLBAR'] = Yii::$app->params['SHOW_DEBUG_TOOLBAR'];
		$_SESSION['IDLE_SESSION_TIMEOUT_PERIOD'] = Yii::$app->params['IDLE_SESSION_TIMEOUT_PERIOD'];
		$_SESSION['LOCALE']=Yii::$app->params['LOCALE'];
}

$application->run();