<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 4/12/2019
 * Time: 5:39 PM
 */


use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Take Payments';
$this->params['breadcrumbs'][] = $this->title;

?>

<?php
    $o_id = $_REQUEST['o_id'];
    $s_id = $_REQUEST['s_id'];

    $c_name = 'barshan';
    $count = 1;
    $c_id = \multebox\models\Order::findOne($o_id)->customer_id;
    $pay = \multebox\models\Payments::find()->where('sub_order_id='.$s_id)->all();
    $c_name = \multebox\models\Customer::findOne($c_id)->customer_name;
    /*echo '<pre>';
    echo print_r($c_name);
    echo '</pre>';exit;*/


?>


    <div class="row">
        <div class="col-lg-12">
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="pay">
                            <thead>
                            <tr>
                                <th>Bill</th>
                                <th>Order#</th>
                                <th>Name</th>
                                <th>EMI #</th>
                                <th>Month</th>
                                <th>Due Date</th>
                                <th>I.Amount</th>
                                <th>O.Charges</th>
                                <th>MOP</th>
                                <th>Status</th>
                                <th>Pay</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($pay as $val){ ?>
                            <tr class="gradeX">
                                <td><?= $val->id?></td>
                                <td><?=  $val->order_id ?></td>
                                <td><?= $c_name ?></td>
                                <td ><?php if($val->installment_no == 0){echo 'Initial';}else{echo $val->installment_no;}?></td>
                                <td ><?= date("F",strtotime($val->due_date)); ?></td>
                                <td><?= date('d-m-Y',strtotime($val->due_date ))?></td>
                                <td><?= $val->installment_amonut ?></td>
                                <td><?= $val->installment_amonut ?></td>
                                <td><?= 'cash' ?></td>
                                <td><?php if($val->status == 0 ){echo 'Unpaid';}else{echo 'Paid';}?></td>
                                <td>
                                    <?php if($val->status == 0){ ?>
                                    <a href="<?=Yii::$app->homeUrl?>payments/payments/get-pay?id=<?=$val->id?>"><span class="btn-white btn btn-xs">Pay Now</span></a>
                                    <?php } 
                                    else if($val->status == 1){ ?>
                                        <a href="<?=Yii::$app->homeUrl?>payments/payments/view?id=<?=$val->id?>"><span class="btn-info btn btn-xs">Dup. Bill</span></a>
                                     <?php } ?>
                                </td>

                            </tr>

                            <?php $count++ ;}?>


                            </tbody>
                        </table>
                    </div>
    </div>


<!-- Page-Level Scripts -->
<script>
    $(document).ready(function(){
        $('#pay').DataTable({
            "paging":   false,
            "ordering": false,
            "info":     false
        });

    });

</script>




