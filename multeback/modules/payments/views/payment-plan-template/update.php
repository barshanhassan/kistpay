<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\PaymentPlanTemplate $model
 */

$this->title = 'Update Payment Plan Template: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Plan Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-plan-template-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_update', [
        'model' => $model,
    ]) ?>

</div>
