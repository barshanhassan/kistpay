<?php

use yii\helpers\Html;
use kartik\file\FileInput;
//use yii\helpers\Html;
use yii\bootstrap\ActiveForm ;

/* @var $this yii\web\View */
/* @var $model app\models\Insurance */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insurance-form">

    <?php $form = ActiveForm::begin(['options' => ['name' => 'multipartforms','enctype' => 'multipart/form-data','enableAjaxValidation' => true, 'novalidate'=>true]]); ?>


    <div class="row">

        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'email')->textInput(['class'=>'form-control rounded']) ?>
                </div>


                <div class="col-md-6">
                    <?= $form->field($model, 'mobile')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'city')->textInput(['class'=>'form-control rounded']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'adress')->textarea(['class'=>'form-control rounded']) ?>
                </div>


            </div>

        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'logo')->widget(FileInput::classname(), [
                'options' => ['multiple' => 'false'],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        <?php ActiveForm::end(); ?>

</div>
