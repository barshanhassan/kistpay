<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InsuranceRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Insurance Requests';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="insurance-request-index">


    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'customer_id',
                'value'=>'customer.first_name'
            ],
            'order_id',
            'modal_no',
            'serial_no',
            'IMEI1',
            'IEMI2',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model, $key, $index){
                    if($model->status == '0')
                        return '<span class="label label-warning">Pending</span>';
                    else if($model->status == 1)
                        return '<span class="label label-primary">Active</span>';
                    else
                        return '<span class="label label-info">Expired</span>';
                }

            ],

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{view} ',

                'buttons' => [
                    /*'delete' => function ($url, $model, $key) {

                        return Html::a('<span class="btn btn-white btn btn-xs">Delete</span>', $url,
                            [
                                'title' => Yii::t('app', 'Delete'),
                                'data-pjax' => '1',
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                    'pjax' => 1,],
                            ]
                        );

                    },
                    'update' => function ($url, $model)
                    {
                        return '<a href="'.Yii::$app->homeUrl.'orders/update?id='.$model->id.'"><span class="btn-white btn btn-xs" >Update</span></a>';
                    } ,*/
                    'view'=>function($url,$model)
                    {

                            return '<a href="'.Yii::$app->homeUrl.'insurance-request/view?id='.$model->id.'"><span class="btn-white btn btn-xs"> View</span></a>';
                    },

                ],
                /*'contentOptions' => function($model)
                {
                    return ['style' => 'max-width:100px !important;'];
                }*/
            ],
        ],
    ]); ?>
</div>
