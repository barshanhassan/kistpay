<?php

use yii\helpers\Html;
use kartik\file\FileInput;
//use yii\helpers\Html;
use yii\bootstrap\ActiveForm ;

/* @var $this yii\web\View */
/* @var $model app\models\InsuranceRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="insurance-request-form">

    <?php $form = ActiveForm::begin(['options' => ['name' => 'multipartforms','enctype' => 'multipart/form-data','enableAjaxValidation' => false, 'novalidate'=>true]]); ?>

    <div class="row">

        <div class="col-lg-8">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'modal_no')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'serial_no')->textInput(['class'=>'form-control rounded']) ?>
                </div>


                <div class="col-md-6">
                    <?= $form->field($model, 'IMEI1')->textInput(['class'=>'form-control rounded']) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'IEMI2')->textInput(['class'=>'form-control rounded']) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textarea(['class'=>'form-control rounded']) ?>
                </div>


            </div>

        </div>


        <div class="col-md-4">
            <?= $form->field($model, 'prod_image')->widget(FileInput::classname(), [
                'options' => ['multiple' => 'false'],
                'pluginOptions' => [
                    'showPreview' => true,
                    'showCaption' => true,
                    'showRemove' => false,
                    'showUpload' => false
                ]
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
