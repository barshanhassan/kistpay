<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\helpers\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Inursance Request', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

</div>

<?php
//$id = $_REQUEST['id'];
$query = \app\models\Customer::find()->joinWith('customerDocuments')->where('customer.id='.$model->customer_id)->one();
$productDtl = \app\models\Orders::find()->joinWith('orderItems')->where('orders.id='.$model->order_id)->one();
/*echo '<pre>';
 print_r($productDtl->orderItems[0]);
echo '</pre>';exit;*/
?>
<div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-lg-11">
            <div class="tabs-container">
                <ul class="nav nav-tabs" role="tablist">
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-1">Personal Information</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-2">Documents</a></li>
                    <li><a class="nav-link" data-toggle="tab" href="#tab-3">Order Details</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                        <div class="panel-body">
                            <div class="col-12">

                                <div class="col-12 has-shadow" style="margin-bottom: 8px;">
                                    <h3 style="text-align: center;font-weight: bold;letter-spacing: 1px;font-size: 15px;">
                                        <?php  'Order # was placed on  Please check the check the following detail' ?>
                                </div>

                                <div class="col-12 has-shadow">
                                    <h3 style="float: left;" class="title">Personal Detail</h3>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td scope="row">Name</td>
                                            <td><?= $query->first_name?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">CNIC</td>
                                            <td><?= $query->cnic?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Gender</td>
                                            <td><?= $query->gender?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Email</td>
                                            <td><?= $query->email?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Mobile #</td>
                                            <td><?=$query->mobile?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Phone</td>
                                            <td><?=$query->phone?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">City</td>
                                            <td><?= $query->city?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Address</td>
                                            <td><?= $query->address?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Income Type</td>
                                            <td><?= $query->income_type ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>


                                <h3 style="float: left;" class="title">Request Status </h3>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td scope="row">Status</td>
                                        <td><?php if($model->status == 0){ ?>
                                                <span class="label label-warning">Pending Request</span>
                                            <?php }
                                            else if($model->status == 1){?>
                                                <span class="label label-primary">Request Accepted</span>
                                            <?php }
                                            else if($model->status == 2)
                                            { ?>
                                                <span class="label label-danger">Request Rejected</span>
                                            <?php }
                                            else{ ?>
                                                <span class="label label-info">Request Under Process</span>
                                            <?php }
                                            ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" id="tab-2" class="tab-pane">
                        <div class="panel-body">

                            <div class="row">
                                <div class="row">
                                    <?php foreach ($query->customerDocuments as $doc) { ?>

                                        <div class="col-lg-6">
                                            <div class="ibox ">
                                                <div class=" text-center p-md">

                                                    <h4 class="m-b-xxs"><?=$doc->document_type?></h4>
                                                    <div class="m-t-md">

                                                        <div class="p-lg ">
                                                            <embed src="<?=Helper::getBaseUrl()?>drive/customerDocuments/<?= $doc->file_name ?>"
                                                                   width="350px" height="200px"/>
                                                        </div>
                                                        <a href="<?=Helper::getBaseUrl()?>drive/customerDocuments/<?= $doc->file_name ?>"><button type="button" class=" btn btn-success">
                                                                Download
                                                            </button></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div role="tabpanel" id="tab-3" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-12">
                                <?php $prod = \app\models\Products::find()->where(['=','id',$productDtl->orderItems[0]->product_id])->one();
                                // echo '<pre>'; echo print_r($prod); echo '</pre>';
                                ?>

                                <div class="col-12 has-shadow">
                                    <h3 style="float: left;" class="title">Order Detail</h3>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td scope="row">Product Name</td>
                                            <td><?=$prod->name?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Quantity</td>
                                            <td><?= $productDtl->orderItems[0]->quantity?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Original Price</td>
                                            <td><?= $prod->price?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Selected Bank</td>
                                            <td><?= $model->order_id->bank->name?></td>
                                        </tr>
                                        <tr>
                                            <td scope="row">Choosen EMI Plan</td>
                                            <td><?= $model->order_id->plan->plan_name?></td>
                                        </tr>

                                        </tbody>
                                    </table>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
