<?php

namespace multeback\modules\support;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'multeback\modules\support\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
