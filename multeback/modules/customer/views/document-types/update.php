<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\DocumentTypes $model
 */

$this->title = 'Update Document Types: ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="document-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
