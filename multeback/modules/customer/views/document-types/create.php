<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\DocumentTypes $model
 */

$this->title = 'Create Document Types';
$this->params['breadcrumbs'][] = ['label' => 'Document Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="document-types-create">
    <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
