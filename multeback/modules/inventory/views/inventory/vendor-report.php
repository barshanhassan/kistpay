<style>
    label {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        float: right !important;
        font-weight: bold;
    }
</style>
<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use multebox\models\CustomerType;
use multebox\models\User;
use multebox\models\search\UserType as UserTypeSearch;
use yii\helpers\ArrayHelper;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var multebox\models\search\CustomerSearch $searchModel
 */

$this->title = Yii::t('app', 'Vendor Report');
$this->params['breadcrumbs'][] = $this->title;

/*function statusLabel($status)
{
	if ($status !='1')
	{
		$label = "<span class=\"label label-danger\">".Yii::t('app', 'Inactive')."</span>";
	}
	else
	{
		$label = "<span class=\"label label-primary\">".Yii::t('app', 'Active')."</span>";
	}
	return $label;
}
$status = array('0'=>Yii::t('app', 'Inactive'),'1'=>Yii::t('app', 'Active'));*/

?>
<div class="customer-index">
   <!-- <div class="page-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </div> -->
   
   <!-- --><?php

   $inventry = \multebox\models\Inventory::find()->all();


   ?>

</div>



<section class="content">
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Vendor Wise Report</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Vendor</th>
                            <th>Product Category</th>
                            <th>Sub Category</th>
                            <th>Model Detail</th>
                            <th>Item Name</th>
                            <th>Unit</th>
                            <th>Purchase rate</th>
                            <th>Item Total Qty</th>
                            <th>Total Discount</th>
                            <th>Shipping Cost</th>
                            <th>Total Cost</th>
                            <th>User</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($inventry as $val){
                          $prod = \multebox\models\Product::find() ->where('id='.$val->product_id)->one();
                            $prod_cat = \multebox\models\ProductCategory::find() ->where('id='.$prod->category_id)->one();
                            $prod_subcat = \multebox\models\ProductSubCategory::find() ->where('id='.$prod->sub_category_id)->one();
                            $prodsubsubcat = \multebox\models\ProductSubSubCategory::find() ->where('id='.$prod->sub_subcategory_id)->one();
                            $vendor = \multebox\models\Vendor::find()->where('id='.$val->vendor_id)->one();
                            //-> leftJoin('tbl_product_category', 'tbl_product_category.id=tbl_product.id')


                          $cat =
                            $provance =0;
                            /*if($address->state_id) {

                                $provance = \multebox\models\State::find()->where('id=' . $address->state_id)->one();
                            }*/
                            ?>

                            <tr>
                                <td><?= $vendor->vendor_name?></td>
                                <td><?= $prod_cat->name?></td>
                                <td><?= $prod_subcat->name?></td>
                                <td><?= $prodsubsubcat->name?></td>
                                <td><?= $val->product_name?></td>
                                <td><?= 'piece'?></td>
                                <td><?= $val->price?></td>
                                <td><?= $val->stock?></td>
                                <td> <?=$val->discount?></td>
                                <td>0</td>
                                <td><?=$val->stock * $val->price?></td>
                                <td><?=$val->added_by_id?></td>
                                <td><?= date('Y-m-d',strtotime($val->added_at))?></td>
                            </tr>


                        <?php }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Vendor</th>
                            <th>Product Category</th>
                            <th>Name</th>
                            <th>Cellphone</th>
                            <th>Email</th>
                            <th>Cnic</th>
                            <th>Reg Date</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th>Province</th>
                            <th>Address</th>
                            <th>Card</th>
                            <th>Date</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>

<script>
    $(function () {

        $('#example1').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
            'paging'      : true,
            'lengthChange': false,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : true

        })
    })
</script>