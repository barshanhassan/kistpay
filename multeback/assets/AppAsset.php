<?php

namespace multeback\assets;

use yii\web\AssetBundle;

/**
 * Main multeback application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
		/*'bower_components/bootstrap/dist/css/bootstrap.min.css',
		'bower_components/font-awesome/css/font-awesome.min.css',
		'bower_components/Ionicons/css/ionicons.min.css',
		'dist/css/AdminLTE.min.css',
		'dist/css/skins/_all-skins.min.css',*/
    ];
    public $js = [
		/*'bower_components/jquery/dist/jquery.min.js',
		'bower_components/jquery-ui/jquery-ui.min.js',
		'bower_components/bootstrap/dist/js/bootstrap.min.js',
		'dist/js/adminlte.min.js',*/
        'theme/js/plugins/dataTables/datatables.min.js',
        'theme/js/plugins/dataTables/dataTables.bootstrap4.min.js',
		'theme/js/bootbox.min.js',
        'theme/js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
