<?php

namespace multebox\models\search;
use Yii;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use multebox\models\ConfigItem as configItemModel;
use multebox\models\File as FileModel;
use multebox\models\Note as NoteModel;
use multebox\models\History as HistoryModel;
use multebox\models\Address as AddressModel;
use multebox\models\Contact;
use multebox\models\User;
use multebox\models\Vendor;
use multebox\models\Product;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\ProductAttributes;
use multebox\models\DiscountCoupons;
use multebox\models\Inventory;
use multebox\models\Cart;
use multebox\models\GlobalDiscount;
use multebox\models\SendEmail;
use multebox\models\AuthAssignment;
//use multebox\models\Order;
//use multebox\models\SubOrder;
use multebox\models\OrderStatus;
use multebox\models\PaypalDetails;
use multebox\models\StripeDetails;
use multebox\models\PaypalRefundDetails;
use multebox\models\StripeRefundDetails;
use multebox\models\PaymentMethods;
use multebox\models\CommissionDetails;
use multebox\models\VendorReview;
use multebox\models\LicenseKeyCode;
use multebox\models\TicketStatus;
use multebox\models\Ticket as TicketModel;
use multebox\models\PaymentPlanTemplate;
use multebox\models\PaymentPlanTemplateDetail;
use app\helpers\Helper;


use yii\helpers\Url;
use yii\helpers\Json;

/**
 * MulteModel represents the model behind the search form about multebox\models\MulteModel.
 */
class MulteModel extends \yii\db\ActiveRecord
{
	/**
     * @inheritdoc
     */
    /*public static function tableName()
    {
        return '';
    }*/
	
	public static function getOrdersForDashboard()
	{
		return Order::find()->where("order_status not in ('".OrderStatus::_CANCELED."', '".OrderStatus::_REFUNDED."', '".OrderStatus::_RETURNED."')")->orderBy('id desc')->limit(7)->all();
	}

	public static function getVendorOrdersForDashboard()
	{
		return SubOrder::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and sub_order_status not in ('".OrderStatus::_CANCELED."', '".OrderStatus::_REFUNDED."', '".OrderStatus::_RETURNED."')")->orderBy('id desc')->limit(7)->all();
	}

	public static function getInventoriesForDashboard()
	{
		return Inventory::find()->orderBy('id desc')->limit(4)->all();
	}

	public static function getVendorInventoriesForDashboard()
	{
		return Inventory::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id)->orderBy('id desc')->limit(4)->all();
	}

	public static function getTotalOrderCount($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return Order::find()->where("added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->count();
	}

	public static function getTotalVendorOrderCount($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return SubOrder::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->count();
	}

	public static function getTotalSaleAmount($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return MulteModel::formatAmount(Order::find()->where("added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->sum(total_cost));
	}

	public static function getTotalVendorSaleAmount($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return MulteModel::formatAmount(SubOrder::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->sum(total_cost));
	}

	public static function getTotalCommission($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return MulteModel::formatAmount(CommissionDetails::find()->where("added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->sum(commission));
	}

	public static function getTotalVendorIncome($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return MulteModel::formatAmount(CommissionDetails::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->sum('sub_order_total - commission'));
	}

	public static function getTotalVendors($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return Vendor::find()->where("added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->count();
	}

	public static function getAverageVendorRating($months)
	{
		$oldtimestamp = strtotime("-$months month 00:00:00");
		$newtimestamp = strtotime('last day of this month 23:59:59', time());

		return VendorReview::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at > ".$oldtimestamp." and added_at <= ".$newtimestamp)->average(rating);
	}

	public static function getCurrentMonthOrderCount()
	{
		return Order::find()->where("added_at >= ".strtotime('first day of this month 00:00:00', time()))->count();
	}

	public static function getCurrentMonthVendorOrderCount()
	{
		return SubOrder::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at >= ".strtotime('first day of this month 00:00:00', time()))->count();
	}

	public static function getCurrentMonthSaleAmount()
	{
		return MulteModel::formatAmount(Order::find()->where("added_at >= ".strtotime('first day of this month 00:00:00', time()))->sum(total_cost));
	}

	public static function getCurrentMonthVendorSaleAmount()
	{
		return MulteModel::formatAmount(SubOrder::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at >= ".strtotime('first day of this month 00:00:00', time()))->sum(total_cost));
	}

	public static function getCurrentMonthCommission()
	{
		return MulteModel::formatAmount(CommissionDetails::find()->where("added_at >= ".strtotime('first day of this month 00:00:00', time()))->sum(commission));
	}

	public static function getCurrentMonthVendorIncome()
	{
		return MulteModel::formatAmount(CommissionDetails::find()->where("vendor_id = ".Yii::$app->user->identity->entity_id." and added_at >= ".strtotime('first day of this month 00:00:00', time()))->sum('sub_order_total - commission'));
	}

	public static function getCurrentMonthVendors()
	{
		return Vendor::find()->where("added_at >= ".strtotime('first day of this month 00:00:00', time()))->count();
	}

	public static function getCurrentMonthVendorRating()
	{
		return VendorReview::find()->where("added_at >= ".strtotime('first day of this month 00:00:00', time()))->average(rating);
	}

	public static function formatAmount($amount)
	{
		$roundval = round($amount, 2);

		return number_format($roundval, 0);
	}

	/*********************************************************
	 * Takes as input sub_order_id and updates its main
	 * order status to most relevant status according to
	 * sub_order item(s) status
	 *********************************************************/
	public static function updateMainOrderStatus ($suborderid)
	{
		$suborder = SubOrder::findone($suborderid);
		$order = Order::findOne ($suborder->order_id);
		$suborders = SubOrder::find()->where("order_id=".$order->id)->all();
		
		$match = true;
		
		if($suborder->sub_order_status != OrderStatus::_IN_PROCESS)
		{
			foreach ($suborders as $row)
			{
				if ($row->sub_order_status == $suborder->sub_order_status || $row->sub_order_status == OrderStatus::_CANCELED)
				{
					continue;
				}
				else
				{
					$match = false;
					break;
				}
			}
		}
		
		$statuscount = 0;
		$savedstatus = '';
		foreach ($suborders as $row)
		{
			if ($row->sub_order_status == OrderStatus::_CANCELED || $row->sub_order_status == OrderStatus::_DELIVERED || $row->sub_order_status == OrderStatus::_REFUNDED)
			{
				$statuscount++;
				if ($row->sub_order_status == OrderStatus::_DELIVERED)
				{
					$savedstatus = OrderStatus::_DELIVERED;
				}
				else
				if ($row->sub_order_status == OrderStatus::_REFUNDED)
				{
					if ($savedstatus != OrderStatus::_DELIVERED)
						$savedstatus = OrderStatus::_REFUNDED;
				}
				else
				if ($row->sub_order_status == OrderStatus::_CANCELED)
				{
					if ($savedstatus != OrderStatus::_DELIVERED && $savedstatus != OrderStatus::_REFUNDED)
						$savedstatus = OrderStatus::_CANCELED;
				}
			}
		}

		if ($statuscount == count($suborders))
		{
			$order->order_status = $savedstatus;
			$order->save();
		}
		else
		if ($match)
		{
			$order->order_status = $suborder->sub_order_status;
			$order->save();
		}
	}
	
	/****************************************************
	 * Encrypts and decrypts input string
	 * $action = 'e' for encryption
	 * $action = 'd' for decryption
	 ****************************************************/
	public static function multecrypt ($string, $action = 'e') 
	{
		$secret_key = 'multecrypt_key';
		$secret_iv = 'multecrypt_iv';

		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash ('sha256', $secret_key);
		$iv = substr (hash ('sha256', $secret_iv), 0, 16);

		if ($action == 'e')
		{
			$output = base64_encode (openssl_encrypt ($string, $encrypt_method, $key, 0, $iv));
		}
		else 
		if($action == 'd')
		{
			$output = openssl_decrypt (base64_decode ($string), $encrypt_method, $key, 0, $iv);
		}

		return $output;
	}
	
	public static  function searchAttachments($params, $entity_id,$entity_type)
	{
		$query = FileModel::find ()->where ( [ 
				'entity_type' => $entity_type,
				'entity_id' => $entity_id 
		] );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}
	
	public static  function searchNotes($params, $entity_id,$entity_type)
	{
		$query = NoteModel::find ()->where ( [ 
				'entity_type' => $entity_type,
				'entity_id' => $entity_id 
		] )->all();
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $query;
	}

	public static  function searchHistory($params, $entity_id,$entity_type)
	{
		$query = HistoryModel::find ()->where ( [ 
				'entity_type' => $entity_type,
				'entity_id' => $entity_id 
		] )->orderBy('id desc');
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}
	
	public static  function searchAddresses($params, $entity_id,$entity_type)
	{
		$query = AddressModel::find()->where("entity_id=$entity_id and entity_type='$entity_type'");
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}

	public static  function searchContacts($params, $entity_id,$entity_type)
	{
		$query = Contact::find()->where("entity_id=$entity_id and entity_type='$entity_type'");
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}

	public static  function searchProductSubCategory($params, $id)
	{
		$query = ProductSubCategory::find()->where("parent_id=$id");
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}

	public static  function searchProductSubSubCategory($params, $id)
	{
		$query = ProductSubSubCategory::find()->where("parent_id=$id");
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}

	public static  function searchProductAttributes($params, $id)
	{
		$query = ProductAttributes::find()->where("parent_id=$id");
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}

	public static function getInventoryActualPrice($inventory_item)
	{
		$inventoryPrice = floatval($inventory_item->price);
		if($inventory_item->price_type == 'B')
		{
			foreach(json_decode($inventory_item->attribute_price) as $row)
			{
				$inventoryPrice += floatval($row);
			}
		}

		return $inventoryPrice;
	}

	public static function getInventoryDiscountPercentage($inventory_item, $total_items)
	{
		$inventoryPrice = MulteModel::getInventoryActualPrice($inventory_item);
		$normal = 0;

		if ($inventory_item->slab_discount_ind == 1) // Slab discount applicable
		{
			if ($inventory_item->slab_4_range != 0 && $inventory_item->slab_4_range != '' && $total_items >= $inventory_item->slab_4_range)
			{
				$discount = $inventory_item->slab_4_discount;
				$discount_type = $inventory_item->slab_discount_type;
			}
			else if ($inventory_item->slab_3_range != 0 && $inventory_item->slab_3_range != '' && $total_items >= $inventory_item->slab_3_range)
			{
				$discount = $inventory_item->slab_3_discount;
				$discount_type = $inventory_item->slab_discount_type;
			}
			else if($inventory_item->slab_2_range != 0 && $inventory_item->slab_2_range != '' && $total_items >= $inventory_item->slab_2_range)
			{
				$discount = $inventory_item->slab_2_discount;
				$discount_type = $inventory_item->slab_discount_type;
			}
			else if ($inventory_item->slab_1_range != 0 && $inventory_item->slab_1_range != '' && $total_items >= $inventory_item->slab_1_range)
			{
				$discount = $inventory_item->slab_1_discount;
				$discount_type = $inventory_item->slab_discount_type;
			}
			else
			{
				$normal = 1;
				$discount = $inventory_item->discount;
				$discount_type = $inventory_item->discount_type;
			}
		}
		else
		{
			$normal = 1;
			$discount = $inventory_item->discount;
			$discount_type = $inventory_item->discount_type;
		}
		/*var_dump($inventory_item->discount);
		var_dump($discount);
		var_dump($discount_type);exit;*/

		if($discount_type == 'P')
		{
			$inventoryDiscount = $discount;
		}
		else if ($normal)
		{
			if($inventoryPrice > 0)
				$inventoryDiscount = round((floatval($discount)/$inventoryPrice)*100,2);
			else
				$inventoryDiscount = 0;
		}
		else
		{
			if($inventoryPrice > 0)
				$inventoryDiscount = (floatval($discount)/($inventoryPrice*$total_items))*100;
			else
				$inventoryDiscount = 0;
		}

		return $inventoryDiscount;
	}

	public static function getInventoryDiscountedPrice($inventory_item, $total_items)
	{
		$inventoryPrice = MulteModel::getInventoryActualPrice($inventory_item);

		$inventoryDiscount = MulteModel::getInventoryDiscountPercentage($inventory_item, $total_items);

		$inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

		return $inventoryDiscountedPrice;
	}

	public static function getInventoryDiscountAmount($inventory_item, $total_items)
	{
		$inventoryPrice = MulteModel::getInventoryActualPrice($inventory_item);

		$inventoryDiscount = MulteModel::getInventoryDiscountPercentage($inventory_item, $total_items);

		$inventoryDiscountAmount = round($inventoryPrice*$inventoryDiscount/100, 2);

		return $inventoryDiscountAmount;
	}

	public static function getInventoryTotalAmount($inventory_item, $total_items)
	{
		return MulteModel::getInventoryDiscountedPrice($inventory_item, $total_items) + $inventory_item->shipping_cost;
	}

	public static function getCartItemCostSingleUnitWithoutShipping($cart_item)
	{
		$inventory_item = Inventory::findOne($cart_item->inventory_id);

		$inventoryPrice = floatval($inventory_item->price);

		$inventoryDiscount = MulteModel::getInventoryDiscountPercentage($inventory_item, $cart_item->total_items);
			
		$inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

		return $inventoryDiscountedPrice;
	}

	public static function getCartItemCostWithoutShipping($cart_item)
	{
		$inventoryDiscountedPrice = MulteModel::getCartItemCostSingleUnitWithoutShipping($cart_item);

		$cart_item_total_price = ($inventoryDiscountedPrice) * $cart_item->total_items;
		
		return $cart_item_total_price;
	}

	public static function getCartItemCostWithShipping($cart_item)
	{
		$inventory_item = Inventory::findOne($cart_item->inventory_id);

		$inventoryDiscountedPrice = MulteModel::getCartItemCostSingleUnitWithoutShipping($cart_item);

		$cart_item_total_price = ($inventoryDiscountedPrice + $inventory_item->shipping_cost) * $cart_item->total_items;
		
		return $cart_item_total_price;
	}

	/***************************************************
	* Return Values
	* a - Invalid Coupon
	* b - Expired Coupon
	* c - Not applicable on cart items
	* d - Not applicable on cart amount
	* e - Coupon code not issued to current customer
	* f - Coupon code max budget exhausted
	* Rest - HTML result to replace checkout page cart
	***************************************************/
	public static function getCouponDiscount($discount_coupon)
	{
		$discount_row = DiscountCoupons::find()->where("coupon_code='".$discount_coupon."'")->one();
		$site_level_coupon = false;
		$category_level_coupon = false;
		$sub_category_level_coupon = false;
		$sub_subcategory_level_coupon = false;

		if (count($discount_row) == 0)
		{
			return "a"; //Invalid Coupon
		}

		if (($discount_row->customer_id > 0) && ($discount_row->customer_id != Yii::$app->user->identity->entity_id))
		{
			return "e"; //Coupon code not issued to current customer
		}

		if (($discount_row->used_count == $discount_row->max_uses) || ($discount_row->expiry_datetime <= time()))
		{
			return "b"; //Coupon expired
		}

		if (($discount_row->max_budget > 0) && ($discount_row->used_budget >= $discount_row->max_budget))
		{
			return "f"; //Coupon budget exhausted
		}

		if(Yii::$app->user->isGuest)
		{
			$cart_items = Cart::find()->where("session_id='".session_id()."'")->all();
		}
		else
		{
			$cart_items = Cart::find()->where("user_id=".Yii::$app->user->identity->id)->all();
		}

		$total_cart_cost = 0;
		$vendor_coupon = false;
		$coupon_added_by = User::findOne($discount_row->added_by_id);

		if ($coupon_added_by->entity_type == 'vendor')
		{
			$vendor_coupon = true;
		}

		if ($discount_row->category_id == '' || $discount_row->category_id == 0)
		{
			$site_level_coupon = true;
			/* Site level coupon */
			foreach ($cart_items as $cart)
			{
				if ($vendor_coupon)
				{
					if(Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id)
					{
						$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
					}
				}
				else
				{
					$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
				}
			}
		}
		else if ($discount_row->sub_category_id == '' || $discount_row->sub_category_id == 0)
		{
			$category_level_coupon = true;
			/* Category level coupon */
			foreach ($cart_items as $cart)
			{
				if($cart->product->category_id == $discount_row->category_id)
				{
					if ($vendor_coupon)
					{
						if(Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id)
						{
							$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
						}
					}
					else
					{
						$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
					}
				}
			}
		}
		else if ($discount_row->sub_subcategory_id == '' || $discount_row->sub_subcategory_id == 0)
		{
			$sub_category_level_coupon = true;
			/* Sub_category level coupon */
			foreach ($cart_items as $cart)
			{
				if(($cart->product->category_id == $discount_row->category_id) && ($cart->product->sub_category_id == $discount_row->sub_category_id))
				{
					if ($vendor_coupon)
					{
						if(Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id)
						{
							$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
						}
					}
					else
					{
						$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
					}
				}
			}
		}
		else
		{
			$sub_subcategory_level_coupon = true;
			/* Sub_subcategory level coupon */
			foreach ($cart_items as $cart)
			{
				if(($cart->product->category_id == $discount_row->category_id) && ($cart->product->sub_category_id == $discount_row->sub_category_id) && ($cart->product->sub_subcategory_id == $discount_row->sub_subcategory_id))
				{
					if($discount_row->inventory_id == '' || $discount_row->inventory_id == 0 || $discount_row->inventory_id == $cart->inventory_id)
					{
						if ($vendor_coupon)
						{
							if(Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id)
							{
								$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
							}
						}
						else
						{
							$total_cart_cost += MulteModel::getCartItemCostWithoutShipping($cart);
						}
					}
				}
			}
		}

		if($total_cart_cost == 0)
		{
			return "c"; //Not applicable on cart items
		}

		if($total_cart_cost < $discount_row->min_cart_amount)
		{
			return "d"; //Not applicable on cart amount
		}

		$coupon_discount = 0;
		if($discount_row->discount_type == 'P')
		{
			$coupon_discount = $total_cart_cost * $discount_row->discount/100;

			if(($discount_row->max_discount > 0) && ($coupon_discount > $discount_row->max_discount))
			{
				$coupon_discount = $discount_row->max_discount;
			}
		}
		else
		{
			if($discount_row->discount > $total_cart_cost) // TODO
				$coupon_discount = $total_cart_cost;
			else
				$coupon_discount = $discount_row->discount;
		}

		if (($discount_row->max_budget > 0) && ($coupon_discount > ($discount_row->max_budget - $discount_row->used_budget)))
		{
			$coupon_discount = $discount_row->max_budget - $discount_row->used_budget;
		}
		
		/* Now update Cart rows with individual discount */
		$coupon_percent = $coupon_discount/$total_cart_cost;

		if ($site_level_coupon)
		{
			foreach($cart_items as $cart)
			{
				if ($vendor_coupon && Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id || !$vendor_coupon)
				{
					$cart_item_cost = MulteModel::getCartItemCostWithoutShipping($cart);
					$cart_item_discount = $cart_item_cost * $coupon_percent;

					if($cart_item_discount + $cart->global_discount_temp > $cart_item_cost)
					{
						$cart_item_discount -= $cart->global_discount_temp;
						$coupon_discount -= $cart->global_discount_temp;
					}
					
					$cart->coupon_discount_temp = $cart_item_discount;
					$cart->discount_coupon_id = $discount_row->id;
					$cart->save();

					/*Cart::updateAll(['coupon_discount_temp' => MulteModel::getCartItemCostWithoutShipping($cart)*$coupon_percent, 'discount_coupon_id' => $discount_row->id], ['=', 'id', $cart->id]);*/
				}
			}
		}
		else if ($category_level_coupon)
		{
			foreach ($cart_items as $cart)
			{
				if($cart->product->category_id == $discount_row->category_id)
				{
					if ($vendor_coupon && Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id || !$vendor_coupon)
					{
						$cart_item_cost = MulteModel::getCartItemCostWithoutShipping($cart);
						$cart_item_discount = $cart_item_cost * $coupon_percent;

						if($cart_item_discount + $cart->global_discount_temp > $cart_item_cost)
						{
							$cart_item_discount -= $cart->global_discount_temp;
							$coupon_discount -= $cart->global_discount_temp;
						}
						
						$cart->coupon_discount_temp = $cart_item_discount;
						$cart->discount_coupon_id = $discount_row->id;
						$cart->save();

						/*Cart::updateAll(['coupon_discount_temp' => MulteModel::getCartItemCostWithoutShipping($cart)*$coupon_percent, 'discount_coupon_id' => $discount_row->id], ['=', 'id', $cart->id]);*/
					}
				}
			}
		}
		else if ($sub_category_level_coupon)
		{
			foreach ($cart_items as $cart)
			{
				if(($cart->product->category_id == $discount_row->category_id) && ($cart->product->sub_category_id == $discount_row->sub_category_id))
				{
					if ($vendor_coupon && Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id || !$vendor_coupon)
					{
						$cart_item_cost = MulteModel::getCartItemCostWithoutShipping($cart);
						$cart_item_discount = $cart_item_cost * $coupon_percent;

						if($cart_item_discount + $cart->global_discount_temp > $cart_item_cost)
						{
							$cart_item_discount -= $cart->global_discount_temp;
							$coupon_discount -= $cart->global_discount_temp;
						}

						$cart->coupon_discount_temp = $cart_item_discount;
						$cart->discount_coupon_id = $discount_row->id;
						$cart->save();

						/*Cart::updateAll(['coupon_discount_temp' => MulteModel::getCartItemCostWithoutShipping($cart)*$coupon_percent, 'discount_coupon_id' => $discount_row->id], ['=', 'id', $cart->id]);*/
					}
				}
			}
		}
		else if ($sub_subcategory_level_coupon)
		{
			foreach ($cart_items as $cart)
			{
				if(($cart->product->category_id == $discount_row->category_id) && ($cart->product->sub_category_id == $discount_row->sub_category_id) && ($cart->product->sub_subcategory_id == $discount_row->sub_subcategory_id))
				{
					if($discount_row->inventory_id == '' || $discount_row->inventory_id == 0 || $discount_row->inventory_id == $cart->inventory_id)
					{
						if ($vendor_coupon && Inventory::findOne($cart->inventory_id)->vendor_id == $coupon_added_by->entity_id || !$vendor_coupon)
						{
							$cart_item_cost = MulteModel::getCartItemCostWithoutShipping($cart);
							$cart_item_discount = $cart_item_cost * $coupon_percent;

							if($cart_item_discount + $cart->global_discount_temp > $cart_item_cost)
							{
								$cart_item_discount -= $cart->global_discount_temp;
								$coupon_discount -= $cart->global_discount_temp;
							}
							$cart->coupon_discount_temp = $cart_item_discount;
							$cart->discount_coupon_id = $discount_row->id;
							$cart->save();

							/*Cart::updateAll(['coupon_discount_temp' => MulteModel::getCartItemCostWithoutShipping($cart)*$coupon_percent, 'discount_coupon_id' => $discount_row->id], ['=', 'id', $cart->id]);*/
						}
					}
				}
			}
		}

		return $coupon_discount;
	}

	public static function getGlobalDiscountSingle($cart, $discount_row, $indicator)
	{
		$global_discount = 0;
		$total_cart_cost = MulteModel::getCartItemCostWithoutShipping($cart);

		if(
			($discount_row->category_id == '' || $discount_row->category_id == 0) || 
			(($discount_row->category_id == $cart->product->category_id) && ($discount_row->sub_category_id == '' || $discount_row->sub_category_id == 0)) || 
			(($discount_row->category_id == $cart->product->category_id && $discount_row->sub_category_id == $cart->product->sub_category_id) && ($discount_row->sub_subcategory_id == '' || $discount_row->sub_subcategory_id == 0)) ||
			($discount_row->category_id == $cart->product->category_id && $discount_row->sub_category_id == $cart->product->sub_category_id && $discount_row->sub_subcategory_id == $cart->product->sub_subcategory_id)
		  )
		{
			//print_r($cart->id."<br> <br>");
			//print_r($discount_row->id."<br><br>");
			if($discount_row->discount_type == 'P')
			{
				$global_discount = $total_cart_cost * $discount_row->discount/100;

				if(($discount_row->max_discount > 0) && ($global_discount > $discount_row->max_discount))
				{
					$global_discount = $discount_row->max_discount;
				}
			}
			else
			{
				$global_discount = $discount_row->discount;
			}

			if ($discount_row->max_budget > 0)
			{
				if($indicator == 0) // Before order confirmation
				{
					if($global_discount > ($discount_row->max_budget - $discount_row->used_budget_temp))
					{
						$global_discount = $discount_row->max_budget - $discount_row->used_budget_temp;
					}
					
					$discount_row->used_budget_temp += $global_discount;
				}
				else
				{
					if($global_discount > ($discount_row->max_budget - $discount_row->used_budget))
					{
						$global_discount = $discount_row->max_budget - $discount_row->used_budget;
					}
					
					$discount_row->used_budget += $global_discount;
				}
			}
		}

		//$cart_item_cost = MulteModel::getCartItemCostWithoutShipping($cart);

		if($global_discount > $total_cart_cost)
		{
			$global_discount = $total_cart_cost;
		}

		if($indicator == 0)
			$discount_row->used_budget_temp = $discount_row->used_budget_temp + $global_discount;
		else
			$discount_row->used_budget = $discount_row->used_budget + $global_discount;

		$discount_row->save();

		Cart::updateAll(['global_discount_temp' => $global_discount, 'global_discount_id' => $discount_row->id], ['=', 'id', $cart->id]);

		return $global_discount;
	}
	
	/*********************************************************
	 * First matching global discount entry will be 
	 * used to calculate the discount. Exact match of
	 * sub_subcategory_id, sub_category_id, category_id
	 * in above order will take precedence
	 * $cart_items - list of cart items
	 * $indicator: 
	 *            0 if before order confirmation
	 *            1 if on order confirmation - !!!Not in Use!!!
	 *********************************************************/
	public static function getGlobalDiscount($cart_items, $indicator)
	{
		$global_discount = GlobalDiscount::find()->orderBy('sub_subcategory_id desc, sub_category_id desc, category_id desc')->all();
		
		if ($indicator == 0) // If before order confirmation
		{
			foreach($global_discount as $discount_row)
			{
				$discount_row->used_budget_temp = $discount_row->used_budget;
				//$discount_row->save();
			}
		}

		$selected_row = new GlobalDiscount;

		$cart_discount = 0;

		foreach($cart_items as $cart)
		{
			Cart::updateAll(['global_discount_temp' => 0, 'global_discount_id' => 0], ['=', 'id', $cart->id]);
			foreach($global_discount as $discount_row)
			{
				if ($discount_row->category_id == '' || $discount_row->category_id == 0)
				{
					$cart_discount += MulteModel::getGlobalDiscountSingle($cart, $discount_row, $indicator);
					break;
				}
				else if (($discount_row->sub_category_id == '' || $discount_row->sub_category_id == 0) && ($discount_row->category_id == $cart->product->category_id))
				{
					$cart_discount += MulteModel::getGlobalDiscountSingle($cart, $discount_row, $indicator);
					break;
				}
				else if (($discount_row->sub_subcategory_id == '' || $discount_row->sub_subcategory_id == 0) && (($discount_row->category_id == $cart->product->category_id && $discount_row->sub_category_id == $cart->product->sub_category_id)))
				{
					$cart_discount += MulteModel::getGlobalDiscountSingle($cart, $discount_row, $indicator);
					break;
				}
				else if ($discount_row->category_id == $cart->product->category_id && $discount_row->sub_category_id == $cart->product->sub_category_id && $discount_row->sub_subcategory_id == $cart->product->sub_subcategory_id)
				{
					$cart_discount += MulteModel::getGlobalDiscountSingle($cart, $discount_row, $indicator);
					break;
				}
			}
		}

		//exit;
		return $cart_discount;
	}

	/*************************************************
	 * Takes all user data except password.
	 * Password gets generated dynamically and
	 * sent to user in email. Assign roles.
	 *************************************************/
	public static function createUserWithRolesAndSendMail($user, $role)
	{
		$length = 8;
		$new_password = Yii::$app->security->generateRandomString ($length);
		
		$user->password_hash=Yii::$app->security->generatePasswordHash ($user->password_hash);

		if($user->save()){

        }else{
		    echo '<pre>';
		    echo print_r($user);
		    exit;
        }

		$emailObj = new SendEmail;


        $emailsubject = 'KistPay User Registered';
        $emailmessage = "<img src='http://kistpay.maaliksoft.com/emi/multefront/web//img/homepage-group-9-1@2x.png' class='img-slider'><br>Dear Customer! <br> Thank you for your REgistration. We appreciate your business. If you have any questions or feedback, please reach  us out on Info@kistpay.com. <br>  Thank You,<br>Team Kistpay";
        $email = Helper::sendEmail($user->email,$emailmessage,$emailsubject);
		//SendEmail::sendNewUserEmail($user->email, $user->first_name." ".$user->last_name, $user->username, $new_password);
		
		$rolemodel = new AuthAssignment;
		$rolemodel->item_name = $role;
		$rolemodel->user_id = $user->id;
		$rolemodel->save();

		$old_session_id = session_id();
		if(Yii::$app->user->login(User::findByUsername($user->username), 3600 * 24 * 30))
		{
			// No need to check each existing cart item for old_session as this is a fresh user and hence
			// no old item can be found in cart of logged in user
			Cart::updateAll(['session_id' => session_id(), 'user_id' => Yii::$app->user->identity->id], ['=', 'session_id', $old_session_id]);
		}

		return $user;
	}

	public static function mapJsonToModel($array_row, \yii\db\ActiveRecord $model)
	{
		$all_attributes = $model->getAttributes();

		foreach($all_attributes as $key=>$value)
		{
			$model->$key = $array_row[$key];	
		}

		return $model;
	}

	public static function mapJsonArrayToModelArray($array, \yii\db\ActiveRecord $model)
	{
		$all_attributes = $model->getAttributes();
		$model_array = [];
		
		foreach($array as $array_row)
		{
			$model_temp = new $model;
			foreach($all_attributes as $key=>$value)
			{
				$model_temp->$key = $array_row[$key];	
			}

			array_push($model_array, $model_temp);
		}

		return $model_array;
	}

	public static function getPriceBeforeTax($price, $tax_percent)
	{
		return $price/(1+$tax_percent/100);
	}

	public static function getCommission($suborder, $rule)
	{
		if($rule->commission_type == 'P')
		{
			$commission = MulteModel::getSubOrderVendorCost($suborder) * $rule->commission/100;
		}
		else
		{
			$commission = $rule->commission;
		}

		return $commission;
	}

	public static function getSubOrderVendorCost($suborder)
	{
		 $coupon = DiscountCoupons::findOne($suborder['discount_coupon_id']);

		 if(count($coupon) > 0)
		 {
			 $added_by = User::findOne($coupon->added_by_id);
			 if($added_by->entity_type == 'vendor')
			 {
				 $cost = $suborder['total_cost'] + $suborder['total_site_discount'];
			 }
			 else
			 {
				$cost = $suborder['total_cost'] + $suborder['total_site_discount'] + $suborder['total_coupon_discount'];
			 }
		 }
		 else
		 {
			 $cost = $suborder['total_cost'] + $suborder['total_site_discount'];
		 }
		 
		 return round($cost, 2);
	}

	public static function releaseInventoryStock($suborder)
	{
		$inventory = Inventory::findOne($suborder->inventory_id);
		$inventory->stock++;
		$inventory->save();
	}

	public static function releaseCouponDiscount($suborder)
	{
		if($suborder->total_coupon_discount > 0)
		{
			$discount_coupon_row = DiscountCoupons::findOne($suborder->discount_coupon_id);
			$discount_coupon_row->used_budget -= $suborder->total_coupon_discount;
			$discount_coupon_row->save();
		}
	}

	public static function releaseGlobalDiscount($suborder)
	{
		if($suborder->total_site_discount > 0)
		{
			$global_discount_row = GlobalDiscount::findOne($suborder->global_discount_id);
			$global_discount_row->used_budget -= $suborder->total_site_discount;
			$global_discount_row->save();
		}
	}

	public static function issueStripeRefund($suborder)
    {
        \Stripe\Stripe::setApiKey(MulteModel::multecrypt(Yii::$app->params['STRIPE_SECRET_KEY'], 'd'));
		$order = Order::findOne($suborder->order_id);
		$stripedetails = StripeDetails::find()->where("order_id=".$order->id)->one();

		if($suborder->sub_order_status != OrderStatus::_RETURNED && $suborder->sub_order_status != OrderStatus::_CANCELED)
		{
			Yii::$app->session->setFlash('error', 'Refund for current order not allowed!');
			return 0;
		}

		$zero_decimal_currencies = Yii::$app->params['zero_decimal_currencies'];

		if(in_array(Yii::$app->params['SYSTEM_CURRENCY'], $zero_decimal_currencies))
		{
			$suborder_cost = round($suborder->total_cost);
		}
		else
		{
			$suborder_cost = round($suborder->total_cost*100);
		}

		try
		{
			$refundedSale = \Stripe\Refund::create(array(
							"charge" => $stripedetails->charge_id,
							"amount" => $suborder_cost,
							//"refund_application_fee" => true,
							//"reverse_transfer" => true
						));

			Yii::$app->session->setFlash('success', 'Refund Successful! Please note down your refund reference number: '.$refundedSale->id);
		}
		catch (Exception $e)
		{
			Yii::$app->session->setFlash('error', 'Stripe refund failed with error: '.$e->getMessage());
			return 0;
		}

		try
		{
			// Capture refund details in system
			$refunddetails = new StripeRefundDetails;
			$refunddetails->order_id = $order->id;
			$refunddetails->sub_order_id = $suborder->id;
			$refunddetails->amount = $suborder->total_cost;
			$refunddetails->result_json = $refundedSale->__toJSON();
			$refunddetails->refund_id = $refundedSale->id;
			$refunddetails->added_at = time();

			$refunddetails->save();

			// Update sub order status and order status (if applicable)
			$suborder->sub_order_status = OrderStatus::_REFUNDED;
			$suborder->save();

			$suborders = SubOrder::find()->where("order_id=".$order->id)->all();
			$safe = true;

			foreach($suborders as $row)
			{
				if($row->sub_order_status != OrderStatus::_REFUNDED && $row->sub_order_status != OrderStatus::_CANCELED)
				{
					$safe = false;
					break;
				}
			}
			
			if($safe)
			{
				$order->order_status = OrderStatus::_REFUNDED;
				$order->save();
			}
		}
		catch (Exception $e)
		{
			Yii::$app->session->setFlash('error', 'Your refund is successful but we enountered some issue with order. Please contact customer support for further assistance! Error message:'.$e->getMessage());
		}

		return 0;
    }

	public static function issuePaypalRefund($suborder)
	{
		$apiContext = new \PayPal\Rest\ApiContext(
												  new \PayPal\Auth\OAuthTokenCredential(
													Yii::$app->params['PAYPAL_API_ID'],
													MulteModel::multecrypt(Yii::$app->params['PAYPAL_SECRET_ID'], 'd')
												  )
												);
		
		$order = Order::findOne($suborder->order_id);
		$paypaldetails = PaypalDetails::find()->where("order_id=".$order->id)->one();

		if($suborder->sub_order_status != OrderStatus::_RETURNED && $suborder->sub_order_status != OrderStatus::_CANCELED)
		{
			Yii::$app->session->setFlash('error', 'Refund for current order not allowed!');
			return 0;
		}

		$zero_decimal_currencies = Yii::$app->params['zero_decimal_currencies'];

		if(in_array(Yii::$app->params['SYSTEM_CURRENCY'], $zero_decimal_currencies))
		{
			$suborder_cost = round($suborder->total_cost);
		}
		else
		{
			$suborder_cost = round($suborder->total_cost, 2);
		}

		$amt = new \PayPal\Api\Amount();
		$amt->setTotal($suborder_cost)
		  ->setCurrency(Yii::$app->params['SYSTEM_CURRENCY']);

		$refund = new \PayPal\Api\Refund();
		$refund->setAmount($amt);

		$sale = new \PayPal\Api\Sale();
		$sale->setId($paypaldetails->sale_id);

		try 
		{
			$refundedSale = $sale->refund($refund, $apiContext);

			Yii::$app->session->setFlash('success', 'Refund Successful! Please note down your refund reference number: '.$refundedSale->getId());
		}
		catch (Exception $e) 
		{
			Yii::$app->session->setFlash('error', 'Paypal refund failed with error: '.$e->getMessage());
			return 0;
		}
		
		try
		{
			// Capture refund details in system
			$refunddetails = new PaypalRefundDetails;
			$refunddetails->order_id = $order->id;
			$refunddetails->sub_order_id = $suborder->id;
			$refunddetails->amount = $suborder->total_cost;
			$refunddetails->result_json = strval($refundedSale);
			$refunddetails->refund_id = $refundedSale->getId();
			$refunddetails->added_at = time();

			$refunddetails->save();

			// Update sub order status and order status (if applicable)
			$suborder->sub_order_status = OrderStatus::_REFUNDED;
			$suborder->save();

			$suborders = SubOrder::find()->where("order_id=".$order->id)->all();
			$safe = true;

			foreach($suborders as $row)
			{
				if($row->sub_order_status != OrderStatus::_REFUNDED && $row->sub_order_status != OrderStatus::_CANCELED)
				{
					$safe = false;
					break;
				}
			}
			
			if($safe)
			{
				$order->order_status = OrderStatus::_REFUNDED;
				$order->save();
			}
		}
		catch (Exception $e)
		{
			Yii::$app->session->setFlash('error', 'Your refund is successful but we enountered some issue with order. Please contact customer support for further assistance! Error message:'.$e->getMessage());
		}

		return 0;
	}

	public static function releaseAndRefund($suborder)
	{
		// Release inventory stock
		MulteModel::releaseInventoryStock($suborder);

		// Release Coupon Discount amount
		MulteModel::releaseCouponDiscount($suborder);

		// Release Global Discount Amouont
		MulteModel::releaseGlobalDiscount($suborder);

		if ($suborder->payment_method != PaymentMethods::_COD)
		{
			// Process refund
			if(($suborder->payment_method == PaymentMethods::_PAYPAL))
			{
				MulteModel::issuePaypalRefund($suborder);
			}
			else
			if(($suborder->payment_method == PaymentMethods::_STRIPE))
			{
				MulteModel::issueStripeRefund($suborder);
			}
		}
	}

	public static function getTimezoneList()
	{
		static $regions = array(
			\DateTimeZone::AFRICA,
			\DateTimeZone::AMERICA,
			\DateTimeZone::ANTARCTICA,
			\DateTimeZone::ASIA,
			\DateTimeZone::ATLANTIC,
			\DateTimeZone::AUSTRALIA,
			\DateTimeZone::EUROPE,
			\DateTimeZone::INDIAN,
			\DateTimeZone::PACIFIC,
		);

		$timezones = array();
		foreach( $regions as $region )
		{
			$timezones = array_merge( $timezones, \DateTimeZone::listIdentifiers( $region ) );
		}

		$timezone_offsets = array();
		foreach( $timezones as $timezone )
		{
			$tz = new \DateTimeZone($timezone);
			$timezone_offsets[$timezone] = $tz->getOffset(new \DateTime);
		}

		// sort timezone by offset
		asort($timezone_offsets);

		$timezone_list = array();
		foreach( $timezone_offsets as $timezone => $offset )
		{
			$offset_prefix = $offset < 0 ? '-' : '+';
			$offset_formatted = gmdate( 'H:i', abs($offset) );

			$pretty_offset = "UTC${offset_prefix}${offset_formatted}";

			$timezone_list[$timezone] = "(${pretty_offset}) $timezone";
		}

		return $timezone_list;
	}

	public static function readInputSheet($inventory_id, $inputFileName)
	{
		try
		{
			//$inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($inputFileName);

			/**  Create a new Reader of the type that has been identified  **/
			//$reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

			/**  Load $inputFileName to a Spreadsheet Object  **/
			$spreadsheet = $reader->load($inputFileName);
			
			$sheetData = $spreadsheet->getSheet(0)->toArray();
			//print_r($sheetData);exit;
			$count = 0;
			foreach ($sheetData as $row)
			{
				$license_model = new LicenseKeyCode;
				$license_model->inventory_id = $inventory_id;
				$license_model->license_key_code = $row[0];
				$license_model->used = 0;
				$license_model->save();
				$count++;
			}

			return $count;
		}
		catch (\Exception $e)
		{
			throw new \Exception(Yii::t('app', 'Error reading input file.').' '.Yii::t('app', 'Make sure it is an excel sheet in xlsx format.').' '.Yii::t('app', 'All data need to be in 1st column of 1st sheet of workbook.').' '.Yii::t('app', 'There should be one row for each record!'));
		}
	}

	public  static function getPendingTicketCountLabel()
	{
		return TicketModel::find()->where("user_assigned_id=".Yii::$app->user->identity->id." and (ticket_status='".TicketStatus::_NEEDSACTION."' or ticket_status='".TicketStatus::_INPROCESS."' or ticket_status='".TicketStatus::_REOPENED."')")->count();
	}

	public static function getPlan($id){
	    $plan = PaymentPlanTemplate::findOne($id)->plan_name;
	    return $plan;
    }

    public static function getAdvance($price,$plan_id){

            $query = PaymentPlanTemplateDetail::find()->where(['=','payment_plan_template_id',$plan_id])->orderBy(['id'=>'ASC'])->all();
            /* echo '<pre>';
             echo  print_r($query);
             echo '</pre>';*/
           // $count = 0;
            $total =0;

            foreach ($query as $val){

                if($val->format == 'rs'){
                    if($val->type == 'initial'){

                        $installment['installment0'] = $val->payment_amount;
                        $total +=  $val->payment_amount;
                    }
                    return $total;
                    /*$installment['total'] = $total;
                    $response = Yii::$app->response;
                    $response->format = \yii\web\Response::FORMAT_JSON;
                    $response->data = $installment;*/

                }
                else{
                    if($val->type == 'initial'){
                        $installment['installment0']  = $price *$val->payment_amount /100;
                        $total +=  $installment['installment0'];
                    }
                    /*else{
                        $installment['installment'.$count]  = $price * $val->payment_amount /100;
                        $total += $installment['installment'.$count];
                    }*/
                    return  $total;
                    /*$response = Yii::$app->response;
                    $response->format = \yii\web\Response::FORMAT_JSON;
                    $response->data = $installment;*/
                }
                /*$resp['type'.$count]= $val->type;
                $resp['format'.$count]= $val->format;
                $resp['amount'.$count] =  $val->payment_amount;
                $response = Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = $resp;*/
               // $count++;


        }
    }

    public static function getEmi($price){
        $plan_id = 3;
        $query = PaymentPlanTemplateDetail::find()->where(['=','payment_plan_template_id',$plan_id])->orderBy(['id'=>'ASC'])->all();
        /* echo '<pre>';
         echo  print_r($query);
         echo '</pre>';*/
        // $count = 0;
        $total =0;

        foreach ($query as $val){

            if($val->format == 'rs'){
                if($val->type == 'initial'){

                    // $installment['installment0'] = $val->payment_amount;
                    $total +=  $val->payment_amount;
                }else{
                    $total  = $val->payment_amount;
                    //$total +=  $val->payment_amount;
                    return (int)$total;
                    break;
                }

                /*$installment['total'] = $total;
                $response = Yii::$app->response;
                $response->format = \yii\web\Response::FORMAT_JSON;
                $response->data = $installment;*/

            }
            else{
                if($val->type == 'initial'){
                    $installment['installment0']  = $price *$val->payment_amount /100;
                    $total +=  $installment['installment0'];
                }
                else{
                    $total  = $price * $val->payment_amount /100;
                    //$total += $installment['installment'.$count];
                    return (int)$total;
                    break;
                }

            }



        }
    }
}