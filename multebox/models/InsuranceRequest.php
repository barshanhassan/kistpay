<?php

namespace multebox\models;

use Yii;

/**
 * This is the model class for table "insurance_request".
 *
 * @property int $id
 * @property int $customer_id
 * @property int $order_id
 * @property string $modal_no
 * @property string $serial_no
 * @property string $IMEI1
 * @property string $IEMI2 description
 * @property string  description
 * @property int $status
 * @property string $prod_image
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class InsuranceRequest extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurance_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['customer_id', 'order_id', 'modal_no', 'serial_no', 'IMEI1', 'IEMI2', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'created_by','serial_no', 'IMEI1',], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['prod_image'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer',
            'order_id' => 'Order #',
            'modal_no' => 'Modal #',
            'serial_no' => 'Serial #',
            'IMEI1' => 'IEMI 1',
            'IEMI2' => 'IEMI 2',
            'status' => 'Status',
            'description' => 'Description',
            'prod_image' => 'Product Image',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    public function getCustomer(){
        return $this->hasOne(Customer::className(),['id'=>'customer_id']);
    }

    public function getPlan(){
        return $this->hasOne(PaymentPlanTemplate::className(),['id'=>'plan_id']);
    }
    public function  getBank(){
        return $this->hasOne(Banks::className(),['id'=>'bank_id']);
    }

    public function getOrderItems(){
        return $this->hasMany(OrderItems::className(),['order_id'=>'id']);
    }
}
