<?php

namespace multebox\models;

use Yii;

/**
 * This is the model class for table "insurance".
 *
 * @property int $id
 * @property string $name
 * @property string $mobile
 * @property int $ntn
 * @property string $email
 * @property string $city
 * @property string $adress
 * @property string $logo
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class Insurance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'insurance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ntn', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'created_by','mobile','name', 'city', 'adress',], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'mobile', 'email', 'city', 'adress', 'logo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mobile' => 'Contact',
            'ntn' => 'Ntn',
            'email' => 'Email',
            'city' => 'City',
            'adress' => 'Address',
            'logo' => 'Logo',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
