<?php

namespace multebox\modules\multeobjects\controllers;

use Yii;
use multebox\models\PrdLic;
use multebox\models\PrdMdl;
use multebox\models\PrdMdlLic;
use multebox\models\search\PrdLic as PrdLicSearch;
use multebox\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PrdLicController implements the CRUD actions for PrdLic model.
 */
class PrdLicController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }
public function init(){
	}
    /**
     * Lists all PrdLic models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PrdLicSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single PrdLic model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['view', 'id' => $model->id]);
        } else {
        return $this->render('view', ['model' => $model]);
}
    }

    /**
     * Creates a new PrdLic model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PrdLic;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PrdLic model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		$prdMdls =PrdMdl::find()->where("NOT EXISTS(Select *
FROM tbl_prd_mdl_lic  WHERE prd_lic_id =".$model->id." and prd_mdl_id=tbl_prd_mdl.id) and mdl_status=1")->orderBy('mdl_name')->asArray()->all();
		if(!empty($_POST['mdls'])){
			foreach($_POST['mdls'] as $value){
					$obj = new PrdMdlLic();
					$obj->prd_lic_id = $model->id;
					$obj->prd_mdl_id = $value;
					$obj->save();
					
			}
			 return $this->redirect(['update', 'id' => $model->id]);
		}
		if(!empty($_GET['del_id'])){
			$del_obj  = PrdMdlLic::findOne($_GET['del_id']);
			if(!is_null($del_obj)){
				$del_obj->delete();
				 return $this->redirect(['update', 'id' => $model->id]);
			}
		}
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
				'prdMdls'=>$prdMdls,
            ]);
        }
    }

    /**
     * Deletes an existing PrdLic model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PrdLic model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PrdLic the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PrdLic::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
    }
}
