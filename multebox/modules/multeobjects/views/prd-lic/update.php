<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var multebox\models\PrdLic $model
 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Prd Lic',
]) . ' ' . $model->prd_lic_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Prd Lics'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->prd_lic_name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<script>
$(document).ready(function(e) {
    $('#prdlic-prd_lic_date-disp').val($('#prdlic-prd_lic_date').val()=='0000-00-00'?'':'<?=date('Y/m/d',strtotime($model->prd_lic_date))?>');
});
</script>
<script type="text/javascript">

$(document).ready(function(){


    $('.modules').appendTo('#w0');
})
</script>
<div class="prd-lic-update">

        <div class="ibox-title">
            <h5> <?=$this->title ?></h5>

            <div class="ibox-tools">

                <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                </a>
               
                <a class="close-link">
                    <i class="fa fa-times"></i>
                </a>
            </div>
</div>
         <div class="ibox-content">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div></div>
<div class="modules">
 <?php

                                        

                        $searchModel = new \multebox\models\search\PrdMdlLic;
                        $dataProvider = $searchModel->searchPrdMdls( Yii::$app->request->getQueryParams (), $model->id );

                        echo Yii::$app->controller->renderPartial("module_tab", [ 

                                'dataProvider' => $dataProvider,

                        ] );

                        
echo Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']);

		echo ' <a href="javascript:void(0)" class="btn btn-success btn-sm" onClick="$(\'.mdlae\').modal(\'show\');">'.Yii::t('app', 'Add Modules to Licence').'</a>';
		echo "</form>";
                        ?>
</div>
<?php
include_once('mdlae.php');
?>