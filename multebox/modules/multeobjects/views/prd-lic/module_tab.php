<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
?>
<div class="prd-mdl-lic-index">
    <?php Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
		'pjax' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'prd_lic_id',
           // 'prd_mdl_id',
			[ 

				'attribute' => 'prd_mdl_id',

				'label' => Yii::t('app', 'Module Name'),

				'format' => 'raw',

				'value' => function ($model, $key, $index, $widget)

				{
						if(isset($model->module->mdl_name))
						return $model->module->mdl_name;

				} 

		],
		[ 

				'attribute' => 'prd_mdl_id',

				'label' => Yii::t('app', 'Module Description'),

				'format' => 'raw',

				'value' => function ($model, $key, $index, $widget)

				{
						if(isset($model->module->mdl_desc))
						return $model->module->mdl_desc;

				} 

		],

            [
                'class' => '\kartik\grid\ActionColumn',
                'buttons' => [
				'view'=>function ($url, $model) { return '';},
                'update' => function ($url, $model) {
                                    return '';
									},
				'delete'=> function ($url,$model){
					$path = Url::to(['/multeobjects/prd-lic/update', 'id' => $_REQUEST['id'], 'del_id' => $model->id]);
					return '<a href="'.$path.'" onClick="return confirm(\''.Yii::t('app','Are you Sure!').'\')"><i class="fa fa-trash"></i></a>	';
				}

                ],
            ],
        ],
        'responsive'=>true,
        'hover'=>true,
        'condensed'=>true,
        'floatHeader'=>false,




        'panel' => [
            'heading'=>'<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '.Yii::t('app','Modules').' </h3>',
            'type'=>'info',
            'before'=>' <a href="javascript:void(0)" class="btn btn-success btn-sm" onClick="$(\'.mdlae\').modal(\'show\');">'.Yii::t('app', 'Add Modules to Licence').'</a>',                                                                                                                                                          'after'=>false,
            'showFooter'=>false
        ],
    ]); Pjax::end(); ?>

</div>
