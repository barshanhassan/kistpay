(function ($) {
"use strict";

$('.slideshow').owlCarousel({
	items: 6,
	autoPlay: 2900,
	singleItem: true,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
	pagination: true
});

$(".owl-carousel.product_carousel, .owl-carousel.latest_category_carousel, .owl-carousel.latest_brands_carousel, .owl-carousel.related_pro").owlCarousel({
		itemsCustom : [[320, 2],[600, 3],[768, 4],[992, 6],[1199, 6]],											   
		lazyLoad : true,
		navigation : true,
		navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		scrollPerPage : true
    }); 

$('#cat_accordion').cutomAccordion({
		saveState: false,
		autoExpand: true
	});

$('#menu .nav > li > .dropdown-menu').each(function() {
		var menu = $('#menu .nav.navbar-nav').offset();
		var dropdown = $(this).parent().offset();

		var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu .nav.navbar-nav').outerWidth());

		if (i > 0) {
			$(this).css('margin-left', '-' + (i + 5) + 'px');
		}
	});

var $screensize = $(window).width();
$('#menu .nav > li, #header .links > ul > li').on("mouseover", function() {
																		
			if ($screensize > 991) {
			$(this).find('> .dropdown-menu').stop(true, true).slideDown('fast');
			}			
			$(this).bind('mouseleave', function() {

			if ($screensize > 991) {
				$(this).find('> .dropdown-menu').stop(true, true).css('display', 'none');
			}
		});});
$('#menu .nav > li div > ul > li').on("mouseover", function() {
			if ($screensize > 991) {
			$(this).find('> div').css('display', 'block');
			}			
			$(this).bind('mouseleave', function() {
			if ($screensize > 991) {
				$(this).find('> div').css('display', 'none');
			}
		});});
$('#menu .nav > li > .dropdown-menu').closest("li").addClass('sub');

$( document ).ready(function() {
  $screensize = $(window).width();
    if ($screensize > 1199) {
        $('#menu .nav > li.mega-menu > div > .column:nth-child(6n)').after('<div class="clearfix visible-lg-block"></div>');
    }
    if ($screensize < 1199) {
        $('#menu .nav > li.mega-menu > div > .column:nth-child(4n)').after('<div class="clearfix visible-lg-block visible-md-block"></div>');
  }
});
$( window ).resize(function() {
    $screensize = $(window).width();
    if ($screensize > 1199) {
        $("#menu .nav > li.mega-menu > div .clearfix.visible-lg-block").remove();
        $('#menu .nav > li.mega-menu > div > .column:nth-child(6n)').after('<div class="clearfix visible-lg-block"></div>');
    } 
    if ($screensize < 1199) {
        $("#menu .nav > li.mega-menu > div .clearfix.visible-lg-block").remove();
        $('#menu .nav > li.mega-menu > div > .column:nth-child(4n)').after('<div class="clearfix visible-lg-block visible-md-block"></div>');
    }
});

$('#menu .navbar-header > span').on("click", function() {
	  $(this).toggleClass("active");  
	  $("#menu .navbar-collapse").slideToggle('medium');
	  return false;
	});

$('#menu .nav > li > div > .column > div, .submenu, #menu .nav > li .dropdown-menu').before('<span class="submore"></span>');

$('span.submore').click(function (e) {
         e.preventDefault();
        var $parentli = $(this).closest('li, .column');
        $parentli.siblings('li, .column').find('div.dropdown-menu:visible, > div:visible').slideUp();
        $parentli.find('> div.dropdown-menu, > div').stop().slideToggle();
        $parentli.siblings('li, .column').find('span.submore.plus').toggleClass('plus');
        $(this).toggleClass('plus');
    });

$('.drop-icon').on("click", function() {
	  $('#header .htop').find('.left-top').slideToggle('fast');
	  return false;
	});

$(".qtyBtn").on("click", function() {
		if($(this).hasClass("plus")){
			var qty = $(".qty #input-quantity").val();
			qty++;
			$(".qty #input-quantity").val(qty);
		}else{
			var qty = $(".qty #input-quantity").val();
			qty--;
			if(qty>0){
				$(".qty #input-quantity").val(qty);
			}
		}
		return false;
	});	


$('[data-toggle=\'tooltip\']').tooltip({container: 'body'});


$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 150) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});
		});
$('#back-top').on("click", function() {
	$('html, body').animate({scrollTop:0}, 'slow');
	return false;
});

})(jQuery);