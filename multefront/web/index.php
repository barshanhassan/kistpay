<?php

include_once("env.php");

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../multebox/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../multebox/config/main.php'),
    require(__DIR__ . '/../../multebox/config/main-local.php'),
    require(__DIR__ . '/../../multebox/config/datecontrol-module.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

$application = new yii\web\Application($config);

loadMulteDBConfigItems();
setMulteSessionParams();

function loadMulteDBConfigItems()
{
		$items = multebox\models\ConfigItem::find()->asArray()->all();
		foreach ($items as $item)
        {
            if ($item['config_item_name'])
			{
				Yii::$app->params[$item['config_item_name']] = $item['config_item_value'];
				Yii::$app->params[$item['config_item_name']."_description"] = $item['config_item_description'];
			}
        }
		
		$company = multebox\models\Company::find()->asArray()->one();
		Yii::$app->params['company'] = $company;
		Yii::$app->params['address']= multebox\models\search\Address::companyAddress($company['id']);

		Yii::$app->params['zero_decimal_currencies'] = ['MGA','BIF','CLP','PYG','DJF','RWF','GNF','JPY','VND','VUV','XAF','KMF','KRW','XOF','XPF'];
}

function setMulteSessionParams()
{
	$_SESSION['SHOW_DEBUG_TOOLBAR'] = 'No';
	$_SESSION['LOCALE']='en-US';
}

$application->run();