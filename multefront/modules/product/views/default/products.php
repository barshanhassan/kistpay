<style>
    .form-control{
        width: 81% !important;
        float: left;
        border-radius: 0px;

    }
    p.pag {
    width: 58px;
    float: left;
}



    .col-5.mb-4.mt-4 {
    text-align: center;
    }
    .loan{
        zoom: 0.9;
        transform: scale(0.9);
        margin: -40px auto;



    }

</style>

<?php
use multebox\models\ProductBrand;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\Inventory;
use multebox\models\search\MulteModel;
use multebox\models\Product;
use multebox\models\File;
use yii\helpers\Url;

include_once('../web/cart_script.php');
$lang = $_REQUEST['lang'];
?>


<div class="loan-form">
    <section class="mt-6">
        <div class="container-fluid">
            <div class="border-0 card">
                <div class="row">
                    <div class="col-sm-12">
                        <article>
                            <h1 class="centr font"><?=Yii::t('app','Exclusive Products')?><br><?=Yii::t('app','Available for You')?></h1></br>
                            <div class="img-big-wrap m-3">


                                <div> <img style="width: 105%;" src="<?=Yii::$app->homeUrl?>img/bg-productpage.PNG" class="product-page-mainimage"></div>
                            </div>
                        </article>
                    </div>
                    <div class="col-sm-12">
                        <!-- card-body.// -->
                    </div>
                    <!-- col.// -->
                </div>
                <!-- row.// -->
            </div>
        </div>
        <div class="container-fluid loan">
            <div class="row">
                <div class="col-12 col-sm-3 ml-3 ml-5 mr-3">
                    <div class="card bg-light mb-3 shop-by">
                        <div class="bg-orange bg-primary card-header text-uppercase text-white">SHOP BY</div>
                        <ul class="list-group category_block">
                            <li class="list-group-item">
                                <h5>Category </h5>
                                <?php $catgs = \multebox\models\ProductCategory::find()->all();
                                $item = $_REQUEST['s_cat'];
                                $cat_item = $_REQUEST['cat'];
                                foreach($catgs as $catg){  ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" <?php echo ($cat_item ==$catg->id)? 'checked':'' ?> onclick="catsearch(this)" id="<?=$catg->id?>" value="<?=$catg->id?>" ><i class="helper"></i><?=$catg->name?></label>
                                </div>
                                <?php }  ?>
                            </li>
                        </ul>
                    </div>
                    <div class="card bg-light mb-3 price-range">
                        <ul class="list-group category_block">
                            <li class="list-group-item">
                                <h5>Price Range</h5>
                            </li>
                            <li class="list-group-item pr-0">
                                <span class="d-inline-flex">
                                    <form method="POST" action="<?=Yii::$app->homeUrl?>product/default/pricefilter">
                                        <input style="width: 19% !important;" type="text" name="price_1" class="form-control mr-1"  value="0">
                                        <input style="width: 19% !important;" type="text" name="price_2"  class="form-control mr-1" value="40000">
                                        <button type="submit" class="btn btn-lg btn-primary details-btn mr-2">SEARCH</button>
                                    </form>
                                </span>
                                <span class="">
                                    <div class="mt-4 rangeslider">
                                        <form method="POST" action="<?=Yii::$app->homeUrl?>product/default/pricefilter" id="range_form">
                                        <input class="min" name="price_1" type="range" min="1" max="100000" id="rangeinput_1" value="100" oninput="rangeoutput_1.value = rangeinput_1.value" onchange="submitform();">
                                        <input class="max" name="price_2" type="range" min="1" max="100000" id="rangeinput_2" value="30000" oninput="rangeoutput_2.value = rangeinput_2.value" onchange="submitform();">
                                        <span class="range_min light left">Rs.<output name="ageOutputName" id="rangeoutput_1">100</output></span>
                                        <span class="range_max light right">Rs.<output name="ageOutputName" id="rangeoutput_2">30000</output></span>
                                        </form>
                                    </div>
                                </span>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"><i class="helper" onclick="loan();"></i>Purchase on Loan</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card bg-light mb-3 manufacturers-filter">
                        <ul class="list-group category_block">
                            <li class="list-group-item">
                                <h5>Manufacturers</h5>
                                <?php $subcats = \multebox\models\ProductSubCategory::find()->all();
                                $item = $_REQUEST['s_cat'];
                                foreach($subcats as $subcat){  ?>

                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="<?=$subcat->id?>" onclick="search(this)" <?php echo ($item ==$subcat->id)? 'checked':'' ?> value="<?=$subcat->id?>"><i class="helper"></i><?= $subcat->name?>
                                    </label>
                                </div>
                               <?php  }?>

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col">
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-4-5">
                                <form method="post" action="<?=Yii::$app->homeUrl?>product/default/search">
                                <input type="hidden" name="_csrf" value="4gMfWqlGpn1MjdBoKH0IUGakL4mB7Nabn2AuNVefXuG4TVU1zyPDHwTOkj1fMWA7IZwdx7ihl_T7I31PFvBoqg==">
                                <input type="text" id="filter_name" class="form-control input-search" name="searchbox" placeholder="Search here....." value="<?=$srch_text?>">
                                <div class="input-group-append">
                                    <button class="bg-orange border-0 btn btn-secondary for-icon-border"
                                            type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-4-5">
                                <div class="nav-item dropdown">
                                    <div id="wrapper">
                                        <p class="pag">Sort By:</p>
                                      <select class="sort" id="sorting">
                                        <option value="name_asc" <?php echo ($_REQUEST['sort'] =='name_asc')? 'selected':'' ?>>by name ascending a-z</option>
                                        <option value="name_desc" <?php echo ($_REQUEST['sort'] =='name_desc')? 'selected':'' ?>>by name decending z-a</option>
                                          <option value="price_asc" <?php echo ($_REQUEST['sort'] =='price_asc')? 'selected':'' ?>>by price ascending</option>
                                          <option value="price_desc" <?php echo ($_REQUEST['sort'] =='price_desc')? 'selected':'' ?>>by price decending</option>
                                      </select>
                                    </div>
                                    <div class="dropdown-menu dropdown-primary"
                                         aria-labelledby="navbarDropdownMenuLink"
                                         style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 40px, 0px);"
                                         x-placement="bottom-start">
                                        <a class="dropdown-item waves-effect waves-light" href="#">Action</a>
                                        <a class="dropdown-item waves-effect waves-light" href="#">Another
                                            action</a>
                                        <a class="dropdown-item waves-effect waves-light" href="#">Something else
                                            here</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="block ml-5">
                            <div class="mb-4-5 row">
                                <?php
                                if(empty($inventoryItemsList)){
                                     echo Yii::t('app','<h2>Sorry! Products are not available against your Search</h2>');

                                 }
                                foreach($inventoryItemsList as $inventoryItem)
                                {
                                $inventoryPrice = floatval($inventoryItem->price);
                                if($inventoryItem->price_type == 'B')
                                {
                                    foreach(json_decode($inventoryItem->attribute_price) as $row)
                                    {
                                        $inventoryPrice += floatval($row);
                                    }
                                }

                                if($inventoryItem->discount_type == 'P')
                                    $inventoryDiscount = $inventoryItem->discount;
                                else
                                {
                                    if($inventoryPrice > 0)
                                        $inventoryDiscount = round((floatval($inventoryItem->discount)/$inventoryPrice)*100,2);
                                    else
                                        $inventoryDiscount = 0;
                                }

                                $inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

                                $fileDetails = File::find()->where("entity_type='product' and entity_id= $inventoryItem->product_id")->one();
                                    if($lang=='ur-UR'){
                                        $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id,'lang'=>'ur-UR']);
                                    }else{
                                        $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id]);
                                    }
                                ?>

                                <div class="col-6">
                                    <div class="mr-2 span4">
                                        <div class="row">
                                            <div class="col-5 mb-4 mt-4">
                                                <div class="content-heading">
                                                    <h5 class="productname-moretiles"><a href="<?=$url?>"><?=$inventoryItem->product_name?></h5>
                                                </div>
                                                <p class="font"><?=Yii::t('app','Full Price')?>: Rs <?=MulteModel::formatAmount($inventoryDiscountedPrice)?></p>
                                                <?php if($inventoryDiscountedPrice > 5000){   ?>
                                                    <p class="card-text m-0 font"><?=Yii::t('app','EMI as low as')?>: Rs <?=MulteModel::formatAmount(MulteModel::getEmi($inventoryDiscountedPrice))?></p>

                                                <?php } ?>                                                <?php if($inventoryItem->stock > 0)
                                                {
                                                    ?>
                                                    <a href="<?=$url?>" class="btn btn-lg btn-primary details-btn mr-2 font"><?=Yii::t('app','DETAILS')?></a>
                                                <?php }
                                                else{?>
                                                    <a href="<?=$url?>" class="btn btn-lg btn-primary details-btn mr-2 font"><?=Yii::t('app','Out of Stock')?></a>                                                <?php }
                                                ?>
                                            </div>
                                            <div class="col-5"><a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="bitmap3" /></a></div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    function search(item){

            var selected = item.value;

            $("#"+selected).change("click", function () {

                if (this.checked)
                {
                    window.location='<?=Yii::$app->homeUrl?>product/default/listing?s_cat='+selected;
                }
                else if(!this.checked)
                {
                    window.location='<?=Yii::$app->homeUrl?>product/default/index';
                }

            });

    }

    function catsearch(item){

        var selected = item.value;

        $("#"+selected).change("click", function () {

            if (this.checked)
            {
                window.location='<?=Yii::$app->homeUrl?>product/default/listing?cat='+selected;
            }
            else if(!this.checked)
            {
                window.location='<?=Yii::$app->homeUrl?>product/default/index';
            }

        });

    }

    function submitform(){
        $("#range_form").submit();
    }
    function loan() {
        window.location='<?=Yii::$app->homeUrl?>product/default/index';
    }

    var urlmenu = document.getElementById( 'sorting' );

        urlmenu.onchange = function() {

            window.location='<?=Yii::$app->homeUrl?>product/default/filter?sort='+urlmenu.value;
        //window.open( 'viewclass.php?classname=' + this.options[ this.selectedIndex ].value );
    };
</script>
