<?php
use yii\helpers\Html;

$this->title = 'Contract';
/**
 * Created by PhpStorm.
 * User: MCE-PC
 * Date: 21-Nov-19
 * Time: 9:34 AM
 */?>
&nbsp;


<style>

    body {
        padding-top: 8px;
        padding-bottom: 20px;
        background: #efefef !important
    }

    .container1{ margin: 0 auto; width: 730px; background:#efefef}
    .row-top{width: 100%;}
    .col-left-reg{ float: left; width: 34%; font-size:8.5pt}
    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .col-center{float: left; width: 34%;}
    .logo{ width: 100%; height: auto;}
    .col-pic{float: left; width: 30%;}
    .pic{ float: right; width: 130px; height: 140px; border: 1px solid #000; margin-right: 5px;}
    .pic img{width: 100%; height: 100%}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif;}
    .span{ background: #f5f5f5; padding:5px; margin: 0px;}

    .d-line{width: 100%; float: left; background:#f5f5f5; padding:3px; margin-top:3px;font-size: 8pt}
    .d-line span{padding:3px; font-weight: bold; font-size: 8pt}
    .half-left{ float: left; width: 60%; }
    .half-right{ float: left; width: 40%; }
    .membership{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt;margin-top:8px;}
    .membership h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}

    .bill-d{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt; margin-top: 10px;}
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}
    #b-line{width: 100%; float: left; padding:3px; margin-top:2px;}
    .b-d-left{float: left; width: 45%;background:#f5f5f5; padding: 2px; }
    .b-d-right{float: right; width: 43%;background:#f5f5f5; padding: 2px; }
    #b-line span{padding:3px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center}


    .text-center{ text-align:center;}
    .d-line abel {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 0px;
        font-weight: bold;
    }

    .col-lg-4 {
        width: 33.33333333%;
        float: left;
    }
    @media print {
        .col-md-3{width: 25%;}
        .col-md-6{width: 50%;}


    }

    /*Uzair Code*/

    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .form{float: left; width: 100%;margin-top: 23px; margin-left: 4px;}
    .col-center{margin-top: 13px;}
    .centerpad{padding-left: 10px; padding-right: 10px;margin-top: 0px;margin-bottom:0;}

    .terms{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:7.9pt; padding-left: 10px;
        padding-top: 4px !important; background: #efefef !important;}
    .terms h2{ text-align: center; margin: 3px; }
    .terms ul{list-style-type:decimal;}
    .terms ul li{ margin-bottom: 3px;}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif;font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif; font-size: 20px;}
    .span{ background: #f5f5f5; padding:5px; margin: 0px;}
    .subspan{font-size: 9pt;}

    .half-left{ float: left; width: 60%; }
    .half-right{ float: left; width: 40%; }

    .secondpage{ }
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; }
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}

    #b-line span{padding:5px 0px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center;background: transparent !important;}

    .text-center{ text-align:center;}



    .top10{
        margin-top: 0px !important;
    }
    /************ For Printing *****/
    @media print {

        body{background: #efefef !important}

        .container1{ margin: 0 auto; width: 730px; background:#efefef !important}
        .form{float: left !important; width: 100% !important;margin-top: 20px !important; margin-top: 23px !important;margin-left: 4px !important;}
        .centerpad{padding-left: 10px; padding-right: 20px;margin-top: 0px;margin-bottom:0 !important;}
        .d-line{width: 100%; float: left; background:#f5f5f5 !important; padding:3px; margin-top:3px;font-size: 8pt}
        .d-line span{padding:3px; font-weight: bold; font-size: 8pt}
        .col-pic{float: left; width: 30%;}
        .pic{ float: right; width: 130px; height: 140px; border: 1px solid #000; margin-right:3px; }
        .pic img{width: 100%; height: 100%;}

        .logo{ width: 100% !important; height: auto !important;}

        .detail{ width: 100% !important; float: left !important; font-family: Verdana, Arial, Helvetica, sans-serif !important; font-size:9pt !important}
        .detail h2{margin: 0 0 5px 0 !important; font-family: "Times New Roman", Georgia, Serif !important;}
        .span{ background: #f5f5f5 !important; padding:5px !important; margin: 0px !important;}
        .subspan{font-size: 9pt !important;}
        .d-line span{padding:3px !important; font-weight: bold !important}
        .half-left{ float: left !important; width: 60% !important; }
        .half-right{ float: left !important; width: 40% !important; }
        .p-d1{float: left !important; width: 30% !important;}
        .p-d2{float: left !important; width: 30% !important;}
        .p-d3{float: left !important; width: 40% !important;}
        .p-d21{float: left !important; width: 25% !important;}
        .p-d22{float: left !important; width: 25% !important;}
        .p-d23{float: left !important; width: 25% !important;}
        .p-d24{float: left !important; width: 25% !important;}
        .bill-d h2{margin: 5px 0 5px 0 !important; font-family:"Times New Roman", Georgia, Serif !important;}
        .bill-d h3{margin: 5px 0 2px 0 !important; font-family:"Times New Roman", Georgia, Serif !important; font-size:16pt !important}
        #b-line{width: 100% !important; float: left !important; padding:1px 0px !important;}
        .b-d-left{float: left !important; width: 43% !important; background:#f5f5f5 !important; padding: 2px !important;}
        .b-d-right{float: right !important; width: 43% !important;background:#f5f5f5 !important; padding: 2px !important; }
        #b-line span{padding:5px 0px !important; font-weight: bold !important}
        .b-d-sign{float: right !important; width: 43% !important; }
        .sign{ border-top: 1px solid #000 !important; padding-top: 10px !important; text-align: center !important;background: transparent !important;}
        .d-line abel {
            display: inline-block;
            max-width: 100%;
            margin-bottom: 0px;
            font-weight: bold;
        }
        .row-top {
            margin-bottom:10px !important;    margin-top: -30px !important;
        }
        .col-top-1 {border-right: 1px solid #cccccc !important;

        }
        .cat-form{ border-bottom: 1px solid #000 !important; display: inline !important; }

        .text-center{ text-align:center !important;}

        .cal-bottom{margin-bottom:8px !important;}

        .row-eq-height {
            display: -webkit-box !important;
            display: -webkit-flex !important;
            display: -ms-flexbox !important;
            display:         flex !important;
        }

        .calculator-button,
        .calculator-button:focus {
            background-color: rgb(233,76,111) !important;
            padding: 21px !important;
            font-size: 26px !important;
        }

        .calculator-button:hover {
            background-color: rgb(198,213,205) !important;
        }

        .calculator-button,
        .calculator-button:hover,
        .calculator-button:focus {
            border:none !important;
            outline:none !important;
        }

        #outer-calc {
            background-color: rgb(84,39,51) !important;
            padding-left: 50px !important;
            padding-right: 50px !important;
            padding-top: 50px !important;
            padding-bottom: 50px !important
        }

        .button-column{
            margin-bottom: 8px !important;
        }
        .p-btn-column{
            padding: 2px 1px !important;
        }

        .amount {
            color: #fff !important;
            background-color: orange !important;
            border-color: orange !important;
        }

        .liter {
            color: #fff !important;
            background-color: #60c441 !important;
            border-color: #60c441 !important;
        }

        .rate {
            color: #fff !important;
            background-color: #434343 !important;
            border-color: #434343 !important;
        }
        .bottom{    margin-bottom: 7px !important;}
        .navbar {
            margin-bottom: 20px !important;
        }
        .jumbotron {
            text-align: center !important;
            background-color: transparent !important;
        }
        .footer {
            padding-top: 10px !important;
            padding-bottom: 10px !important;
            margin-top: 20px !important;
            border-top: 1px solid #eee !important;
            background: #f8f8f8 !important;
            text-align: center !important;
        }
        @page {size: portrait !important;margin: inherit !important;}
        .top10{

        }
        .mainhead {
            border-bottom: 1px black solid !important;
            padding-bottom: 5px !important;
            margin-bottom: 5px !important;
        }
    }

    @media print {

        .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
            float: left!important;
        }
        .col-lg-12 {
            width: 100% !important;
        }
        .col-lg-11 {
            width: 91.66666666666666% !important;
        }
        .col-lg-10 {
            width: 83.33333333333334% !important;
        }
        .col-lg-9 {
            width: 75% !important;
        }
        .col-lg-8 {
            width: 66.66666666666666% !important;
        }
        .col-lg-7 {
            width: 58.333333333333336% !important;
        }
        .col-lg-6 {
            width: 50% !important;
        }
        .col-lg-5 {
            width: 41.66666666666667% !important;
        }
        .col-lg-4 {
            width: 33.33333333333333% !important;
        }
        .col-lg-3 {
            width: 25% !important;
        }
        .col-lg-2 {
            width: 16.666666666666664% !important;
        }
        .col-lg-1 {
            width: 8.333333333333332% !important;
        }
        /*.watermark {
            color: BLACK !important;
            font-size: 96px !important;
            left: 35% !important;
            opacity: 0.21 !important;
            position: fixed !important;
            top: 50% !important;
            -ms-transform: rotate(-66deg) !important;
            -webkit-transform: rotate(-66deg) !important;
            transform: rotate(-66deg) !important;
            display: block !important;
        }*/
        .remarks{margin-top: 15px !important; margin-bottom: 15px !important; background: #efefef !important;}
        .remarks .billtitle {  font-size: 19px !important;  }
        .remarks .remarksblock {  height: 100px !important;border: 1px solid black !important;padding: 5px !important; float: left !important; white-space:pre-wrap !important  }
    }
    .blur-content {
        -webkit-filter: url(#svg-blur);
        filter: url(#svg-blur);

    }
    #Panes {
        position: relative;
        z-index: 10;
    }
</style>

<?php
//$cart = json_encode($cart_items);

/*echo '<pre>';
echo print_r($cart_items);
foreach ($cart_items as $val){
    echo $val;
}*/
  ?>

<div class="container1">
   <div class="centerpad">
        <div class="row">
            <div class="col-lg-12">
                <div class="top10">
                    <div class="col-left-reg  pad-left-right">
                        <div class="form">
                        </div>
                    </div>

                    <div class="col-center  pad-left-right text-center">
                        <div class="col-xs-12">
                            <h4>Digital Adgreement</h4>
                            <!--<img src="../web/images/logo.jpg"  width="200">-->
                        </div>


                    </div>

                </div>

                <div class="terms">
                    <br>
                    Please carefully read all of the membership policies and terms. We request all prospective members to adhere to the Kistpay’s
                    policies and procedures. Failure may lead to cancellation of membership without any refund or liability to the Kistpay.
                    <br><br>
                </div><!--close terms-->
            </div>
        </div><!-- close row-top-->

        <div class="blur-content">
            <div id="Panes">
                <div class="detail">
                    <h2><span class="span">Personal Detail</span></h2>
                    <div class="d-line">
                        <span>Name </span> <?php echo 'Barshan Hassan' ?>
                    </div>

                    <div class="d-line">
                        <span>Father Name </span> <?php echo "Hassan" ?>
                    </div>


                    <div class="d-line">
                        <span>Address </span> <?php echo "Islamabad" ?>,
                    </div>

                    <div class="d-line">
                        <span>Business Adress </span> <?php echo "Islamabad" ?>
                    </div>

                    <div class="d-line">
                        <span>Occupation </span> <?php echo "Job" ?>
                    </div>


                    <div class="d-line">
                        <div class="half-left"><span>Contact Number </span> <?php echo '03069091792'; ?></div>
                        <div class="half-right"><span>Email </span> <?php echo 'barshanhassan@gmail.com'; ?></div>
                    </div>

                    <div class="d-line">
                        <div class="half-left"><span>CNIC # </span> <?php echo '8978978784'; ?></div>


                    </div>

                </div><!-- personal Detail-->


                <div class="bill-d">
                    <!--<h2><span class="span">Office Use</span></h2>-->
                    <h2><span class="span">Billing Detail&nbsp;</span></h2>

                    <div id="b-line">
                        <div class="b-d-left"><span class="billtitle">Registration Charges</span> : <?php echo  '878'; ?></div>
                        <div class="b-d-right"><span class="billtitle">Registration Discount</span> : <?php echo  '30'; ?></div>
                    </div>

                    <div id="b-line">
                        <div class="b-d-left"><span class="billtitle">Membership Charges</span> : <?php echo  '100'; ?>	</div>
                        <div class="b-d-right"><span class="billtitle">Membership Discount</span> : <?php echo  '2000'; ?>	</div>
                    </div>
                </div><!-- Membership type-->


                <div class="uzair" style="page-break-before: always; float: left">&nbsp;
                </div>

                <div class="terms secondpage">
                    <h2>Rules & Regulations</h2>
                    <div class="">
                        <div class="terms">
                            <ul>
                                <li>
                                    Membership to any facility of Megazone is subject to the approval of Management, the Management reserves the right to reject any application for membership of the facility for any reason whatsoever
                                </li>
                                <li>
                                    Membership to any facility of Megazone is Non-transferable, Non Freezable and Non Refundable under any circumstances.
                                </li>
                                <li>
                                    No one is allowed to bring with them any sort of drugs or intoxicating substance, violation of the rule shall result in immediate termination of membership with no refunds.
                                </li>
                                <li>
                                    Management shall observe the attitude and behavior of the members towards staff and other members, any misconduct, ill mannered attitude or violating behavior shall result in termination of your membership without any refund.
                                </li>
                                <li>
                                    All members are advised to take care of their belongings/valuables; Management shall not be responsible / liable for any loss of article and valuables.
                                </li>
                                <li>
                                    Members and Guests shall not bring any food into the fitness club/Swimming Pool at any time; beverages consumed during workout must be in a container with a lid.
                                </li>
                                <li>
                                    No Pets are allowed in the premises of Megazone.
                                </li>
                                <li>
                                    Gym Facility is available for Gym members only, any guest, family member or mates shall not be allowed in the Gym area.
                                </li>
                                <li>
                                    Members are responsible for their own health and safety, and Megazone will not be responsible or liable to any compensation for any accident or injury.
                                </li>
                                <li>
                                    Any Person having any medical issue such as Epilepsy, Asthma, Heart issues or any other medical problem which may cause serious attack; are advised to consult with their own health Expert/Doctor before using the facility, Megazone shall not be responsible of any mishap or accident during or after the Workout / Swimming.
                                </li>
                                <li>
                                    Children under 6 years of age or under the height of 45” (45 Inches) are not allowed in the Swimming Pool.
                                </li>
                                <li>
                                    Children under 12 Years of age must be accompanied by a Parent or an adult guardian at the Swimming Pool
                                </li>
                                <li>
                                    Megazone will not refund any fee or portion in case a member wishes to terminate his/her membership or is unable to utilize facilities of the center any reason whatsoever.
                                </li>
                                <li>
                                    Management reserves the rights of Admission.
                                </li>
                                <li>
                                    Opening and Closing hours are established by the Management and can be revised without prior notice.
                                </li>
                                <li>
                                    Registration fee is valid for 3 months if any member does not renew his/her membership within three (3) months for any reason, he/she has to pay full registration fee to restore / Activate his/her membership afterwards with Regular fee/Charges (Non Discounted Charges) shall be applicable.
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>


    <div class="terms">
        <br>
        I have read acknowledged and agreed to all the terms and conditions; I certify the information provided by me is correct. I understand that non-compliance from my end may lead to termination of my membership without any obligation or liability towards
        <br><br>
    </div><!--close terms-->
        <!--<p class="centerpad">
            I have read acknowledged and agreed to all the terms and conditions; I certify the information provided by me is correct. I understand that non-compliance from my end may lead to termination of my membership without any obligation or liability towards.
        </p>-->

        <div  style="bottom: 10px !important; margin-top: 40px; !important" id="agreed_1">

                <div class=" b-d-left sign" > <span class="btn btn-block btn-success" id="agree">Agree</span></div>
                <a href="<?=Yii::$app->homeUrl?>" id="disagree"><div class="b-d-sign sign"> <button type="button" class="btn btn-block btn-danger">Disagree</button></div></a>

        </div>
        <div  style="bottom: 10px !important; margin-top: 40px; !important" id="agree_2">
            <form method="POST" action="<?=Yii::$app->homeUrl?>order/default/proceed-payment" enctype="multipart/form-data">
               <?php
                foreach($cart_items as $n=>$val){ ?>

                <input type="hidden" name="cart_items[<?=$n?>]" value="<?=$val?>">

               <?php } ?>


                <div class=" " > <button type="submit"  class="btn btn-block btn-success" >Confirm Order</button></div>

            </form>


        </div>



   </div><!--close centerpad-->
</div><!-- close container1-->

<!-- SVG Blur Filter -->
<!-- 'stdDeviation' is the blur amount applied -->
<svg id="svg-filter">
    <filter id="svg-blur">
        <feGaussianBlur in="SourceGraphic" stdDeviation="4"></feGaussianBlur>
    </filter>
</svg>

<!--<div style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif !important;color: BLACK !important; font-size: 96px !important; left: 25% !important; opacity: 0.21 !important; position: fixed !important; top: 45% !important; -ms-transform: rotate(-40deg) !important; -webkit-transform: rotate(-40deg) !important; transform: rotate(-40deg) !important;display: block !important;">Kistpay</div>-->


<script >


    $(document).ready(function () {

        $('#agree_2').hide();

    })

    $('#agree').click(function () {

        $('#agreed_1').hide();
        $('#agree_2').show();

    })


</script>