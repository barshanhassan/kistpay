<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Payments */

$id = $_REQUEST['id'];
$model =  \multebox\models\Payments::findOne($id);
$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>




    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <title>Termal Report</title>


        <style>
            .item-name{
                width:50px;
            }

            * { margin: 0; padding: 0; }
            body { font: 14px/1.4 Georgia, serif; }
            #wrap { width: 320px; margin: 0 auto; }
            .col-center{float: left; width: 100%; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt}
            .pad-left-right{padding-left: 0px; padding-right: 0px;}
            .text-center{ text-align:center;}
            .col-email{float: left; width: 100%; font-family:"Times New Roman", Georgia, Serif; font-size:9pt; margin-top: 10px;}
            .item-n{width: 100%; float: left; text-align: right}
            .item-num{width: 100%; float: left; border-bottom: 1px dashed #000;}
            .item-qty{width: 100%; float: left;}






            textarea { border: 0;
                font: 12px Georgia, Serif;
                overflow: hidden;
                resize: none;
                text-align: center; }
            table { border-collapse: collapse; }
            table td, table th { border: 1px solid #e3d9d9; padding: 5px; }

            .d-area{width: 100%;  padding:2px; float: left; margin-bottom: 5px; }
            .time{width: 48%; padding:5px 0; float:left;font-size: 9pt;  }
            .date{width: 48%;
                padding: 5px 0;
                float: left;
                text-align: right;
                border-left: 1px solid #cecece;
                font-size: 9pt;
            }
            .date_chng{
                font-family: sans-serif;
                font-size: 12px;
            }
            .fnt_weight{
                font-weight: 800;
            }

            #cus-in { margin-top: 6px; width: 100%; float: right; font:13px Helvetica, Sans-Serif; }
            #cus-in span {font: bold 15px Helvetica, Sans-Serif; }
            #cus-in  td { text-align: right; width: 65%; border: 1px solid #e6e1e1 }
            #cus-in  td.cus-in-head { text-align: left;}
            #cus-in  td textarea { width: 100%; height: 20px; text-align: right; }


            #address { width: 250px; height: 150px; float: left; }
            #customer { overflow: hidden; }



            #items { clear: both; width: 100%; text-align: center; margin: 10px 0 0 0; float: left; border: 1px solid black; font: bold 13px Helvetica, Sans-Serif; }
            #items th { background: #eee; }
            #items textarea { width: 80px; height: 26px; font: 12px Helvetica, Sans-Serif; }
            #items tr.item-row td { border: 0; vertical-align: top; border: 1px solid #f3f3f3; font: 13px Helvetica, Sans-Serif; }
            #items td.description { width: 40%; }
            #items td.item-name { width: 37%; }
            #items td.description textarea, #items td.item-name textarea { width: 100%; }
            #items td.total-line { border-right: 0; text-align: right; width: 65%; }
            #items td.total-value { border-left: 0; padding: 5px;text-align: right;}
            #items td.total-value textarea { height: 20px; background: none; }

            #items td.blank { border: 0; }

            #comment { text-align: center; margin: 20px 0 0 0;  float: left;width: 100%;}
            #comment h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
            #comment textarea { width: 100%; text-align: center;}

            .delete-wpr { position: relative; }
            .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }
            .breakpg{
                page-break-after  : always;
                page-break-before : always;
                page-break-inside : auto    ;
            }
            #page-wrap {
                width: 320px;
                float: left;
            }

            @media print{
                #wrap { width: 248px; margin: -30px auto; }
                #page-wrap {    width: 248px;    margin: 0 auto; float:left}
                .breadcrumb{display: none}
                p{none}
                .text-left{font-size: 12px; text-align: center; float: left}
                .wrap > .container { padding: 5px 0px 0px;}

            }

            div#page-wrapper {
                height: 1192px !important;
            }
            .ibox-content{
                height: 974px !important;
            }
        </style>

    </head>


    <body>
    <div class="payments-view">

        <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

        <p>
            <a class="btn btn-primary" onclick="window.print()" href="#">Print</a>
            <?php if((int)Yii::$app->user->can('payments/update')){ ?>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php } if((int)Yii::$app->user->can('payments/delete')){ ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            <?php } ?>
        </p>
        <div id="wrap">


            <?php $cus = \multebox\models\Customer::findOne($model->customer_id)->customer_name?>
            <div id="page-wrap">

                <!--time date Section-->
                <div class="d-area">
                    <div class="time">Invoice No: <?php echo $model->id; ?></div>
                    <div class="date">Date: <?php echo Date('d/m/Y',strtotime($model->payment_date));?></div>
                </div>
                <!--time date Section-->

                <table id="d-area" class="date_chng" style="width: 100%">
                    <tr>

                        <td>
                            <span class="fnt_weight">Customer No#</span> <br>
                            <?php echo str_pad($model->customer_id, 5, '0', STR_PAD_LEFT); ?>
                        </td>

                        <td class="cus-in-head">
                            <span class="fnt_weight">Name</span> <br>
                            <?php echo $cus; ?>
                        </td>
                    </tr>

                    <tr>
                        <td>

                                <span class="fnt_weight">CNIC</span> <br>
                                <?php echo $cus; ?>
                        </td>

                        <td class="cus-in-head">
                            <span class="fnt_weight">Contact No.</span> <br>
                            <?php echo $cus; ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="cus-in-head">
                            <span class="fnt_weight">Reg Date#</span> <br>
                            <?php echo date('d/m/Y',strtotime($model->payment_date)); ?>
                        </td>

                        <td>
                            <span class="fnt_weight">Payment Date#</span> <br>
                            <?php echo date('d/m/Y',strtotime($model->payment_date)); ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="cus-in-head">
                            <span class="fnt_weight"> Installment #</span> <br>
                            <?php
                            if($model->installment_no == 0){
                                echo 'Initial';
                            }
                            else
                                echo $model->installment_no;
                            ?>
                        </td>

                        <td>
                        <span class="fnt_weight">User</span><br>
                        <?php echo \Yii::$app->user->identity->username; ?>
                    </td>
                    </tr>

                    
                </table>

            <?php if(!empty($_REQUEST['dup'])){ ?>
                <div class="watermark" style="position: relative: z-index:-1;   color: #000000!important;line-height: 14px;font-size: 26px !important; margin: 0 auto; opacity: 0.5 !important; position: absolute !important; -ms-transform: rotate(-66deg) !important; -webkit-transform: rotate(-66deg) !important; transform: rotate(-66deg) !important;">Duplicate Bill</div>
            <?php } ?>
            <!--time date Section-->
            <div class="d-area">
                <div style="font-size: 12px;" class="text-center"><b>Customer's Copy</b></div>
            </div>
            <!--time date Section-->
            <table id="items">

                <tr>
                    <th>Items </th>
                    <th class="item-name">Charges</th>
                    <th class="item-name">Discount</th>
                    <th class="item-name">Total</th>

                </tr>


                <tr class="item-row" >
                    <td class="item-name" >Installment fee</td>
                    <td class="age"><?php echo $model->installment_amonut;?></td>
                    <td class="item-name">0</td>
                    <td class="age"><?php echo $model->installment_amonut?></td>
                </tr>


                <tr>

                    <td colspan="3" class="total-line">Grand Total</td>
                    <td class="total-value"><div id="subtotal"><?php echo $model->grand_total?></div></td>
                </tr>
                <?php ?>
                <tr>
                    <?php if($model->total_paid!='0'){?>
                    <td colspan="3" class="total-line">Paid Amount</td>
                    <td class="total-value"><div id="subtotal"><?php echo $model->total_paid?></div></td>
                </tr>

            <?php }?>
            </table>

            <!--time date Section-->
            <div class="d-area">
                <div style="font-size: 8px;" class="text-center">The slip has been generated by <b><?php echo $model->created_by; ?></b></div>
            </div>
            <!--time date Section-->
        </div>
    </div>



    <div class="breakpg"></div>



    <div id="wrap">

        <!--<div class="col-center  pad-left-right text-center">
            <img src="<?/*= Yii::$app->request->baseUrl.'/images/mega-zone-logo.jpg' */?>"  width="210">


        </div>
-->
        <?php if(!empty($_REQUEST['dup'])){ ?>
            <div class="watermark" style=" float: left; position: relative: z-index:-1;   color: #000000!important;line-height: 14px;font-size: 26px !important; margin:50% auto; opacity: 0.5 !important; position: absolute !important; -ms-transform: rotate(-66deg) !important; -webkit-transform: rotate(-66deg) !important; transform: rotate(-66deg) !important;">Duplicate Bill</div>
        <?php } ?>

        <div id="page-wrap">


            <!--time date Section-->
            <div class="d-area">
                <div class="time">Invoice No: <?php echo $model->id; ?></div>
                <div class="date">Date: <?php  echo Date('d/m/Y',strtotime($model->payment_date));?></div>
            </div>
            <!--time date Section-->
            <div style="clear: both;"></div>
            <table id="d-area" class="date_chng" style="width: 100%">
                <tr>

                    <td>
                        <span class="fnt_weight">Reg No#</span> <br>
                        <?php echo str_pad($model->customer_id, 5, '0', STR_PAD_LEFT); ?>
                    </td>

                    <td class="cus-in-head">
                        <span class="fnt_weight">Name</span> <br>
                        <?php echo $cus; ?>
                    </td>
                </tr>

                <tr>

                    <td>
                        <span class="fnt_weight">CNIC</span> <br>
                        <?php echo $cus; ?>
                    </td>

                    <td class="cus-in-head">
                        <span class="fnt_weight">Contact No.</span> <br>
                        <?php echo $cus; ?>
                    </td>
                </tr>

                <tr>
                    <td class="cus-in-head">
                        <span class="fnt_weight">Reg Date#</span> <br>
                        <?php echo date('d/m/Y',strtotime($model->payment_date)); ?>
                    </td>

                    <td>
                        <span class="fnt_weight">Expiry Date#</span> <br>
                        <?php echo date('d/m/Y',strtotime($model->due_date)); ?>
                    </td>
                </tr>

               <tr>
                        <td class="cus-in-head">
                            <span class="fnt_weight">Reg Date#</span> <br>
                            <?php echo date('d/m/Y',strtotime($model->payment_date)); ?>
                        </td>

                        <td>
                            <span class="fnt_weight">Payment Date#</span> <br>
                            <?php echo date('d/m/Y',strtotime($model->payment_date)); ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="cus-in-head">
                            <span class="fnt_weight"> Installment #</span> <br>
                            <?php
                            if($model->installment_no == 0){
                                echo 'Initial';
                            }
                            else
                                echo $model->installment_no;
                            ?>
                        </td>

                        <td>
                        <span class="fnt_weight">User</span><br>
                        <?php  echo \Yii::$app->user->identity->username; ?>
                    </td>
                    </tr>

            </table>


            <!--time date Section-->
            <div class="d-area">
                <div style="font-size: 12px;" class="text-center"><b>Office's Copy</b></div>
            </div>
            <!--time date Section-->
            <table id="items">

                <tr>
                    <th>Items </th>
                    <th class="item-name">Charges</th>
                    <th class="item-name">Discount</th>
                    <th class="item-name">Total</th>

                </tr>


                <tr class="item-row" >
                    <td class="item-name" >Installment Amount</td>
                    <td class="age"><?php echo $model->paid_by;?></td>
                    <td class="item-name">0</td>
                    <td class="age"><?php echo $model->installment_amonut?></td>
                </tr>

                <tr>

                    <td colspan="3" class="total-line">Grand Total</td>
                    <td class="total-value"><div id="subtotal"><?php echo $model->total_paid?></div></td>
                </tr>
                <?php ?>
                <tr>
                    <?php if($model->total_paid!='0'){?>

                    <td colspan="3" class="total-line">Paid Amount</td>
                    <td class="total-value"><div id="subtotal"><?php echo $model->total_paid?></div></td>
                </tr>
            
            <?php }?>
            </table>

            <!--time date Section-->
            <div class="d-area">
                <div style="font-size: 8px;" class="text-center">The slip has been generated by <b><?php echo $model->created_by; ?></b></div>
            </div>
            <!--time date Section-->

            <div style="clear:both"></div>
        </div>
    </div>
    </div>
    </body>


</html>





