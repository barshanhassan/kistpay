<?php
/**
 * Created by PhpStorm.
 * User: MCE_Barshan
 * Date: 4/10/2019
 * Time: 2:05 PM
 */
?>

<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">


    <?php   $this->render('_search', ['model' => $searchModel]); ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'header' =>'Customer',
                'format' => 'raw',
                'value'=>function($model) {
                    $c_id = \multebox\models\Order::findOne(['=','order_id',$model->order_id])->customer_id;
                    $c_name = \multebox\models\Customer::findOne($c_id);
                    $cus = $c_name->customer_name;
                    return  $cus;
                },
            ],



            //'order_no',
            ['attribute'=>'plan_id',
                'value'=> 'plan.plan_name'
            ],
            ['attribute'=>'bank_id',
                'value'=>'bank.name'],
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            [
                'attribute'=>'status',
                'format'=>'raw',
                'value'=>function($model, $key, $index){
                    if($model->status == '0')
                        return '<span class="label label-warning">Pending</span>';
                    else if($model->status == 1)
                        return '<span class="label label-primary">Accepted</span>';
                    else if($model->status == 2)
                        return '<span class="label label-danger">Rejected</span>'; 
                    else 
                        return '<span class="label label-info">Under Processing</span>';
                }
            ],

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{view} ',

                'buttons' => [
                    /*'delete' => function ($url, $model, $key) {

                        return Html::a('<span class="btn btn-white btn btn-xs">Delete</span>', $url,
                            [
                                'title' => Yii::t('app', 'Delete'),
                                'data-pjax' => '1',
                                'data' => [
                                    'method' => 'post',
                                    'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                    'pjax' => 1,],
                            ]
                        );

                    },
                    'update' => function ($url, $model)
                    {
                        return '<a href="'.Yii::$app->homeUrl.'orders/update?id='.$model->id.'"><span class="btn-white btn btn-xs" >Update</span></a>';
                    } ,*/
                    'view'=>function($url,$model)
                    {
                        return '<a href="'.Yii::$app->homeUrl.'payments//payments/view-payments?s_id='.$model->id.'&o_id='.$model->order_id.'"><span class="btn-white btn btn-xs">Take Payment</span></a>';
                    },

                ],
                /*'contentOptions' => function($model)
                {
                    return ['style' => 'max-width:100px !important;'];
                }*/
            ],
        ],
    ]); ?>
</div>

