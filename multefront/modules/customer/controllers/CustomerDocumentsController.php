<?php

namespace multefront\modules\customer\controllers;

use Yii;
use multebox\models\CustomerDocuments;
use multebox\models\search\CustomerDocumentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use app\helpers\Helper;


/**
 * CustomerDocumentsController implements the CRUD actions for CustomerDocuments model.
 */
class CustomerDocumentsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerDocuments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerDocumentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerDocuments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        return $this->render('view');
    }

    /**
     * Creates a new CustomerDocuments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $count =0;
        $model = new CustomerDocuments();

        if ($_REQUEST['customer_id']){
            foreach($_FILES['docs']['tmp_name'] as $key => $tmp_name ){

                /*echo $file_name = $key.$_FILES['docs']['name'][$key].'<br>';
                $file_size =$_FILES['docs']['size'][$key];
                $file_tmp =$_FILES['docs']['tmp_name'][$key];
                $file_type=$_FILES['docs']['type'][$key];*/


               $model = new CustomerDocuments();
               $model->document_type = $_REQUEST['document_type'][$count];
               $model->customer_id =$_REQUEST['customer_id'];
               $model->status = 1;
               $model->created_by = Yii::$app->user->identity->getId();
               $model->created_at = date("Y-m-d H:i:s");
               $directory = Yii::getAlias('@app/drive/customerDocuments') . DIRECTORY_SEPARATOR;
               if (!is_dir($directory)) {
                   FileHelper::createDirectory($directory);
               } else {

                   // foreach ($model->file_name as $model->file_name) {

                   $timestamp1 = date('YmdHis');
                   $ext = pathinfo($key.$_FILES['docs']['name'][$key], PATHINFO_EXTENSION);
                   $filename = $model->document_type . '-' . $timestamp1 . '.' . $ext;
                   $file_name1 = str_replace(array(" ", "&"), array("_", "-"), $filename);
                   $filePath = $directory. $file_name1;
                   $model->file_type = $_FILES['docs']['type'][$key];
                   $model->file_size = $_FILES['docs']['size'][$key];

                   if (move_uploaded_file($_FILES['docs']['tmp_name'][$key], $filePath)) {
                       $model->file_name = $file_name1;
                       if ($model->save()) {


                       } else {
                           echo '<pre>';
                           echo print_r($model);
                           echo '</pre>';
                       }
                   } else {
                       echo 'error';
                   }
                   //}
               }
               $count++;

            }
            return $this->redirect(array('view'));
           }
               return $this->render('create', [
                   'model' => $model,
               ]);
       }

       /**
        * Updates an existing CustomerDocuments model.
        * If update is successful, the browser will be redirected to the 'view' page.
        * @param integer $id
        * @return mixed
        * @throws NotFoundHttpException if the model cannot be found
        */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CustomerDocuments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerDocuments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerDocuments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = CustomerDocuments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
