<?php
use multebox\models\ProductBrand;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\Inventory;
use multebox\models\search\MulteModel;
use multebox\models\Product;
use multebox\models\File;
use yii\helpers\Url;

include_once('../web/cart_script.php');
$lang = $_REQUEST['lang'];
?>

<header class="bg-img">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

        <div class="carousel-inner" role="listbox">
            <!-- Slide One - Set the background image for this slide in the line below -->
            <div class="carousel-item active for-bg-img">
                <img src="<?= Yii::$app->homeUrl?>img/homepage-bitmap.png" class="img-slider">
                <div class="carousel-caption d-md-block">
                    <div class="smartloansforsmar">
                        <span class="span1"><?= Yii::t('app', 'Smart Loan') ?></span>
                        <span class="span3"><?= Yii::t('app', 'for') ?><br><?= Yii::t('app', 'Smart Phones') ?></span>
                        <p class="lead"><?= Yii::t('app', 'One of a kind platform enabling customers to purchase smartphones on 0% Markup, easy installment plans with Smartphone Insurance & theft protection') ?>
                        </p>
                    </div>
                    <div class="img-div"></div>
                </div>
            </div>
        </div>

    </div>
</header>

    <!-- Page Content -->
    <section class="py-5 text-center">
        <div class="container">
            <!--<h1 class="font-weight-500 text-center"><?= Yii::t('app','Our Lending Partners')?></h1>
            <div class="row">
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?= Yii::$app->homeUrl?>img/homepage-bitmap-1@2x.png"></div>
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?/*= Yii::$app->homeUrl*/?>img/homepage-bitmap-1@2x.png"></div>
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?/*= Yii::$app->homeUrl*/?>img/homepage-bitmap-1@2x.png"></div>
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?/*= Yii::$app->homeUrl*/?>img/homepage-bitmap-1@2x.png"></div>
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?/*= Yii::$app->homeUrl*/?>img/homepage-bitmap-1@2x.png"></div>
                <div class="col-2 col-2-custom"><img class="bitmap1" src="<?/*= Yii::$app->homeUrl*/?>img/homepage-bitmap-1@2x.png"></div>
            </div>
        </div>-->

        <div class="container mt-5">
            <div class="row">
                <?php
                $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->limit(4)->all();
                foreach($inventoryItemsList as $inventoryItem)
                {
                    $inventoryPrice = floatval($inventoryItem->price);
                    if($inventoryItem->price_type == 'B')
                    {
                        foreach(json_decode($inventoryItem->attribute_price) as $row)
                        {
                            $inventoryPrice += floatval($row);
                        }
                    }

                    if($inventoryItem->discount_type == 'P')
                        $inventoryDiscount = $inventoryItem->discount;
                    else
                    {
                        if($inventoryPrice > 0)
                            $inventoryDiscount = round((floatval($inventoryItem->discount)/$inventoryPrice)*100,2);
                        else
                            $inventoryDiscount = 0;
                    }

                    $inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

                    $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
                    if($lang=='ur-UR'){
                        $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id,'lang'=>'ur-UR']);
                    }else{
                        $url=Url::to(['/product/default/prodetail', 'inventory_id' => $inventoryItem->id]);
                    }

                    ?>
                <div class="col-3">
                    <div class="col-12 col-3-custom"><a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="bitmap3" /></a></div>
                    <a class="card-block mt-4 prod-text">
                        <h4 class="card-title"><a href="<?=$url?>"><?=$inventoryItem->product_name?></a></h4>
                        <p class="card-text m-0 font"><?=Yii::t('app','Full Price')?>: Rs <?=MulteModel::formatAmount($inventoryDiscountedPrice)?></p>
                        <?php if($inventoryDiscountedPrice > 5000){   ?>
                            <p class="card-text m-0 font"><?=Yii::t('app','EMI as low as')?>: Rs <?=MulteModel::formatAmount(MulteModel::getEmi($inventoryDiscountedPrice))?></p>

                        <?php } ?>
                        <?php if($inventoryItem->stock > 0)
                        {
                        ?>
                            <a href="<?=$url?>"><p class="card-text font"><b><?= Yii::t('app','Buy Now!')?></b></p></a>
                        <?php }
                        else{?>
                        <button class="card-text" type="button" disabled><p><?=Yii::t('app', 'Out of Stock')?></p></button>
                        <?php }
                        ?>
                </div>

                <?php
                }
                ?>
                <!--<div class="col-3">
                    <div class="col-12 col-3-custom"><img class="bitmap3" src="<?/*= Yii::$app->homeUrl*/?>/img/homepage-bitmap-2@2x.png"></div>
                    <div class="card-block mt-4 prod-text">
                        <h4 class="card-title">Huawei Honor 10</h4>
                        <p class="card-text m-0">7.5 Inch Display</p>
                        <p class="card-text">Buy Now!</p>
                    </div>
                </div>-->
            </div>
            <div class="row">
                <div class="col-5 col-5-custom">
                    <div class="col-12">
                        <a href="<?= Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'product/default/index?lang=ur-UR':'product/default/index')?>"><button type="button" class="btn btn-circle-cus btn-lg btn-primary font"><?=Yii::t('app', 'VIEW MORE MOBILES')?></button></a>
                    </div>
                </div>
            </div>

        </div>
    </section>

