<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="loan-form signup-form">
    <section class="py-5 sec-loanform ">
        <div class="container">



            <div class="box-shad-light card card-sign card-signup">

                <div class="card-body">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>


                    <div class="col-12">
                        <?= $form->field($model, 'password', [
                            'template' => '{input}{beginLabel}{labelTitle}{endLabel}<i class="bar"></i>{error}{hint}',
                            'labelOptions' => ['class' => 'control-label font']
                        ])->textInput(['class' => '']); ?>
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', '<span>Reset Password</span>'), ['class' => 'button m-0']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $("input").prop('required',true);
</script>