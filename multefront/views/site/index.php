<?php
use multebox\models\ProductBrand;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\Inventory;
use multebox\models\search\MulteModel;
use multebox\models\Product;
use multebox\models\File;
use yii\helpers\Url;

include_once('../web/cart_script.php');
?>
<head>

    <style>
        a:hover {
            color: #fed700}

        .newsletter-popup .close {
            background: #fed700 none repeat scroll 0 0;
        }

        #newsletter-form .actions .button-subscribe {
            background-color: #fed700;
        }

        .welcome-info .page-header p em {
            border-bottom: 2px #fed700 solid;
        }

        .welcome-info .page-header .text-main {
            color: #fed700}

        .header-top a:hover {
            color: #fed700;
        }

        .mini-cart .actions .btn-checkout {
            background: #fed700;
        }

        .mini-cart .actions .view-cart:hover {
            background: #fed700;
        }

        .page-header .text-main {
            color: #fed700}

        .hot-deal .title-text {
            background-color: #fed700;
        }

        .jtv-banner-box .button {
            background-color: #fed700;
        }

        .icon-new-label {
            background: #fed700;
        }

        .add-to-cart-mt {
            background: #fed700;
        }

        @media (max-width:1024px){
            .add-to-cart-mt {
                background: #fed700;
            }
        }

        .pr-button .mt-button a:hover {
            background: #fed700;
        }

        .product-item .item-inner .item-info .item-title a:hover {
            color: #fed700;
        }

        .slider-items-products .owl-buttons a:hover {
            background: #fed700;
            border: 1px #fed700 solid
        }

        .home-testimonials strong.name {
            color: #fed700;
        }

        .our-clients {
            background-color: #fed700;
        }

        .blog-content-jtv a:hover {
            color: #fed700;
        }

        @media (max-width: 1024px) {
            .bottom-banner-img .shop-now-btn {
                background-color: #fed700;
                border-color: #fed700;
            }
        }

        .bottom-banner-img:hover .shop-now-btn {
            background-color: #fed700;
            border-color: #fed700;
        }

        .jtv-category-area .button-cart button {
            border: 1px solid #fed700;
            background-color: #fed700;
        }

        .jtv-category-area .jtv-extra-link a:hover, .jtv-category-area .button-cart button:hover {
            background: #fed700 none repeat scroll 0 0;
            border-color: #fed700;
        }

        .cat-title::before {
            background: #fed700;
        }

        .totop {
            background: none repeat scroll 0 0 #fed700;
        }

        .footer-newsletter {
            background: #000000;
        }

        nav {
            background: #000000}

        .mtmegamenu>ul>li.active, .menu-bottom .menu-bottom-dec a {
            background-color: #fed700;
        }

        .mtmegamenu>ul>li.active:hover, .menu-bottom .menu-bottom-dec a:hover {
            border-color: #fed700}

        .menu > li > a:hover, .menu > li > a:focus, .menu > li.active > a {
            color: #fed700;
        }

        .mega-menu-category > .nav > li > a:hover, .mega-menu-category > .nav > li > a:focus, .mega-menu-category > .nav > li.active > a {
            background-color: #fed700;
        }

        .box-banner .price-sale {
            color: #fed700;
        }

        .box-banner a:hover {
            color: #fed700;
        }

        .navbar-primary {
            background-color: #fed700;
        }

        .view-mode li.active a {
            color: #fed700}

        .pagination-area ul li a.active {
            background: #fed700;
            border: 1px solid #fed700;
        }

        .filter-price .ui-slider .ui-slider-handle {
            border: 2px solid #fed700;
        }

        button.button {
            background: #fed700;
            border: 2px solid #fed700;
        }

        a.my-button {
            background: #fed700;
            border: 2px solid #fed700;
        }

        .category-sidebar .sidebar-title {
            background-color: #fed700;
        }

        .sidebar-bar-title h3 {
            border-bottom: 2px #fed700 solid;
        }

        .product-price-range .slider-range-price {
            background: #fed700;
        }

        .product-price-range .slider-range-price .ui-slider-handle {
            background: #fed700;
        }

        .check-box-list label:hover {
            color: #fed700}

        .check-box-list input[type="checkbox"]:checked+label span.button {
            background: #fed700 url("../images/checked.png") no-repeat center center
        }
        .check-box-list input[type="checkbox"]:checked+label {
            color: #fed700}

        .size li a:hover {
            border-color: #fed700}

        .popular-tags-area .tag li a:hover {
            background: #fed700;
        }

        .special-product a.link-all {
            background: #fed700;
            border: 2px solid #fed700;
        }

        .category-description a.info:hover {
            background: #fed700;
        }

        .products-list .desc a.link-learn {
            color: #fed700}

        .product-view-area .flexslider-thumb .flex-direction-nav a:hover {
            background-color: #fed700;
        }

        .dec.qtybutton:hover, .inc.qtybutton:hover {
            background-color: #fed700;
        }

        button.button.pro-add-to-cart {
            background: #fed700;
            border: 2px #fed700 solid;
        }

        .product-cart-option ul li a:hover, .product-cart-option ul li a:hover i {
            color: #fed700}

        .product-tabs li.active a {
            border: 2px solid #fed700;
            background: #fed700;
        }

        .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
            background: #fed700;
            border: 2px solid #fed700;
        }

        button.button.add-tags {
            background: #fed700;
            border: 2px solid #fed700;
        }

        .review-ratting p a {
            color: #fed700;
        }

        .reviews-content-right h3 span {
            color: #fed700}

        .page-order .cart_navigation a.checkout-btn:hover {
            border: 2px solid #fed700;
            background: #fed700}

        .cart_summary .qty a:hover {
            background: #fed700;
        }

        .wishlist-item table .td-add-to-cart > a {
            background: #fed700;
        }

        .wishlist-item table .td-add-to-cart > a:hover, .wishlist-item .all-cart:hover {
            background: #fed700;
        }

        .error_pagenotfound em {
            color: #fed700;
        }

        a.button-back {
            background: #fed700;
        }

        .about-page ul li a:hover {
            color: #fed700;
        }

        .about-page .text_color {
            color: #fed700;
        }

        .align-center-btn a.button {
            border: 2px #fed700 solid;
            background: #fed700;
        }

        .align-center-btn a.button.buy-temp {
            background: #fed700;
            border: 2px #fed700 solid;
        }

        .panel-info>.panel-heading {
            color: #fed700;
            background-color: #fed700;
            border-color: #fed700;
        }

        .panel-info>.panel-heading h3{
            color: #fed700;
        }

        .panel-info>.panel-heading .summary{
            color: #fed700;
        }

        .panel-info {
            border-color: #fed700;
        }

        .btn-primary {
            background-color: #00000;
        }

        .label-primary {
            background-color: #fed700;
        }

        #mobile-menu {
            background: #fed700;
        }

        .mobile-menu li li {
            background: #fed700;
        }

        .mobile-menu li {
            display: block;
            border-top: 1px solid #D3B300;
        }

        /*Loader Css*/
        .lds-hourglass {
            display: inline-block;
            position: relative;
            width: 64px;
            height: 64px;
        }
        .lds-hourglass:after {
            content: " ";
            display: block;
            border-radius: 50%;
            width: 0;
            height: 0;
            margin: 6px;
            box-sizing: border-box;
            border: 26px solid #fff;
            border-color: #fff transparent #fff transparent;
            animation: lds-hourglass 1.2s infinite;
        }
        @keyframes lds-hourglass {
            0% {
                transform: rotate(0);
                animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
            }
            50% {
                transform: rotate(900deg);
                animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
            }
            100% {
                transform: rotate(1800deg);
            }
        }





    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.css">

    <!--<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
    <!------ Include the above in your HEAD tag ---------->

   <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">-->
    <!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->

</head>
<body>
<!--<div class="lds-hourglass"></div>-->


<!-- Slideshow Start-->
<div class="slideshow single-slider owl-carousel">
    <div class="item"> <a href="#"><img class="img-responsive" src="<?=Url::base()?>/image/slider/banner-1.jpg" alt="banner 1" /></a> </div>
    <div class="item"> <a href="#"><img class="img-responsive" src="<?=Url::base()?>/image/slider/banner-2.jpg" alt="banner 2" /></a> </div>
    <div class="item"> <a href="#"><img class="img-responsive" src="<?=Url::base()?>/image/slider/banner-3.jpg" alt="banner 3" /></a> </div>
</div>
<!-- Slideshow End-->

<div class="wrapper-wide">
</div>

  


<!-- Latest Product Start-->
<div class="container">
<h3 class="subtitle"><?=Yii::t('app', 'Latest Products')?></h3>
<div class="owl-carousel product_carousel">
    <?php
    $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['id'=>SORT_DESC])->limit(15)->all();
    foreach($inventoryItemsList as $inventoryItem)
    {
        $inventoryPrice = floatval($inventoryItem->price);
        if($inventoryItem->price_type == 'B')
        {
            foreach(json_decode($inventoryItem->attribute_price) as $row)
            {
                $inventoryPrice += floatval($row);
            }
        }

        if($inventoryItem->discount_type == 'P')
            $inventoryDiscount = $inventoryItem->discount;
        else
        {
            if($inventoryPrice > 0)
                $inventoryDiscount = round((floatval($inventoryItem->discount)/$inventoryPrice)*100,2);
            else
                $inventoryDiscount = 0;
        }

        $inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

        $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
        ?>
        <div class="product-thumb clearfix">
            <?php
            $url=Url::to(['/product/default/detail', 'inventory_id' => $inventoryItem->id]);
            ?>
            <div class="image"><a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="img-responsive img-listing" /></a></div>
            <div class="caption">
                <h4><a href="<?=$url?>"><?=$inventoryItem->product_name?></a></h4>
                <p class="price">
                    <span class="price-new"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span>
                    <?php
                    if($inventoryDiscountedPrice != $inventoryPrice)
                    {
                        ?>
                        <span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span>
                        <?php
                    }
                    ?>
                <p> <input type="text" class="multe-rating-nocap-sm" value="<?=$inventoryItem->product_rating?>" readonly> </p>
                <?php
                if($inventoryDiscount > 0)
                {
                    ?>
                    <span class="saving">-<?=$inventoryDiscount?>%</span>
                    </p>
                    <?php
                }
                else
                {
                    ?>
                    </p>
                    <?php
                }
                ?>
            </div>
            <div class="button-group">
                <?php
                if($inventoryItem->stock > 0)
                {
                    ?>
                    <!--<button class="btn-primary addtocart" type="button" value="<?/*=$inventoryItem->id*/?>"><span><?/*=Yii::t('app', 'Buy Now')*/?></span></button>-->
                    <a href="<?=$url?>"><button class="btn-primary addtocart2" type="button" value="<?=$inventoryItem->id?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>
                    <?php
                }
                else
                {
                    ?>
                    <button class="btn-primary" type="button" disabled><span><?=Yii::t('app', 'Out of Stock')?></span></button>
                    <?php
                }
                ?>

            </div>
        </div>
        <?php
    }
    ?>
</div>
</div>
<!-- Latest Product End-->

<!-- Top Rated Product Slider Start -->
<div class="container">
<h3 class="subtitle"><?=Yii::t('app', 'Top Rated Products')?></h3>
<div class="owl-carousel product_carousel">
    <?php
    $inventoryItemsList = Inventory::find()->where('stock > 0')->orderBy(['product_rating'=>SORT_DESC])->limit(15)->all();
    foreach($inventoryItemsList as $inventoryItem)
    {
        $inventoryPrice = floatval($inventoryItem->price);
        if($inventoryItem->price_type == 'B')
        {
            foreach(json_decode($inventoryItem->attribute_price) as $row)
            {
                $inventoryPrice += floatval($row);
            }
        }

        if($inventoryItem->discount_type == 'P')
            $inventoryDiscount = $inventoryItem->discount;
        else
        {
            if($inventoryPrice > 0)
                $inventoryDiscount = round((floatval($inventoryItem->discount)/$inventoryPrice)*100,2);
            else
                $inventoryDiscount = 0;
        }

        $inventoryDiscountedPrice = round($inventoryPrice - $inventoryPrice*$inventoryDiscount/100, 2);

        $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
        ?>
        <div class="product-thumb clearfix">
            <?php
            $url=Url::to(['/product/default/detail', 'inventory_id' => $inventoryItem->id]);
            ?>
            <div class="image"><a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="img-responsive img-listing" /></a></div>
            <div class="caption">
                <h4><a href="<?=$url?>"><?=$inventoryItem->product_name?></a></h4>
                <p class="price">
                    <span class="price-new"><?=MulteModel::formatAmount($inventoryDiscountedPrice)?></span>
                    <?php
                    if($inventoryDiscountedPrice != $inventoryPrice)
                    {
                        ?>
                        <span class="price-old"><?=MulteModel::formatAmount($inventoryPrice)?></span>
                        <?php
                    }
                    ?>
                <p> <input type="text" class="multe-rating-nocap-sm" value="<?=$inventoryItem->product_rating?>" readonly> </p>
                <?php
                if($inventoryDiscount > 0)
                {
                    ?>
                    <span class="saving">-<?=$inventoryDiscount?>%</span>
                    </p>
                    <?php
                }
                else
                {
                    ?>
                    </p>
                    <?php
                }
                ?>
            </div>
            <div class="button-group">
                <?php
                if($inventoryItem->stock > 0)
                {
                    ?>
                    <!--<button class="btn-primary addtocart" type="button" value="<?/*=$inventoryItem->id*/?>"><span><?/*=Yii::t('app', 'Buy Now')*/?></span></button>-->
                    <a href="<?=$url?>"><button class="btn-primary addtocart2" type="button" value="<?=$inventoryItem->id?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>

                    <?php
                }
                else
                {
                    ?>
                    <button class="btn-primary" type="button" disabled><span><?=Yii::t('app', 'Out of Stock')?></span></button>
                    <?php
                }
                ?>

            </div>
        </div>
        <?php
    }
    ?>
</div>
</div>

<!-- Categories Product Slider End -->

<div class="container">
    <div class="row">
        <div class="col-sm-4 col-xs-12">
            <div class="jtv-banner-box banner-inner">
                <div class="image"> <a class="jtv-banner-opacity" href="/product/listing/1/1"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/45b979e2fc27ed.jpg" alt="45b979e2fc27ed.jpg"></a> </div>
                <div class="jtv-content-text">
                    <h3 class="title">Save Upto 25%</h3>
                    <span class="sub-title">Nice & Cleans</span> </div>
            </div>
        </div>


        <div class="col-sm-5 col-xs-12">
            <div class="jtv-banner-box">
                <div class="image"> <a class="jtv-banner-opacity" href="javascript:void(0)"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/5b83d29356c27.jpg" alt="5b83d29356c27.jpg"></a> </div>
                <div class="jtv-content-text">
                    <h3 class="title">Powerful Stereo Sound</h3>
                    <span class="sub-title">You're future in the sound</span> <a href="/product/listing/1/2/8" class="button">BUY NOW</a> </div>
            </div>
        </div>

        <div class="col-sm-3 col-xs-12">
            <div class="jtv-banner-box banner-inner">
                <div class="image"> <a class="jtv-banner-opacity" href="/product/listing/1/3"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/5b83d3272f6fa.jpg" alt="5b83d3272f6fa.jpg"></a> </div>
                <div class="jtv-content-text">
                    <h3 class="title">NEW ARRIVAL</h3>
                </div>
            </div>

            <div class="jtv-banner-box banner-inner">
                <div class="image "> <a class="jtv-banner-opacity" href="/product/listing/1"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/5b83d39444a66.jpg" alt="5b83d39444a66.jpg"></a> </div>
                <div class="jtv-content-text">
                    <h3 class="title">ACCESSORIES</h3>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="bottom-banner-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4"> <a href="/product/listing/1/4" class="bottom-banner-img"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/5b83d54a70bdf.jpg" alt="5b83d54a70bdf.jpg"> <span class="banner-overly"></span>
                    <div class="bottom-img-info">
                        <h3>Electronic</h3>
                        <h6>This is excellent</h6>
                        <span class="shop-now-btn">Shop Now!</span> </div>
                </a> </div>
            <div class="col-md-8 col-sm-8"> <a href="/product/listing/1" class="bottom-banner-img last"><img class="mylazy"  data-src="https://system.multecart.com/attachments/banner/5b83d56f7f9f8.jpg" alt="5b83d56f7f9f8.jpg"> <span class="banner-overly last"></span>
                    <div class="bottom-img-info last">
                        <h3>New collection</h3>
                        <h6>Excellent for every one</h6>
                        <span class="shop-now-btn">Show Now!</span> </div>
                </a> </div>
        </div>
    </div>
</div>



        <div class="jtv-category-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="jtv-single-cat">
                            <h2 class="cat-title">Top Rated</h2>
                            <!-- Start -->
                            <?php $cnt = 0;
                            foreach(array_reverse($inventoryItemsList) as $inventoryItem)
                            {   $cnt++;
                                $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
                                $price = Inventory::find()->where(['=','product_id',$inventoryItem->product_id])->one()->price;
                                $url=Url::to(['/product/default/detail', 'inventory_id' => $inventoryItem->id]);
                                ?>
                                <div class="jtv-product jtv-cat-margin">
                                    <div class="product-img"> <a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="img-responsive img-listing" /></a></div>
                                    <div class="jtv-product-content">
                                        <h3><a href="<?=$url?>"><?=$inventoryItem->product_name?></a></h3>
                                        <div class="price-box">
                                            <p class="regular-price"> <span class="price"><?=MulteModel::formatAmount($price)?></span> </p>
                                        </div>



                                    </div><div class="button-group">
                                        <!--<div class="jtv-extra-link">
                                            <div class="button-cart">
                                                <button class="addtocartmini" value="24" title="Buy Now" data-placement="top" data-container="body" data-toggle="tooltip"><i class="fa fa-shopping-cart"></i></button>
                                            </div>
                                            <a class="add_to_compare" href="javascript:void(0)" title="Add to Comparelist" data-placement="top" data-container="body" data-toggle="tooltip">
                                                <i class="fa fa-signal"></i>
                                                <input type="hidden" value="24">
                                            </a>

                                            <a class="add_to_wishlist" href="javascript:void(0)" title="Add to Wishlist" data-placement="top" data-container="body" data-toggle="tooltip">
                                                <i class="fa fa-heart"></i>
                                                <input type="hidden" value="24">
                                            </a>
                                        </div>-->
                                        <a href="<?=$url?>"><button class="btn-primary addtocart2" type="button" value="<?=$inventoryItem->id?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>
                                    </div>
                                </div>
                                <?php if($cnt == 3){
                                break;
                            }
                            } ?>
                            <!-- End -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="jtv-single-cat">
                            <h2 class="cat-title">Latest</h2>
                            <!-- Start -->
                            <?php $cnt = 0;
                            foreach($inventoryItemsList as $inventoryItem)
                            {   $cnt++;
                                $fileDetails = File::find()->where("entity_type='product' and entity_id=$inventoryItem->product_id")->one();
                                $price = Inventory::find()->where(['=','product_id',$inventoryItem->product_id])->one()->price;
                                $url=Url::to(['/product/default/detail', 'inventory_id' => $inventoryItem->id]);
                                ?>
                                <div class="jtv-product jtv-cat-margin">
                                    <div class="product-img"> <a href="<?=$url?>"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" alt="<?=$inventoryItem->product_name?>" title="<?=$inventoryItem->product_name?>" class="img-responsive img-listing" /></a></div>
                                    <div class="jtv-product-content">
                                        <h3><a href="<?=$url?>"><?=$inventoryItem->product_name?></a></h3>
                                        <div class="price-box">
                                            <p class="regular-price"> <span class="price"><?=MulteModel::formatAmount($price)?></span> </p>
                                        </div>
                                        <div class="button-group">
                                            <!--<div class="jtv-extra-link">
                                                <div class="button-cart">
                                                    <button class="addtocartmini" value="24" title="Buy Now" data-placement="top" data-container="body" data-toggle="tooltip"><i class="fa fa-shopping-cart"></i></button>
                                                </div>
                                                <a class="add_to_compare" href="javascript:void(0)" title="Add to Comparelist" data-placement="top" data-container="body" data-toggle="tooltip">
                                                    <i class="fa fa-signal"></i>
                                                    <input type="hidden" value="24">
                                                </a>

                                                <a class="add_to_wishlist" href="javascript:void(0)" title="Add to Wishlist" data-placement="top" data-container="body" data-toggle="tooltip">
                                                    <i class="fa fa-heart"></i>
                                                    <input type="hidden" value="24">
                                                </a>
                                            </div>-->
                                            <a href="<?=$url?>"><button class="btn-primary addtocart2" type="button" value="<?=$inventoryItem->id?>"><span><?=Yii::t('app', 'Buy Now')?></span></button></a>
                                        </div>
                                    </div>
                                </div>
                                <?php if($cnt == 3){
                                break;
                            }
                            } ?>
                            <!-- End -->
                        </div>
                    </div>

                    <!-- service area start -->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="jtv-service-area">

                            <!-- jtv-single-service start -->

                            <div class="jtv-single-service">
                                <div class="service-icon"> <img alt="24/7 Customer Service" src="https://ultimate.multecart.com/images/customer-service-icon.png"> </div>
                                <div class="service-text">
                                    <h2>24/7 Customer Service</h2>
                                    <p><span>Call us 24/7</span></p>
                                </div>
                            </div>
                            <div class="jtv-single-service">
                                <div class="service-icon"> <img alt="Free Shipping Worldwide" src="https://ultimate.multecart.com/images/shipping-icon.png"> </div>
                                <div class="service-text">
                                    <h2>Free Shipping Worldwide</h2>
                                    <p><span>On Applicable Orders</span></p>
                                </div>
                            </div>
                            <div class="jtv-single-service">
                                <div class="service-icon"> <img alt="Money Back Guarantee!" src="https://ultimate.multecart.com/images/guaratee-icon.png"> </div>
                                <div class="service-text">
                                    <h2>Money Back Guarantee!</h2>
                                    <p><span>For Returnable Products</span></p>
                                </div>
                            </div>

                            <!-- jtv-single-service end -->

                        </div>
                    </div>
                </div>
            </div>

                <!-- category-area end -->
                <div class="footer-newsletter">
                    <!--<div class="container">-->
                        <div class="row">
                            <div class="col-md-8 col-sm-7">
                                <form id="newsletter-validate-detail" method="post" action="/site/news-signup">
                                    <input type="hidden" name="_csrf" value="tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ==">
                                    <h3 class="hidden-sm">Sign up for newsletter</h3>
                                    <div class="newsletter-inner">
                                        <input class="newsletter-email" name="newsemail" placeholder="Enter Your Email"/>
                                        <button class="button subscribe" type="submit" title="Subscribe">Subscribe</button>
                                    </div>
                                </form>
                            </div>
                            <div class="social col-md-4 col-sm-5 social-icons">
                                <ul class="inline-mode">
                                    <li class="social-network fb"><a title="Join us on Facebook" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
                                    <li class="social-network googleplus"><a title="Join us on Google+" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="social-network tw"><a title="Join us on Twitter" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
                                    <li class="social-network linkedin"><a title="Join us on LinkedIn" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="social-network youtube"><a title="Join us on YouTube" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
                                    <li class="social-network instagram"><a title="Join us on Instagram" data-toggle="tooltip" data-placement="bottom" target="_blank" href="http://www.instagram.com"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    <!--</div>-->
                </div>
</body>

<script>
    $(document).ready(function()
    {

        $(".mylazy").lazyload({
            event : "turnPage",
            effect : "fadeIn"
        });

        $('#notshowpopup').change(function() {
            //if($(this).is(':checked'))
            $.post("/site/ajax-unset-news-popup", { '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
            })

        });

        jQuery('.tp-banner').revolution(
            {
                delay:9000,
                startwidth:1170,
                startheight:530,
                hideThumbs:10,

                navigationType:"bullet",
                navigationStyle:"preview1",
                hideArrowsOnMobile:"on",
                touchenabled:"on",
                onHoverStop:"on",
                spinner:"spinner4"
            });

        if(51703670 > 0)
            CountBack_slider(51703670,"countbox_1", 1);

    })
</script>

<script>
    function Add_Error(obj,msg){
        $(obj).parents('.form-group').addClass('has-error');
        $(obj).parents('.form-group').append('<div style="color:#D16E6C; clear:both" class="error"><i class="icon-remove-sign"></i> '+msg+'</div>');
        return true;
    }

    function Remove_Error(obj){
        $(obj).parents('.form-group').removeClass('has-error');
        $(obj).parents('.form-group').children('.error').remove();
        return false;
    }

    function Add_ErrorTag(obj,msg){
        obj.css({'border':'1px solid #D16E6C'});

        obj.after('<div style="color:#D16E6C; clear:both" class="error"><i class="icon-remove-sign"></i> '+msg+'</div>');
        return true;
    }

    function Remove_ErrorTag(obj){
        obj.removeAttr('style').next('.error').remove();
        return false;
    }

    //$(document).ready(function () {
    //$(".add_to_wishlist").click(function() {
    $(document).on('click', '.add_to_wishlist', function() {
        $.post("/site/add-to-wishlist", { 'id': $('input', this).val(), '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
            if(result == -1)
            {
                window.location.href = '/login';
            }
            else if(result == -2)
            {
                $('.wishexistmodal').modal('show');
                setTimeout(function() {$('.wishexistmodal').modal('hide');}, 1500);
            }
            else
            {
                $('.mywishlistcount').html(result);
                $('.wishconfirmmodal').modal('show');
                setTimeout(function() {$('.wishconfirmmodal').modal('hide');}, 1500);
            }
        })
    });

    //$(".add_to_compare").click(function() {
    $(document).on('click', '.add_to_compare', function() {
        $.post("/site/add-to-comparelist", { 'id': $('input', this).val(), '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
            if(result == -1)
            {
                $('.compareexistmodal').modal('show');
                setTimeout(function() {$('.compareexistmodal').modal('hide');}, 1500);
            }
            else if(result == -2)
            {
                $('.comparemaxmodal').modal('show');
                setTimeout(function() {$('.comparemaxmodal').modal('hide');}, 1500);
            }
            else
            {
                $('.mycomparelistcount').html(result);
                $('.compareconfirmmodal').modal('show');
                setTimeout(function() {$('.compareconfirmmodal').modal('hide');}, 1500);
            }
        })
    });

    /*$( ".basket" ).click(function() {
          setTimeout(function() {$( ".top-cart-content" ).trigger("click");}, 1);
        });*/

    $('.dropdown-toggle').dropdown();

    $('.multe-rating').rating({
        'showClear': false,
        'showCaption': true,
        'stars': '5',
        'min': '0',
        'max': '5',
        'step': '1',
        'size': 'xs',
        'starCaptions': {0: "Not Rated", 1: "Poor", 2: "Fair", 3: "Good", 4: "Very Good", 5: "Excellent"}
    });

    $('.multe-rating-nocap').rating({
        'showClear': false,
        'showCaption': false,
        'stars': '5',
        'min': '0',
        'max': '5',
        'step': '1',
        'size': 'xs'
    });

    $('.multe-rating-sm').rating({
        'showClear': false,
        'showCaption': true,
        'stars': '5',
        'min': '0',
        'max': '5',
        'step': '1',
        'size': 'xxs',
        'starCaptions': {0: "Not Rated", 1: "Poor", 2: "Fair", 3: "Good", 4: "Very Good", 5: "Excellent"}
    });

    $('.multe-rating-nocap-sm').rating({
        'showClear': false,
        'showCaption': false,
        'stars': '5',
        'min': '0',
        'max': '5',
        'step': '1',
        'size': 'xxs'
    });

    //});

    $(window).load(function() {
        // Animate loader off screen
        $(".se-pre-con").fadeOut("slow");;
    });

    $(".mainlazy").lazyload({
        event : "turnPage",
        effect : "fadeIn"
    });

</script>

<script>
    $(document).ready(function(e) {
        $(document).on('click', '.addtocart', function () {
            var cart = $('.mycart');
            var imgtodrag = $(this).parent().parent('.product-thumbnail').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');



                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }

            $.post("/order/default/ajax-add-to-cart", { 'inventory_id': $(this).val(), 'total_items' : '1', '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
                $('.mini-products-list').html(result);
                $('.cartcount').html($('.hiddencartvalue').val() + ' ' + 'Item(s)');
                $('.confirmmodal').modal('show');
                setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
            })
        });

        $('.addtocartmini').on('click', function () {
            var cart = $('.mycart');
            var imgtodrag = $(this).closest('.jtv-product').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');



                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }

            $.post("/order/default/ajax-add-to-cart", { 'inventory_id': $(this).val(), 'total_items' : '1', '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
                $('.mini-products-list').html(result);
                $('.cartcount').html($('.hiddencartvalue').val() + ' ' + 'Item(s)');
                $('.confirmmodal').modal('show');
                setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
            })
        });

        $('.wish-add-to-cart').on('click', function () {
            var cart = $('.mycart');
            var imgtodrag = $(this).closest('tr').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');



                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }

            $.post("/order/default/ajax-add-to-cart", { 'inventory_id': $(this).val(), 'total_items' : '1', 'wish' : 'true', '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
                $('.mini-products-list').html(result);
                $('.cartcount').html($('.hiddencartvalue').val() + ' ' + 'Item(s)');
                $('.confirmmodal').modal('show');
                setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
                setTimeout(function() {location.reload();}, 1500);
            })
        });

        $('.compare-add-to-cart').on('click', function () {
            var cart = $('.mycart');
            var imgtodrag = $(this).closest('table').find("img").eq(0);
            if (imgtodrag) {
                var imgclone = imgtodrag.clone()
                    .offset({
                        top: imgtodrag.offset().top,
                        left: imgtodrag.offset().left
                    })
                    .css({
                        'opacity': '0.5',
                        'position': 'absolute',
                        'height': '150px',
                        'width': '150px',
                        'z-index': '100'
                    })
                    .appendTo($('body'))
                    .animate({
                        'top': cart.offset().top + 10,
                        'left': cart.offset().left + 10,
                        'width': 75,
                        'height': 75
                    }, 1000, 'easeInOutExpo');



                imgclone.animate({
                    'width': 0,
                    'height': 0
                }, function () {
                    $(this).detach()
                });
            }

            $.post("/order/default/ajax-add-to-cart", { 'inventory_id': $(this).val(), 'total_items' : '1', '_csrf' : 'tDa4U-0zMZNWKhsxCvlbXh8Kb7srBxonzyC_B0QmNiGBXcgfpgdp5RtdVkhQjTI6d1A3y0xjYEChdvFoM211ZQ=='}) .done(function(result){
                $('.mini-products-list').html(result);
                $('.cartcount').html($('.hiddencartvalue').val() + ' ' + 'Item(s)');
                $('.confirmmodal').modal('show');
                setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
                setTimeout(function() {location.reload();}, 1500);
            })
        });
    });
</script>

<script>
    wow = new WOW(
        {
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
    };
</script>
<script>
$('.hiddencartvalue').val() + ' ' + 'Item(s)');
                $('.confirmmodal').modal('show');
                setTimeout(function() {$('.confirmmodal').modal('hide');}, 1500);
                setTimeout(function() {location.reload();}, 1500);
            })
        });
    });
</script>

<script>
    wow = new WOW(
        {
            animateClass: 'animated',
            offset:       100,
            callback:     function(box) {
                console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
            }
        }
    );
    wow.init();
    document.getElementById('moar').onclick = function() {
        var section = document.createElement('section');
        section.className = 'section--purple wow fadeInDown';
        this.parentNode.insertBefore(section, this);
    };
</script>