<?php
use multebox\models\Cart;
use multebox\models\Inventory;
use multebox\models\File;
use multebox\models\Vendor;
use multebox\models\Address;
use multebox\models\Contact;
use multebox\models\search\MulteModel;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use multebox\models\Country;
use multebox\models\State;
use multebox\models\City;
use multebox\models\PaymentMethods;
?>






<section class="py-5 text-center" id="obaid">

    <form method="post" novalidate  action="<?=Yii::$app->homeUrl?>site/info" enctype="multipart/form-data">

        <input type="hidden" name="_csrf" value="<?php echo $this->renderDynamic('return Yii::$app->request->csrfToken;'); ?>">

        <?php $lang = $_REQUEST['lang'];
        if($lang){?>
            <input type="hidden" name="lang" value="ur-UR">
        <?php }?>

    <div class="container mt-6">
        <div class="row">
            <?php
            $customer = \Yii::$app->user->identity->entity_id;


            $order = \multebox\models\Order::find()->where(['=','customer_id',$customer])->all();
            /*echo '<pre>';
            echo print_r($order);
            echo '</pre>';
            exit;*/




            if(count($order) == 0)
            {
                ?>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <td><h1 class="title font"><?=Yii::t('app', 'Order history is empty.')?></h1></td>
                        </tr>
                        </thead>
                    </table>
                </div>

                <a href = "<?=Yii::$app->homeUrl?>site/index"><div class="btn btn-success pull-left btn-fyi font"><span
                                class="glyphicon glyphicon-chevron-left font"></span> <?= Yii::t('app','CONTINUE SHOPPING')?>
                    </div></a>
                <?php
            }
            else
            {
                ?>

                <div class="col-md-12 col-sm-12 mt-5">
                    <h2 class="cart-heading mb-5 text-left"><?=Yii::t('app','Shopping History')?></h2>

                    <table class="table table-hover font">
                        <thead>
                        <tr>
                            <th><?= Yii::t('app','Order')?></th>
                            <th><?= Yii::t('app','Date')?></th>
                            <th><?= Yii::t('app','Payment Plan')?></th>
                            <th>&nbsp<?= Yii::t('app','Order Status')?></th>
                            <th class="text-right"><?=Yii::t('app', 'Contract')?></th>
                            <th class="text-center"><?=  Yii::t('app','T.Amount')?></th>
                            <th class="text-center"><?=  Yii::t('app','P. Amount')?></th>
                            <th class="text-center"><?= Yii::t('app','Payment Status')?></th>
                            <th></th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total_cart_price = 0;
                        foreach ($order as $ord) {
                            $cart = \multebox\models\SubOrder::find()->where(['=', 'order_id', $ord->id])->orderBy(['id' => SORT_DESC])->one();
                            /*echo '<pre>';
                            echo print_r($cart_items);
                            echo '</pre>';
                            exit;*/





                                $inventory_item = Inventory::findOne($cart->inventory_id);


                               // $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
                                ?>
                                <tr class="bg-light-color">
                                    <td><?=$cart->id?></td>
                                    <!--<td class="col-sm-8 col-md-6">
                                        <div class="media">
                                            <a class="thumbnail pull-left" href="#">
                                                <div class="cart-product-container"><img
                                                            src="<?/*= Url::base() */?>/../../multeback/web/attachments/<?/*= '' */?><?/*= strrchr($fileDetails->file_name, ".") */?>"
                                                            class="media-object"></div>
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="#"><?/*= $prod_title */?></a></h4>

                                            </div>
                                        </div>
                                    </td>-->
                                    <td  style="text-align: center">
                                        <?= date('Y-m-d',$cart->added_at) ?>
                                    </td>
                                    <td class=" text-center"><strong><?php if ($cart->plan_id != 0) {
                                                echo MulteModel::getplan($cart->plan_id);
                                            } else {
                                                echo 'Full Cash';
                                            } ?></strong>
                                    </td>
                                    <td><?=$cart->sub_order_status?></td>
                                    <td class="text-right">
                                        <?php if($ord->agreement == 'yes'){
                                            echo 'Yes';
                                        }else{
                                            echo 'No';
                                        } ?>
                                    </td>
                                    <td class=" text-center">
                                        <strong><?= MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items)) ?></strong>
                                    </td>
                                    <?php
                                    $total_cart_price += MulteModel::getInventoryTotalAmount($inventory_item, $cart->total_items) * $cart->total_items;
                                    ?>
                                    <td class=" text-center">
                                        <strong><?php echo ($cart->plan_id != 0) ? MulteModel::formatAmount(round(MulteModel::getAdvance(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items), $cart->plan_id))) : MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items)) ?></strong>
                                    </td>
                                    <td class=" text-center"><strong><?php if ($cart->sub_order_status == 'NEW') {
                                                echo '<button type="button" class="btn btn-danger">
                                            <span class="glyphicon glyphicon-view">Not Paid
                                        </button>';
                                            } else {
                                                echo '<button type="button" class="btn btn-success">
                                            <span class="glyphicon glyphicon-view">Paid
                                        </button>';
                                            } ?></strong>
                                    </td>
                                    <td class="">
                                        <form method="post" novalidate  action="<?=Yii::$app->homeUrl?>site/info" enctype="multipart/form-data">
                                        <input type="hidden" name="_csrf" value="<?php echo $this->renderDynamic('return Yii::$app->request->csrfToken;'); ?>">
                                        <input type="hidden" value="<?=$cart->id?>" name="sub_order_id">
                                        <button href="<?=Yii::$app->homeUrl?>site/info" class="btn btn-info">
                                            <span class="glyphicon glyphicon-view"></span> <?= Yii::t('app', 'View') ?>
                                        </button>

                                        </form>
                                    </td>

                                </tr>
                            <?php
                        }
                        ?>

                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>

    </div>

    </form>
</section>


