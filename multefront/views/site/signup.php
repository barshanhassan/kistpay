


<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \multeback\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>

<style>

    .site-signup{
        background-image: url("image/background.jpg");
    }

    .log{
        text-align:center;
    }
    .form {
        margin: 0 auto !important;
        position: relative;
        top: 50%;

        width: 337px;

    }
    .form-control, .input-group-addon {
        border-radius: 4px;
    }
    .sign {
        width: 41.66666667%;
        background-color: #ebebebb5;
        margin-left: 31%;
        margin-bottom: 2%;
    }

    input[type="button"] {
        margin-left: -50px;
        height: 25px;
        width: 50px;
        
        color: white;
        border: 0;
        -webkit-appearance: none;
    }
    input.sndmail {
    position: absolute;
    top: 178px;
    left: 93%;
    width: 20%;
}
input.sndotp {
    position: absolute;
    top: 424px;
    left: 93%;
    width: 20%;
}

input#verify {
    height: 24px;
    width: 44px;
}
.btn {
    padding: 0 0px !important;
}

</style>
<div class="site-signup">
    <div class="row">
        <div class="col-lg-5 sign">

    <h1 class="log"><?= Html::encode($this->title) ?></h1>

    <p class="log"><?=Yii::t('app', 'Please fill out the following fields to signup')?></p>

            <div class="form">

            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>



				<?= $form->field($model, 'firstname') ?>

				<?= $form->field($model, 'lastname') ?>

                <?= $form->field($model, 'email', [
            'template' => '{beginLabel}{labelTitle}{endLabel}<div class="input-group">{input}
            <span class="input-group-addon"><button class="btn class="btnSubmit" id="esend" onclick="sendmail();">Send Code</button></span></div>{error}{hint}'
        ])->textInput(['id'=>'email']); ?>

                <?= $form->field($model, 'eotp',[
                    'template' => '{beginLabel}{labelTitle}{endLabel}<div class="input-group">{input}
            <span class="input-group-addon"><button class="btn class="btnSubmit vrify" id="everify" onclick="verifymail();">Verify Code</button></span></div>{error}{hint}'
                ])->textInput(['id'=>'emailotp']); ?>

                <?=$form->field($model, 'eotpcheck')->hiddenInput(['id'=>'emailotpcheck'])->label(false); ?>

                <?= $form->field($model, 'cnic') ?>

               <?=  $form->field($model, 'network')->dropDownList(['1' => 'Jazz', '2' => 'Ufone','3'=>'Telenor','4'=>'Zong'],['prompt'=>'Select Option']) ?>

               <?= $form->field($model, 'mobile', ['template' => '{beginLabel}{labelTitle}{endLabel}<div class="input-group">{input}
                <span class="input-group-addon"><button class="btn class="btnSubmit" id="mobsend" onclick="sendOTP();">Send Code</button></span></div>{error}{hint}'
                ])->textInput(['id' => 'mobile']); ?>

                <?= $form->field($model, 'mobotp', ['template' => '{beginLabel}{labelTitle}{endLabel}<div class="input-group">{input}
                <span class="input-group-addon"><button id="mobverify" class="btn =class"btnSubmit" onclick="verifyOTP();">Verfy Code</button></span></div>{error}{hint}'
                ])->textInput(['id' => 'mobileOtp']); ?>

                <?=$form->field($model, 'mobotpcheck')->hiddenInput(['id'=>'mobmailotpcheck'])->label(false); ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Signup'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

            </div>
        </div>
        </div>
</div>



