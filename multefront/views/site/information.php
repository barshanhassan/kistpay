<style>
    .buttons {
        position: relative;
        width: 96%;
    }

    .table {
        width: 92%;
        margin-bottom: 1rem;
        color: #212529;
        margin: 0 auto !important;
    }

    .row-top{width: 100%;}
    .col-left-reg{ float: left; width: 34%; font-size:8.5pt}
    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .col-center{float: left; width: 34%;}
    .logo{ width: 100%; height: auto;}
    .col-pic{float: left; width: 30%;}
    .pic{ float: right; width: 130px; height: 140px; border: 1px solid #000; margin-right: 5px;}
    .pic img{width: 100%; height: 100%}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif;}
    .span{ background: #f5f5f5; padding:5px; margin: 0px;}

    .d-line{width: 100%; float: left; background:#f5f5f5; padding:3px; margin-top:3px;font-size: 8pt}
    .d-line span{padding:3px; font-weight: bold; font-size: 8pt}
    .half-left{ float: left; width: 60%; }
    .half-right{ float: left; width: 40%; }
    .membership{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt;margin-top:8px;}
    .membership h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}

    .bill-d{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:9pt; margin-top: 10px;}
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; font-size:14pt}
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}
    #b-line{width: 100%; float: left; padding:3px; margin-top:2px;}
    .b-d-left{float: left; width: 45%;background:#f5f5f5; padding: 2px; }
    .b-d-right{float: right; width: 43%;background:#f5f5f5; padding: 2px; }
    #b-line span{padding:3px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center}


    .text-center{ text-align:center;}
    .d-line abel {
        display: inline-block;
        max-width: 100%;
        margin-bottom: 0px;
        font-weight: bold;
    }

    .col-lg-4 {
        width: 33.33333333%;
        float: left;
    }


    /*Uzair Code*/

    .pad-left-right{padding-left: 0px; padding-right: 0px;}
    .form{float: left; width: 100%;margin-top: 23px; margin-left: 4px;}
    .col-center{margin-top: 13px;}
    .centerpad{padding-left: 10px; padding-right: 10px;margin-top: 0px;margin-bottom:0;}

    .terms{width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif; font-size:7.9pt; padding-left: 10px;
        padding-top: 4px !important; background: #efefef !important;}
    .terms h2{ text-align: center; margin: 3px; }
    .terms ul{list-style-type:decimal;}
    .terms ul li{ margin-bottom: 3px;}
    .detail{ width: 100%; float: left; font-family: Verdana, Arial, Helvetica, sans-serif;font-size:9pt}
    .detail h2{margin: 0 0 5px 0; font-family: "Times New Roman", Georgia, Serif; font-size: 20px;}
    .span{ background: #f5f5f5; padding:5px; margin: 0px;}
    .subspan{font-size: 9pt;}

    .half-left{ float: left; width: 60%; }
    .half-right{ float: left; width: 40%; }

    .secondpage{ }
    .bill-d h2{margin: 5px 0 5px 0; font-family:"Times New Roman", Georgia, Serif; }
    .bill-d h3{margin: 5px 0 2px 0; font-family:"Times New Roman", Georgia, Serif; font-size:16pt}

    #b-line span{padding:5px 0px; font-weight: bold}
    .b-d-sign{float: right; width: 43%; }
    .sign{ border-top: 1px solid #000; padding-top: 10px; text-align: center;background: transparent !important;}

    .text-center{ text-align:center;}



    .top10{
        margin-top: 0px !important;
    }
    /************ For Printing *****/

    .blur-content {
        -webkit-filter: url(#svg-blur);
        filter: url(#svg-blur);

    }
    #Panes {
        position: relative;
        z-index: 10;
    }

</style>

<?php
use multebox\models\Order;
use multebox\models\OrderStatus;
use multebox\models\City;
use multebox\models\State;
use multebox\models\Country;
use multebox\models\SubOrder;
use multebox\models\PaymentMethods;
use multebox\models\search\MulteModel;
use multebox\models\Inventory;
use multebox\models\DigitalRecords;
use multebox\models\File;
use multebox\models\Vendor;
use multebox\models\Cart;
use multebox\models\LicenseKeyCode;
use yii\helpers\Url;
use yii\helpers\Json;

?>

<script>
$(document).on("click", '#cancelitem', function(event)
{
	if (confirm('Are you sure You want to cancel this item from the order?'))
	{
		$('.tooltip-inner').remove();
		$('.tooltip-arrow').remove();

		var sub_order_id = $(this).val();
		$.post("<?=Url::to(['/order/default/cancel-sub-order'])?>", { 'sub_order_id': sub_order_id, '_csrf' : '<?=Yii::$app->request->csrfToken?>'}) .done(function(result){
					//alert(result);
					$('.orderitems tbody').html(result);
				});

		$('body').tooltip({
			selector: '[data-toggle="tooltip"]'
		});
	}
});
</script>
<?php
$entity = Yii::$app->user->identity->entity_id;
$address = multebox\models\Address::find()->where('entity_id='.$entity)->one();//Json::decode($order->address_snapshot);
$contact = multebox\models\Contact::find()->where('entity_id='.$entity)->one();// Json::decode($order->contact_snapshot);
?>
<section class="mt-6 py-5">
    <div class="container-fluid">

      <div class="row">
        <!--Middle Part Start-->

        <div id="" class="col-sm-6">

            <form style="margin-left: 122px; margin-top: 100px;">
                <h2>Customer Details</h2>
                <div class="col-10">
                    <div class="form-group">
                        <input type="text" required="required" value="<?=$contact['first_name']?><?=$contact['last_name']?>">
                        <label for="input" class="control-label">FULL NAME</label><i class="bar"></i>
                    </div>
                </div>
                <div class="col-10">
                    <div class="form-group">
                        <input type="email" required="required" value="<?=Yii::$app->user->identity->email?>">
                        <label for="input" class="control-label">EMAIL</label><i class="bar"></i>
                    </div>
                </div>

            </form>
        </div>

            <div id="" class="col-sm-6">
                <form style="margin-top: 110px;">
                    <br>
                    <div class="col-8">
                        <div class="form-group">
                            <input type="text" required="required" value="<?=$address['address_1']."-".$address['address_2']."-".Country::findOne($address['country_id'])->country?>">

                            <label for="input" class="control-label">Address</label><i class="bar"></i>
                        </div>

                    </div>
                    <div class="col-8">
                        <div class="form-group">
                            <input type="mobile" required="required" value="<?=Yii::$app->user->identity->username?>">
                            <label for="input" class="control-label">Mobile</label><i class="bar"></i>
                        </div>
                    </div>
                </form>
            </div>


            </div>
            <div class="pull-left" style="margin-left: 4%; margin-top: 4%;" >

            <?php
            /*$order->id = $_REQUEST['order_id'];
            echo '<div class="alert alert-success">
            <strong>'.Yii::t("app","Congratulations!") .'</strong>&nbsp &nbsp Your Order has been Placed Succefully and detail is give below if you have any Query feel free to contact us at info@kistpay.com</div>';
            */?>
            <h1 class="title"><?=Yii::t('app', 'Order')?> #<?=$order->id?> - <?=Yii::t('app', $order->status->label)?></h1>
        </div>
        <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td colspan="3" class="text-left"><?=Yii::t('app', 'Order Details')?></td>
          </tr>
        </thead>
        <tbody>
          <!--<tr>
            <td style="width: 33%;" clpull-leftass="text-left">
			  <b><?/*=Yii::t('app', 'Order ID')*/?>:</b> No.<?/*=$order->id*/?><br>
              <b><?/*=Yii::t('app', 'Order Date')*/?>:</b> <?/*=date('M d, Y, H:i', $order->added_at)*/?>
			</td>
            <td style="width: 33%;" class="text-left">
			  <b><?/*=Yii::t('app', 'Payment Method')*/?>:</b> <?/*=Yii::t('app', PaymentMethods::find()->where("method='".$order->payment_method."'")->one()->label)*/?><br>
              <b><?/*=Yii::t('app', 'Shipping Method')*/?>:</b> <?/*=Yii::t('app', 'Flat Rate Shipping')*/?>
			</td>
			<td style="width: 33%;" class="text-left">

			  <b><?/*=Yii::t('app', 'Shipping Address')*/?></b><br>
			  <?/*=$contact['first_name']*/?> <?/*=$contact['last_name']*/?><br>
			  <?/*=$address['address_1']*/?><br>
			  <?/*=$address['address_2']*/?><br>
			  <?/*=City::findOne($address['city_id'])->city*/?><br/>
			  <?/*=State::findOne($address['state_id'])->state*/?><br/>
			  <?/*=Country::findOne($address['country_id'])->country*/?> - <?/*=$address['zipcode']*/?><br/>
			  <?/*=Yii::t('app', 'Phone')*/?>: <?/*=$contact['mobile']*/?><br/>
			</td>
          </tr>-->
        </tbody>
      </table>
      
      <div class="table-responsive">
        <table class="table table-bordered table-hover orderitems">
          <thead>
            <tr>
                <td class="text-left"><?=Yii::t('app', 'Order ID')?></td>
			  <td class="text-left"><?=Yii::t('app', 'Product Name')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Quantity')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Unit Price')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Payment Plan')?></td>
		  	  <td class="text-right"><?=Yii::t('app', 'Total')?></td>
			  <td class="text-right"><?=Yii::t('app', 'Payment Method')?></td>
              <td style="width: 20px;"></td>
            </tr>
          </thead>
          <tbody>
		  <?php
		 // $cart_items = MulteModel::mapJsonArrayToModelArray(Json::decode($order->cart_snapshot), new Cart);
          $cart= $order;
			//$cart = MulteModel::mapJsonToModel($cart, new Cart);
			//var_dump($cart);exit;
              $inventory_item = Inventory::findOne($cart['inventory_id']);
              $prod_title = $inventory_item->product_name;
			//$fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
		  ?>
		  <tr>
              <td class="text-left"><?=$order->id?></td>
              <td class="text-left"><?=$prod_title?><br/></td>
			<td class="text-left">
			  <div class="text-right">
				<?=$cart->total_items?>
			  </div>
			</td>
			<td class="text-right"><?=MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items))?></td>
			<td class="text-right"><?= ($cart->pay_process_id) == 1 ? 'Installments': 'Full Cash'?></td>

            <td>
                <?php if($cart->plan_id == Null ||$cart->plan_id == 0){
                    echo MulteModel::formatAmount(MulteModel::getAdvance($cart->total_cost, $cart->plan_id));

                }
                else{
                    echo MulteModel::formatAmount($cart->total_cost);
                }?>
            </td>
			<?php
			if($sub_order->sub_order_status == OrderStatus::_CANCELED)
			{
				$label_color = 'label-danger';
			}
			else if($sub_order->sub_order_status == OrderStatus::_RETURNED)
			{
				$label_color = 'label-warning';
			}
			else if($sub_order->sub_order_status == OrderStatus::_REFUNDED)
			{
				$label_color = 'label-success';
			}
			else
			{
				$label_color = 'label-info';
			}
			?>
			<td class="text-right"><?php
                if($cart->payment_method == 'PFT'){
                    echo 'Payfast';
                }else{
                    echo 'COD';
                }
                Yii::t('app', $cart->payment_method)?></td>
			<?php
			if($sub_order->sub_order_status != OrderStatus::_REFUNDED && $sub_order->sub_order_status != OrderStatus::_CANCELED)
			{
				$total_cart_price += MulteModel::getInventoryTotalAmount($inventory_item, $cart->total_items)*$cart->total_items;
			}
			?>
							  
              <td style="white-space: nowrap;" class="text-center">
                  <?php if($order->sub_order_status == OrderStatus::_NEW && $order->payment_method != PaymentMethods::_COD){

                  $merchantid = "204";
                  $secret = "bTO3NfZJaHTYXL8c";
                  /**
                   * token URL
                   */
                  $token_url = "https://ipg1.apps.net.pk/Ecommerce/api/Transaction/GetAccessToken?MERCHANT_ID=".$merchantid."&SECURED_KEY=".$secret;
                  $contents = file_get_contents($token_url);

                  /**
                   * decode JSON returned data
                   */
                  $token_info = json_decode($contents);
                  $token = "";
                  if (isset($token_info->ACCESS_TOKEN)) {
                      $token = $token_info->ACCESS_TOKEN;

                  }
                  $payment = ($cart->plan_id != 0) ? round(MulteModel::getAdvance(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items), $cart->plan_id)) : MulteModel::formatAmount(MulteModel::getInventoryActualPrice($inventory_item) - MulteModel::getInventoryDiscountAmount($inventory_item, $cart->total_items));
                      ?>

                  <form method="post" id="checkoutform" action="https://ipg1.apps.net.pk/Ecommerce/api/Transaction/PostTransaction">

                      <INPUT TYPE="hidden" NAME="MERCHANT_ID" target="payfast" VALUE="<?=$merchantid; ?>">
                      <INPUT TYPE="hidden" NAME="MERCHANT_NAME" value="KistPay">
                      <INPUT TYPE="hidden" NAME="TOKEN" VALUE="<?=$token?>">
                      <INPUT TYPE="hidden" NAME="PROCCODE" VALUE="00">
                      <INPUT TYPE="hidden" NAME="SIGNATURE" VALUE="RANDOMSjjhjTRINGVALUE">
                      <INPUT TYPE="hidden" NAME="VERSION" VALUE="MY_VER_1.0">
                      <INPUT TYPE="hidden" NAME="SUCCESS_URL" VALUE="http://kistpay.com/">
                      <INPUT TYPE="hidden" NAME="FAILURE_URL" VALUE="http://kistpay.com/multefront/web/site/index?">
                      <INPUT TYPE="hidden" NAME="ORDER_DATE" VALUE="<?=date('Y-m-d')?>">
                      <INPUT TYPE="hidden" NAME="CHECKOUT_URL" VALUE="http://merchantsite.com/order/backend/confirm">
                      <INPUT TYPE="hidden" NAME="TXNAMT" VALUE ="<?php echo $payment?>">
                      <INPUT TYPE="hidden" NAME="BASKET_ID" VALUE="<?=$cart->id?>">
                      <INPUT TYPE="hidden" NAME="TXNDESC" VALUE="<?=$prod_title?>">
                      <INPUT TYPE="hidden" NAME="CUSTOMER_MOBILE_NO" VALUE="<?=Yii::$app->user->identity->mobile?>">
                      <INPUT TYPE="hidden" NAME="CUSTOMER_EMAIL_ADDRESS" VALUE="<?=Yii::$app->user->identity->email?>">
                      <button type="submit" class="btn btn-success pull-right btn-fyi font"><?=Yii::t('app', 'Proceed to Payment')?></button><span class="glyphicon glyphicon-chevron-right"></span>


                  <?php }else{?>
                        <?=$sub_order->sub_order_status?>
                 <?php }

                  ?>
				<!--<a href="<?/*=Url::to(['/order/default/information', 'order_id' => $order->id, 'cancel_id' => $sub_order->id])*/?>" data-toggle="tooltip" title="<?/*=Yii::t('app', 'Cancel Item')*/?>" id="cancelitem" onclick="return confirm('<?/*=Yii::t ('app','Are you Sure!')*/?>')"><i class="fa fa-times-circle"></i></a>
                <a href="<?/*=Url::to(['/order/default/information', 'order_id' => $order->id, 'return_id' => $sub_order->id])*/?>" data-toggle="tooltip" title="<?/*=Yii::t('app', 'Return Item')*/?>" id="returnitem" onclick="return confirm('<?/*=Yii::t ('app','Are you Sure!')*/?>')"><i class="fa fa-reply"></i></a>
                -->
			  </td>

            </tr>
	      <?php


		  $global_discount = $order->total_site_discount; 
		  $coupon_discount = $order->total_coupon_discount;
							
			if ($global_discount > 0)
			{
			?>
			  <tr>
				<input type="hidden" name="special_discount" value="<?=$global_discount?>">
				<td class="text-right" colspan="9"><strong><?=Yii::t('app', 'Total Special Discount')?>:</strong></td>
				<td class="text-right"><?=MulteModel::formatAmount($global_discount)?></td>
			  </tr>
			<?php
			}
			
			if ($coupon_discount > 0)
			{
			?>
			  <tr>
				<input type="hidden" name="coupon_discount" value="<?=$coupon_discount?>">
				<td class="text-right" colspan="9"><strong><?=Yii::t('app', 'Total Coupon Discount')?> (<?=Json::decode($order->discount_coupon_snapshot)['coupon_code']?>):</strong></td>
				<td class="text-right"><?=MulteModel::formatAmount($coupon_discount)?></td>
			  </tr>
			<?php
			}
			?>

			  <tr>
				<input type="hidden" name="coupon_discount" value="0">
				<input type="hidden" name="total_cost" value="<?=$total_cart_price - $global_discount?>">
				<td class="text-right" colspan=9><strong><?=Yii::t('app', 'Total Amount')?>:</strong> <?php if($cart->plan_id == Null ||$cart->plan_id == 0){
                        echo MulteModel::formatAmount(MulteModel::getAdvance($cart->total_cost, $cart->plan_id));

                    }
                    else{
                        echo MulteModel::formatAmount($cart->total_cost);
                    }?></td>
			  </tr>


          </tbody>
        </table>
      </div>
                 
      <!--<div class="buttons clearfix">
        <div class="pull-right"><a class="btn btn-primary" href="<?/*=Url::to(['/site/index'])*/?>"><?/*=Yii::t('app', 'Continue')*/?></a></div>
      </div>-->



        </div>
        <!--Middle Part End -->
      </div>
    </div>

    <?php

    $order = Order::findOne($cart->order_id);
    /*echo '<pre>';
    echo print_r($order);
    echo $order['agreement'];
    exit;*/
    if($order['agreement'] == 'yes'){ ?>
        <div class="container-fluid" style="width: 70%;padding-right: 15px;padding-left: 15px;margin-right: auto;margin-left: auto;">
            <div class="centerpad">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="top10">
                            <div class="col-left-reg  pad-left-right">
                                <div class="form">
                                </div>
                            </div>

                            <div class="col-center  pad-left-right text-center">
                                <div class="col-xs-12">
                                    <h4>Digital Adgreement</h4>
                                    <!--<img src="../web/images/logo.jpg"  width="200">-->
                                </div>


                            </div>

                        </div>

                        <div class="terms">
                            <br>
                            Please carefully read all of the membership policies and terms. We request all prospective members to adhere to the Kistpay’s
                            policies and procedures. Failure may lead to cancellation of membership without any refund or liability to the Kistpay.
                            <br><br>
                        </div><!--close terms-->
                    </div>
                </div><!-- close row-top-->

                <div class="blur-content">
                    <div id="Panes">
                        <div class="detail">
                            <h2><span class="span">Personal Detail</span></h2>
                            <div class="d-line">
                                <span>Name </span> <?php echo 'Barshan Hassan' ?>
                            </div>

                            <div class="d-line">
                                <span>Father Name </span> <?php echo "Hassan" ?>
                            </div>


                            <div class="d-line">
                                <span>Address </span> <?php echo "Islamabad" ?>,
                            </div>

                            <div class="d-line">
                                <span>Business Adress </span> <?php echo "Islamabad" ?>
                            </div>

                            <div class="d-line">
                                <span>Occupation </span> <?php echo "Job" ?>
                            </div>


                            <div class="d-line">
                                <div class="half-left"><span>Contact Number </span> <?php echo '03069091792'; ?></div>
                                <div class="half-right"><span>Email </span> <?php echo 'barshanhassan@gmail.com'; ?></div>
                            </div>

                            <div class="d-line">
                                <div class="half-left"><span>CNIC # </span> <?php echo '8978978784'; ?></div>


                            </div>

                        </div><!-- personal Detail-->


                        <div class="bill-d">
                            <!--<h2><span class="span">Office Use</span></h2>-->
                            <h2><span class="span">Billing Detail&nbsp;</span></h2>

                            <div id="b-line">
                                <div class="b-d-left"><span class="billtitle">Registration Charges</span> : <?php echo  '878'; ?></div>
                                <div class="b-d-right"><span class="billtitle">Registration Discount</span> : <?php echo  '30'; ?></div>
                            </div>

                            <div id="b-line">
                                <div class="b-d-left"><span class="billtitle">Membership Charges</span> : <?php echo  '100'; ?>	</div>
                                <div class="b-d-right"><span class="billtitle">Membership Discount</span> : <?php echo  '2000'; ?>	</div>
                            </div>
                        </div><!-- Membership type-->


                        <div class="uzair" style="page-break-before: always; float: left">&nbsp;
                        </div>

                        <div class="terms secondpage">
                            <h2>Rules & Regulations</h2>
                            <div class="">
                                <div class="terms">
                                    <ul>
                                        <li>
                                            Membership to any facility of Megazone is subject to the approval of Management, the Management reserves the right to reject any application for membership of the facility for any reason whatsoever
                                        </li>
                                        <li>
                                            Membership to any facility of Megazone is Non-transferable, Non Freezable and Non Refundable under any circumstances.
                                        </li>
                                        <li>
                                            No one is allowed to bring with them any sort of drugs or intoxicating substance, violation of the rule shall result in immediate termination of membership with no refunds.
                                        </li>
                                        <li>
                                            Management shall observe the attitude and behavior of the members towards staff and other members, any misconduct, ill mannered attitude or violating behavior shall result in termination of your membership without any refund.
                                        </li>
                                        <li>
                                            All members are advised to take care of their belongings/valuables; Management shall not be responsible / liable for any loss of article and valuables.
                                        </li>
                                        <li>
                                            Members and Guests shall not bring any food into the fitness club/Swimming Pool at any time; beverages consumed during workout must be in a container with a lid.
                                        </li>
                                        <li>
                                            No Pets are allowed in the premises of Megazone.
                                        </li>
                                        <li>
                                            Gym Facility is available for Gym members only, any guest, family member or mates shall not be allowed in the Gym area.
                                        </li>
                                        <li>
                                            Members are responsible for their own health and safety, and Megazone will not be responsible or liable to any compensation for any accident or injury.
                                        </li>
                                        <li>
                                            Any Person having any medical issue such as Epilepsy, Asthma, Heart issues or any other medical problem which may cause serious attack; are advised to consult with their own health Expert/Doctor before using the facility, Megazone shall not be responsible of any mishap or accident during or after the Workout / Swimming.
                                        </li>
                                        <li>
                                            Children under 6 years of age or under the height of 45” (45 Inches) are not allowed in the Swimming Pool.
                                        </li>
                                        <li>
                                            Children under 12 Years of age must be accompanied by a Parent or an adult guardian at the Swimming Pool
                                        </li>
                                        <li>
                                            Megazone will not refund any fee or portion in case a member wishes to terminate his/her membership or is unable to utilize facilities of the center any reason whatsoever.
                                        </li>
                                        <li>
                                            Management reserves the rights of Admission.
                                        </li>
                                        <li>
                                            Opening and Closing hours are established by the Management and can be revised without prior notice.
                                        </li>
                                        <li>
                                            Registration fee is valid for 3 months if any member does not renew his/her membership within three (3) months for any reason, he/she has to pay full registration fee to restore / Activate his/her membership afterwards with Regular fee/Charges (Non Discounted Charges) shall be applicable.
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="terms">
                <br>
                I have read acknowledged and agreed to all the terms and conditions; I certify the information provided by me is correct. I understand that non-compliance from my end may lead to termination of my membership without any obligation or liability towards
                <br><br>
            </div><!--close terms-->
            <!--<p class="centerpad">
                I have read acknowledged and agreed to all the terms and conditions; I certify the information provided by me is correct. I understand that non-compliance from my end may lead to termination of my membership without any obligation or liability towards.
            </p>-->

            <div  style="bottom: 10px !important; margin-top: 40px; !important" id="agreed_1">

                <div class=" b-d-left sign" >Sign</div>

            </div>




        </div><!--close centerpad-->
        </div><!-- close container1-->

              <!-- SVG Blur Filter -->
              <!-- 'stdDeviation' is the blur amount applied -->
        <svg id="svg-filter">
            <filter id="svg-blur">
                <feGaussianBlur in="SourceGraphic" stdDeviation="4"></feGaussianBlur>
            </filter>
        </svg>

    <?php } ?>
</section>

<!--<div style="font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif !important;color: BLACK !important; font-size: 96px !important; left: 25% !important; opacity: 0.21 !important; position: fixed !important; top: 45% !important; -ms-transform: rotate(-40deg) !important; -webkit-transform: rotate(-40deg) !important; transform: rotate(-40deg) !important;display: block !important;">Kistpay</div>-->

