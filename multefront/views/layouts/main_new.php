<?php

/* @var $this \yii\web\View */
/* @var $content string */

use multefront\assets\AppAsset;
use multefront\assets\AppAssetRTL;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use multebox\widgets\Alert;
use multebox\models\ProductBrand;
use multebox\models\ProductCategory;
use multebox\models\ProductSubCategory;
use multebox\models\ProductSubSubCategory;
use multebox\models\Cart;
use multebox\models\Inventory;
use multebox\models\File;

if(Yii::$app->params['RTL_THEME'] == 'No')
    AppAsset::register($this);
else
    AppAssetRTL::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode('KistPay') ?></title>
    <?php $this->head() ?>
    <meta name="format-detection" content="telephone=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="<?=Url::to(['/image/favicon.png'])?>" rel="icon" />
    <meta name="description" content="EMI: vendor ecommerce system">

    <style id="compiled-css" type="text/css">

        /* @font-face {
             font-family: "AzoSans-Bold";
             src: url('<?=Yii::$app->homeUrl?>fonts/Rui Abreu - AzoSans-Bold.otf') format("opentype");
        }

        @font-face {
            font-family: "AzoSans-Light";
            src: url('<?=Yii::$app->homeUrl?>fonts/Rui Abreu - AzoSans-Light.otf') format("opentype");
        }
        @font-face {
            font-family: "AzoSans-Black";
            src: url('<?=Yii::$app->homeUrl?>fonts/Rui Abreu - AzoSans-Black.otf') format("opentype");
        }
        @font-face {
            font-family: "AzoSans-Regular";
            src: url('<?=Yii::$app->homeUrl?>fonts/Rui Abreu - AzoSans-Regular.otf') format("opentype");
        }
        @font-face {
            font-family: "AzoSans-Medium";
            src: url('<?=Yii::$app->homeUrl?>fonts/Rui Abreu - AzoSans-Medium.otf') format("opentype");
        }*/

        @font-face{

            font-family: "NotoNastaliqUrdu";
            src: url('<?=Yii::$app->homeUrl?>fonts/JameelNooriNastaleeq.ttf') format("truetype");


        }
        .carousel-item {
            height: 100vh;
            min-height: 350px;
            background: no-repeat center center scroll;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .for-bg-img::after {
            display: block;
            content: "";
            background-image: url(<?=Yii::$app->homeUrl?>/img/homepage-path-2.png);
            width: 50%;
            height: 100%;
            background-size: cover;
            right: 0px;
            position: absolute;
        }

        /* .for-bg-img::before {
            display: block;
            content: "";
            background-image: url(./img/homepage-bitmap.png);
            width: 50%;
            height: 60%;
            background-size: cover;
            right: 230px;
            position: absolute;
            z-index: 99;
            top: 330px;
        } */
        .my-account{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            background-color: rgb(155, 44, 225) !important;
        }

        .details-btn1{
            border-radius: 12px;
            background-color: rgba(247, 176, 79, 1.0);
            border: 0;
            padding-left: 2rem;
            padding-right: 2rem;
            font-size: 18.0px;
            color: rgba(255, 255, 255, 1.0);
            text-align: left;
            letter-spacing: 0.3px;
            line-height: 20.0px;
        }
        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #fff;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            margin-left: 10px;
            margin-top: 1px;
            border-radius: 4px;
        }

        .dropdown-content a {
            font-family: "Helvetica-Bold", Helvetica, Arial, serif;
            color: rgba(0,0,0,.5);
            padding: 12px 28px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: #fff;}

        .dropdown:hover .dropdown-content {display: block;}

        .dropdown:hover .dropbtn {background-color: #fff;}

    </style>


    <!-- CSS Part End-->


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


</head>

<?php $this->beginBody() ?>

<?php
$lang = $_REQUEST['lang'];

if(Yii::$app->user->isGuest)
{
    $cart_items = Cart::find()->where("session_id='".session_id()."'")->all();
}
else
{
    $cart_items = Cart::find()->where("user_id=".Yii::$app->user->identity->id)->all();
}

$itemcount = 0;
$count =0;

foreach($cart_items as $cart)
{
    $itemcount += $cart->total_items;
    $count++;
}


?>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="group101"><a class="navbar-brand" href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'site/index?lang=ur-UR' : 'site/index')?>">

                <img src="<?=Yii::$app->homeUrl?>img/homepage-group-9-1@2x.png" style="width: 100%;">
            </a>
        </div>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse"
                data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarResponsive" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item menu-text active">
                    <a class="nav-link" href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'site/index?lang=ur-UR' : 'site/index')?>"><?= Yii::t('app', 'HOME') ?>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item menu-text">
                    <a class="nav-link" href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'product/default/index?lang=ur-UR' : 'product/default/index')?>"><?= Yii::t('app', 'MOBILES') ?></a>
                </li>
                <!--<li class="nav-item menu-text">
                    <a class="nav-link" href="#">APPLY FOR</a>
                </li>-->
                <?php

                $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                if($_REQUEST['inventory_id'] != '' && $_REQUEST['lang'] == ''){
                    $url = $url.'&lang=ur-UR';
                }else if($_REQUEST['lang'] == ''){
                    $url = $url.'?lang=ur-UR';
                }
                ?>


                <li class="nav-item menu-text">
                    <a href="<?php echo $url?>" class="nav-link"><img
                                src="<?=Yii::$app->homeUrl?>img/pk.png"
                                title="اردو" alt="اردو"><span style="margin-left:0.3em; font-family: NotoNastaliqUrdu">اردو</span></a>
                </li>
                <?php
                $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

                if($_REQUEST['inventory_id'] != ''){

                    $url = strtok($url, '&');
                }else if($lang != ''){
                    $url = strtok($url, '?');
                }
                ?>
                <li class="nav-item menu-text">
                    <a href="<?php echo $url?>" class="nav-link"><img
                                src="<?=Yii::$app->homeUrl?>img/english.png"
                                title="&nbsp;English" alt="&nbsp;English"><span
                                style="margin-left:0.3em; " >&nbsp;ENGLISH</span></a>
                </li>
                <?php if(Yii::$app->user->isGuest){ ?>
                <li class="nav-item menu-text">
                    <a class="nav-link" href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'site/login?lang=ur-UR' : 'site/login')?>"><?= Yii::t('app', 'SIGN IN') ?></a>
                </li>
                 <?php }else{ ?>
                    <li class="nav-item menu-text">
                        <a class="nav-link" href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'site/login?lang=ur-UR' : 'site/logout')?>"><?= Yii::t('app', 'LOGOUT') ?></a>
                    </li>
                <?php } ?>
                <li class="nav-item menu-text">
                    <div class="input-group mb-4-5">
                        <div class="dropdown">
                        <?php
                        if(Yii::$app->user->isGuest)
                        {
                        ?>
                            <a href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'site/login?lang=ur-UR' : 'site/signup')?>" class="btn btn-lg btn-primary details-btn menu-signup font"><?= Yii::t('app', 'REGISTER') ?></a>

                        <?php
                        }
                        else
                        {
                        ?>
                         <a href="<?=Yii::$app->homeUrl?>site/o-history" class="btn btn-lg btn-primary details-btn menu-signup"><?= Yii::t('app', 'MY ACCOUNT') ?></a>


                        <?php } ?>
                        </div>
                        <div class="input-group-append dropdown">
                           <a href="<?=Yii::$app->homeUrl?><?php echo ($lang == 'ur-UR' ? 'order/default/cart?lang=ur-UR' : 'order/default/cart')?>">
                               <button class="bg-danger border-0 btn btn-secondary for-icon-border" type="button">
                                    <i class="fa fa-shopping-cart mycart"></i>
                                    <span class="badge badge-danger cart-total cartcount" id="lblCartCount"> <?=$itemcount?> </span>
                               </button>
                           </a>


                            <div class="dropdown-content" style="left: -200px;">
                                <table class="table carttable" style="width: 200px">
                                    <tbody>
                                    <input type="hidden" class="hiddencartvalue" value="">
                                    <input type="hidden" class="hiddenremainingstock" value="">
                                    <?php if(count($cart_items) <= 0){ ?>
                                        <h6 style="padding-top: 15px; padding-right: 15px;padding-left: 15px">Items Currently</h6>
                                    <?php }?>
                                    <?php if(count($cart_items) > 0){  ?>
                                    <h6 style="padding: 5px">Recently Added</h6>

                                    <?php
                                    foreach($cart_items as $cart)
                                    {
                                        $inventory_item = Inventory::findOne($cart->inventory_id);
                                        $prod_title = $inventory_item->product_name;
                                        $fileDetails = File::find()->where("entity_type='product' and entity_id=".$inventory_item->product_id)->one();
                                        ?>
                                        <tr>
                                            <td class="text-center"><div class="cart-product-container"><img src="<?=Url::base()?>/../../multeback/web/attachments/<?=$fileDetails->id?><?=strrchr($fileDetails->file_name, ".")?>" class="media-object"></div></td>
                                            <td class="text-left"><a href="<?=Url::to(['/product/default/detail', 'inventory_id' => $cart->inventory_id])?>"><?=$prod_title?></a></td>

                                            <td class="text-center"></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                    <?php }

                                    ?>
                                    </tbody>
                                </table>
                                <?php if(count($cart_items) > 0){  ?>
                                <hr>
                                <p style="padding: 5px"><b>Total Amount:</b><?=$inventory_item->price?></p>
                                <hr>

                                <div class="btn32">
                                    <a  href="<?=Url::to(['/order/default/cart'])?>" class="btn btn-primary" style="float: left; width: 116px; font-size: 12px; margin-right: 10px; margin-left: 8px;margin-bottom: 5px; border-color: red;  background-color: red;"> <?=Yii::t('app', 'View')?></a>
                                </div>
                                <div class="btn33">
                                    <a  href="<?=Url::to(['/order/default/cart'])?>" class="btn btn-primary" style="float: left; margin-bottom: 5px;width: 116px; font-size: 12px; border-color: #007bff;  background-color: #007bff;"> <?=Yii::t('app', 'Checkout')?></a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<?= $content ?>


<footer class="section footer-classic context-dark bg-image"style="color: gray">
    <div class="container">
        <div class="row row-30">
            <div class="col-md-3">
                <div class="pr-xl-3"><a class="brand" href="#"></a>
                    <h4 class="font"><?=Yii::t('app','About Us')?></h4>
                    <!-- Rights-->
                    <p class="rights"><?=Yii::t('app','KistPay is an online leasing website which enables you to buy smartphones and IOT devices on easy monthly installments at the most reasonable and market competitive prices. We provide a safe, one window solution by aggregating telecoms, financial institutions, insurance companies & smartphone brands together on one platform')?></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pr-xl-3"><a class="brand" href="#"></a>
                    <h4 class="font"><?=Yii::t('app','Contact')?></h4>
                    <!-- Rights-->
                    <p class="rights">info@kistpay.com</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pr-xl-3"><a class="brand" href="#"></a>
                    <h4 class="font"><?=Yii::t('app','GET TO KNOW US')?></h4>
                    <!-- Rights-->
                    <p class="rights"><a href="http://kistpay.com/multeback/web/" target="_blank" style="color: #808080;">Management Login</a></p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="pr-xl-3"><a class="brand" href="#"></a>
                    <div class="group101"><a class="navbar-brand" href="<?=Yii::$app->homeUrl?><?php echo ($_REQUEST['lang'] == 'ur-UR' ? 'site/index?lang=ur-UR' : 'site/index')?>"><img src="<?= Yii::$app->homeUrl?>/img/homepage-group-9-1@2x.png" style="width: 100%;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p style="text-align:center" ><b>© Kistpay.com<?=date('Y')?></b></p>
</footer>

    <?php $this->endBody() ?>

<!-- JS Part Start-->
<?php
/*$this->registerJsFile('@web/js/jquery-2.1.1.min.js');
$this->registerJsFile('@web/js/jPages.js');
$this->registerJsFile('@web/js/lazyload.js');
$this->registerJsFile('@web/js/bootstrap/js/bootstrap.min.js');
$this->registerJsFile('@web/js/jquery.easing-1.3.min.js');
$this->registerJsFile('@web/js/jquery.dcjqaccordion.min.js');
$this->registerJsFile('@web/js/owl.carousel.min.js');
$this->registerJsFile('@web/js/custom.js');
$this->registerJsFile('@web/js/jquery.elevateZoom-3.0.8.min.js');
$this->registerJsFile('@web/js/swipebox/lib/ios-orientationchange-fix.js');
$this->registerJsFile('@web/js/swipebox/src/js/jquery.swipebox.min.js');*/
?>
<!-- JS Part End-->
</html>
<?php $this->endPage() ?>

