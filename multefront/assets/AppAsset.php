<?php

namespace multefront\assets;

use yii\web\AssetBundle;

/**
 * Main multefront application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
		//'js/bootstrap/css/bootstrap.min.css',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
		'css/font-awesome/css/font-awesome.min.css',
		//'css/stylesheet.css',
        'css/style.css',
		'css/owl.carousel.css',
		'css/owl.transitions.css',
		//'css/responsive.css',
		'css/jPages.css',
		'css/star-rating.css',
		'js/swipebox/src/css/swipebox.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'


    ];
    public $js = [
		//'js/jquery-2.1.1.min.js',
		'js/jPages.js',
		'js/lazyload.js',
        'js/jquery.flexslider.js',
        'js/jquery.bxslider.js',
		'js/bootstrap/js/bootstrap.min.js',
		'js/jquery.easing-1.3.min.js',
		'js/jquery.dcjqaccordion.min.js',
		'js/owl.carousel.min.js',
		'js/custom.js',
		'js/star-rating.js',
		'js/jquery.elevateZoom.js',
		'js/swipebox/lib/ios-orientationchange-fix.js',
		'js/swipebox/src/js/jquery.swipebox.min.js',
        'js/verification.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
