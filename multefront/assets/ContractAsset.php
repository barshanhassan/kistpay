<?php

namespace multefront\assets;

use yii\web\AssetBundle;

/**
 * Main multefront application asset bundle.
 */
class ContractAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/contract.css?asas',



    ];
    public $js = [


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
