<?php

namespace multefront\assets;

use yii\web\AssetBundle;

/**
 * Main multefront application asset bundle.
 */
class AppAssetRTL extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
		'js/bootstrap/css/bootstrap.min.css',
		'js/bootstrap/css/bootstrap-rtl.min.css',
		'css/font-awesome/css/font-awesome.min.css',
		'css/owl.carousel.css',
		'css/owl.transitions.css',
		'css/stylesheet.css',
		'css/stylesheet-rtl.css',
		'css/jPages.css',
		'css/star-rating.css',
		'js/swipebox/src/css/swipebox.min.css',
		'https://fonts.googleapis.com/css?family=Open+Sans',
    ];
    public $js = [
		'js/jquery-2.1.1.min.js',
		'js/jPages.js',
		'js/lazyload.js',
		'js/bootstrap/js/bootstrap.min.js',
		'js/jquery.easing-1.3.min.js',
		'js/jquery.dcjqaccordion.min.js',
		'js/owl.carousel.min.js',
		'js/custom.js',
		'js/star-rating.js',
		'js/jquery.elevateZoom.js',
		'js/swipebox/lib/ios-orientationchange-fix.js',
		'js/swipebox/src/js/jquery.swipebox.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
